<?php
class sms_addHeaderFields{

	public function __construct(){
		add_action('redux/metaboxes/sms_redux/boxes', array($this, 'add_redux_metaboxes'), 10);
	}

	public function add_redux_metaboxes($metaboxes, $suppress = false) {

		$post_id = intval( $_GET['post'] );
		$post_type = get_post_type( $post_id );
		if( $post_type !== "sms_header" ){
			// exit if this isn't a header post
			return $metaboxes;
		}

		global $sms_redux;
		global $sms_utils;
		$current_post_id = intval( $_GET['post'] );
		$current_post_meta = $sms_utils->get_redux_post_meta( $current_post_id , 'sms_redux', true);

		/////////////////////////////////////////////////////////////////////////////////////
		// Custom Markup
		/////////////////////////////////////////////////////////////////////////////////////

		$fields = array();

		//CSS
		$fields[] = array(
			'id'        => 'less',
			'type'      => 'ace_editor',
			'title'     => 'Custom LESS',
			'mode'      => 'less',
			'theme'     => 'monokai',
			'desc'      => '',
			'subtitle'  => '',
		);

		// Create a Meta Box and reset the meta box section array
		//------------------------------------------------------------------
		$boxSections[]['fields'] = $fields;
		$metaboxes[] = array(
			'id' => 'header-css',
			'title' => 'CSS',
			'post_types' => array('sms_header'),
			'sections' => $boxSections
		);

		$metaboxes[] = array(
			'sections' => $boxSections
		);
		$boxSections = array();

		//------------------------------------------------------------------
		//------------------------------------------------------------------
		// Return all the fields and metaboxes if
		// $suppress isn't true. This is for debug purposes
		if(!$suppress)
			return $metaboxes;
	}

}

global $sms_header_fields;
$sms_header_fields = new sms_addHeaderFields();