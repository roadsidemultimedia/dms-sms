<?php
if ( !function_exists( "redux_add_font_kit_metaboxes" ) ):
	
	function redux_add_font_kit_metaboxes($metaboxes) {

		$post_id = intval( $_GET['post'] );
		$post_type = get_post_type( $post_id );
		if( $post_type !== "sms_font_kit" ){
			// exit if this isn't a font kit post
			return $metaboxes;
		}

		global $sms_redux;
		global $sms_utils;
		$sms_options = get_option('sms_options');
		$selected_font_kit_meta = $sms_utils->global_selected_font_kit_meta;
		$current_post_id = intval( $_GET['post'] );
		$current_post_meta = $sms_utils->get_redux_post_meta( $post_id );

		$font_type_field_array = $sms_options['fonts']['definition-field-data'];
		$font_assignment_field_array = $sms_options['fonts']['assignment-field-data'];
		$font_type_options = $sms_utils->get_font_type_list_as_redux_select_array($current_post_id);
		$font_size_options = $sms_utils->sms_options['fonts']['size-name-px-list'];
		$font_weight_options_named = $sms_utils->get_weights_for_font_type( $font_type_slug, $current_post_id );

		
		$fields = array();
		//=========================================
		// Custom Markup
		//-----------------------------------------
		$fields[] = array(
		 'id'        => 'font-kit-type',
		 'title'     => 'Font Asset Type(s) to Load',
		 'subtitle'      => 'Choose the type of font kit asset you need to load.',
		 'type'          => 'select',
			 'default'       => 'none',
			 'options'   => array( 
				 'none' => "None", 
				 'custom' => 'Custom', 
				 'google' => 'Google', 
				 'typekit' => 'Typekit', 
			 ),
		);

		$fields[] = array(
			'id'        => 'font-kit-markup',
			'type'      => 'ace_editor',
			'title'     => 'Custom &lt;head&gt; Markup',
			'mode'      => 'html',
			'theme'     => 'monokai',
			'subtitle'  => 'Load your Typekit, Webink, and Google fonts in here.',
			'desc'      => 'This will appear in the &lt;head&gt; tag, and could destroy the site, so excercise caution.',
			'required'  => array('font-kit-type', 'equals', 'custom'),
			// 'default'   => "<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>",
		);

		$fields[] = array(
			'id'            => 'google-font-code',
			'title'         => 'Google Font Code',
			'subtitle'      => 'Paste the Google font &lt;link&gt; here.',
			'type'          => 'text',
			'required'      => array('font-kit-type', 'equals', 'google'),
			// 'default'       => "",
		);

		if( $current_post_meta['typekit-id'] ){
			$typekit_link = '<a target="_blank" href="https://typekit.com/kit_editor/kits/'.$current_post_meta['typekit-id'].'">Edit font kit @ Typekit.com</a><br>Be sure to add this domain ('.$_SERVER['SERVER_NAME'].') to the allow list ';
		} else {
			$typekit_link = 'Save the font kit ID to see a link to edit the font kit.';
		}
		$fields[] = array(
			'id'            => 'typekit-id',
			'title'         => 'Typekit ID',
			'type'          => 'text',
			'desc'          => $typekit_link,
			'subtitle'      => 'Enter the 7 character Typekit ID that you want to load.',
			'default'       => '',
			'required'      => array('font-kit-type', 'equals', 'typekit'),
		);

		// Create a Meta Box and reset the meta box array
		//-----------------------------------------
		//
		
		// add fields to boxsections
		$boxSections[] = array(
			'icon' => 'el-gear',
			'fields' => $fields,
			'id' => 'font-kit-assets',
			'title' => 'Font Kit Assets',
		);
		//Create a Meta Box and reset the meta box array
		//-----------------------------------------
		$metaboxes[] = array(
			'id' => 'font-assets-meta-box',
			'title' => 'Configure Font Assets',
			'priority' => 'high',
			'post_types' => array('sms_font_kit'),
			'sections' => $boxSections
		);
		//Reset for next meta box
		$fields = array();
		$boxSections = array();

		$stock_heading_assignment_fields = array(
			'h1' => array(
				'title' => 'H1',
				'default' => 'xxxlarge',
			),
			'h2' => array(
				'title' => 'H2',
				'default' => 'xxlarge',
			),
			'h3' => array(
				'title' => 'H3',
				'default' => 'xlarge',
			),
			'h4' => array(
				'title' => 'H4',
				'default' => 'large',
			),
			'h5' => array(
				'title' => 'H5',
				'default' => 'medium',
			),
			'h6' => array(
				'title' => 'H6',
				'default' => 'small',
			),
		);

		foreach ($stock_heading_assignment_fields as $key => $value) {
			$fields[] = array(
				'id'      => "stock-$key-assignment",
				'type'    => 'select',
				'title'   => $value['title'],
				'desc'    => 'Assign a size.',
				'options' => $font_size_options,
				'default' => $value['default'],
			);
		}


		// Create a Meta Box and reset the meta box array
		//-----------------------------------------
		//
		
		// add fields to boxsections
		$boxSections[] = array(
			'icon' => 'el-gear',
			'fields' => $fields,
			'id' => 'font-kit-stock-assignments',
			'title' => 'H1-H6',
		);
		//Create a Meta Box and reset the meta box array
		//-----------------------------------------
		$metaboxes[] = array(
			'id' => 'font-kit-stock-meta-box',
			'title' => 'Assign Standard Heading Sizes',
			'priority' => 'high',
			'post_types' => array('sms_font_kit'),
			'sections' => $boxSections
		);
		//Reset for next meta box
		$fields = array();
		$boxSections = array();

		$fields[] = array(
				'id'    => 'font-type-enable-tip',
				'type'  => 'info',
				'style' => 'success',
				'title' => 'Tip',
				'icon'  => 'el-icon-info-sign',
				'desc'  => 'Set which types of fonts are available for use on the site.<br>Set the font family and weights available.',
		);

		$fields[] = array(
				'id'    => 'font-type-enable-tip',
				'type'  => 'info',
				'style' => 'success',
				'title' => 'Tip',
				'icon'  => 'el-icon-info-sign',
				'desc'  => 'The stars on the left indicate if a font type is enabled <i class="el-icon-star color-black"></i> or disabled <i class="el-icon-star-empty"></i>',
		);

		$fields[] = array(
				'id'    => 'font-type-enable-tip',
				'type'  => 'info',
				'style' => 'success',
				'title' => 'Only enable types that you need',
				'icon'  => 'el-icon-info-sign',
				'desc'  => 'You do not need to configure every font type. Only enable and configure the font types that will be used on the site.',
		);

		// add fields to boxsections
		$boxSections[] = array(
			'icon' => 'el-icon-info-sign',
			'fields' => $fields,
			'id' => 'font-type-config-tip',
			'title' => "Tips",

		);
		$fields = array();





		foreach ($font_type_field_array as $value) {

			$fields[] = array(
				'id'            => $value['id'].'-enabled',
				'title'         => 'Enable '.$value['title'].'?',
				'type'          => 'switch',
				'default'       => false,
			);

			$fields[] = array(
				'id'            => $value['id'].'-font-family',
				'title'         => 'Font Family',
				'subtitle'      => 'Define the font-family value that will be used in CSS.',
				'type'          => 'text',
				'default'       => $value['default-family'],
				'required'      => array($value['id'].'-enabled', 'equals', true),
			);

			$fields[] = array(
				'id'            => $value['id'].'-weights',
				'title'         => 'Available Weights',
				'subtitle'      => 'What font weights are available and loading in for this font-family?',
				'type'          => 'select',
				'multi'         => true,
				'default'       => '400',
				'options'       => $sms_options['fonts']['weight-name-list'],
				'required'      => array($value['id'].'-enabled', 'equals', true),
			);

			
			// add fields to boxsections
			$boxSections[] = array(
				'icon' => $icon = $current_post_meta[ $value['id'].'-enabled' ] ? 'el-icon-star' : 'el-icon-star-empty',
				'fields' => $fields,
				'id' => $value['id'].'-config',
				'title' => $value['title'],

			);
			$fields = array();
			
		}
		//Create a Meta Box and reset the meta box array
		//-----------------------------------------
		$metaboxes[] = array(
			'id' => 'font-types-meta-box',
			'title' => 'Configure Font Types',
			// 'priority' => 'high',
			'post_types' => array('sms_font_kit'),
			'sections' => $boxSections
		);
		//Reset for next meta box
		$fields = array();
		$boxSections = array();


		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////



		$fields[] = array(
			'id'    => 'font-kit-assignments-tip',
			'type'  => 'info',
			'style' => 'success',
			'icon'  => 'el-icon-info-sign',
			'title' => 'Purpose of font assignments',
			'desc'  => 'In this section, you\'ll set which types of fonts are assigned to various elements throughout the site.<br>This is where all the real magic happens.',
		);
		$fields[] = array(
			'id'    => 'font-type-enable-tip',
			'type'  => 'info',
			'style' => 'success',
			'title' => 'Star icons',
			'icon'  => 'el-icon-info-sign',
			'desc'  => 'The stars on the left indicate if a font assignment is active <i class="el-icon-star color-black"></i> or disabled <i class="el-icon-star-empty"></i>',
		);

		// add fields to boxsections
		$boxSections[] = array(
			'icon' => 'el-icon-info-sign',
			'fields' => $fields,
			'id' => 'font-type-config-tip',
			'title' => "Tips",

		);
		$fields = array();


		foreach ($font_assignment_field_array as $key => $value) {

			$title               = $value['title'];
			$font_type_slug      = $current_post_meta[ $key .'-font'];

			// TEMPORARILY DISABLED: due to the way redux handles default values. This is causing issues with CSS output.
			// $font_size_default   = $value['default_size'];
			// $font_type_default   = $font_type_options[0];
			// if($font_type_slug){
			//   $font_weight_default = $font_weight_options_named[0];
			// }

			// Font selection
			$fields[] = array(
				'id'        => $key.'-font',
				'title'     => "Font Type",
				'type'      => 'select',
				'subtitle'  => 'Select Font Type (<strong>HINT:</strong>you may have to refresh twice for this to populate correctly.)<br>'.
				               '<strong>Selected Font Family:</strong> <small>' . $current_post_meta[$font_type_slug.'-font-family'].'</small>',
				'default'   => $font_type_default,
				'options'   => $font_type_options,
			);

			// Size selection
			if($key !== 'body'){
				$fields[] = array(
					'id'        => $key.'-size',
					'title'     => "Size",
					'type'      => 'select',
					// 'default'   => $font_size_default,
					'options'   => $font_size_options,
				);
			}

			// Weight selection
			$fields[] = array(
				'id'        => $key.'-weight',
				'title'     => "Weight",
				'type'      => 'select',
				// 'default'   => $font_weight_default,
				// 'options'   => $sms_utils->get_weights_for_font_type( $font_type_slug, $current_post_id ),
				'options'   => $sms_utils->sms_options['fonts']['weight-name-list'],
				// 'required'  => array($key.'-font', 'equals', true),
			);


			/*
			 * Work in progress
			 * the goal is to display a preview of the chosen font. 

			$fields[] = array(
				'id'          => 'opt-typography',
				'type'        => 'typography', 
				'title'       => __('Typography', 'redux-framework-demo'),
				'google'      => false, 
				'font-backup' => false,
				'units'       =>'px',
				// 'subtitle'    => __('Typography option with each property can be called individually.', 'redux-framework-demo'),
				'default'     => array(
					'color'       => '#333', 
					'font-style'  => '700', 
					'font-family' => 'Abel', 
					'google'      => true,
					'font-size'   => '33px', 
				),
				// 'fonts' => $sms_utils->get_weights_for_font_type( $font_type_slug, $current_post_id ),
				'fonts' => array(
					'Georgia' => 'georgia',
				),
				'font-style' => false,
				'font-weight' => false,
				'font-size' => false,
				// 'font-family' => false,
				'subsets' => false,
				'line-height' => false,
				'text-align' => false,
				'color' => false,
				'preview' => array(
					'text' => 'the quick brown fox is great',
					'always_display' => true,
				),
			);
		*/

			//////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////

			if( $current_post_meta[ $key.'-font' ] || $current_post_meta[ $key.'-size' ] || $current_post_meta[ $key.'-weight' ] ){
				$icon = 'el-icon-star';
			} else{
				$icon = 'el-icon-star-empty';
			}
			// add fields to boxsections
			$boxSections[] = array(
				'icon' => $icon,
				'fields' => $fields,
				'id' => $value['id'].'-config',
				'title' => $value['title'],
				'desc' => $value['desc'],

			);
			$fields = array();

		} // endforloop

		//Create a Meta Box and reset the meta box array
		//-----------------------------------------
		$metaboxes[] = array(
			'id' => 'font-assignment-meta-box',
			'title' => 'Assign Fonts',
			// 'priority' => 'high',
			'post_types' => array('sms_font_kit'),
			'sections' => $boxSections
		);
		//Reset for next meta box
		$fields = array();
		$boxSections = array();

		// Create a Meta Box and reset the meta box array
		//-----------------------------------------
		// $boxSections[]['fields'] = $fields;
		// $metaboxes[] = array(
		//    'id' => $key.'-assignment-section',
		//    'title' => $title,
		//    'post_types' => array('sms_font_kit'),
		//    'sections' => $boxSections
		// );
		// $fields = array();
		// $boxSections = array();

		return $metaboxes;
	}
	add_action('redux/metaboxes/sms_redux/boxes', 'redux_add_font_kit_metaboxes');

endif;
