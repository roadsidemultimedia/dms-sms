<?php
if ( !function_exists( "redux_add_color_scheme_metaboxes" ) ):
	
	function redux_add_color_scheme_metaboxes($metaboxes) {
    $post_id = intval( $_GET['post'] );
    $post_type = get_post_type( $post_id );
    if( $post_type !== "sms_color" ){
      // exit if this isn't a color scheme post
      return $metaboxes;
    }

    global $sms_redux;
    global $sms_utils;
    $sms_options = $sms_utils->sms_options;
    $current_post_id = intval( $_GET['post'] );
    $current_post_meta = $sms_utils->get_redux_post_meta( $current_post_id , 'sms_redux', true);

    $brightness_array = $sms_options['colors']['brightness-list'];

		$fields = array();

    if(!$current_post_meta){
      $fields[] = array(
        'id'    => 'color-set-colors-warning',
        'type'  => 'info',
        'title' => 'Please define theme colors',
        'style' => 'warning',
        'desc'  => "Once you've defined the colors, you'll be able to assign them to elements throughout the site.",
      );
    }

    //=========================================
    // Human-readable color names
    //-----------------------------------------

    $color_definition_registration = array(
      'primary' => array(
        'id'          => 'def-color-primary',
        'desc'        => 'Dark and light colors are based off this color',
        'title'       => 'Primary',
        'default'     => '#14269f',
      ),
      'secondary' => array(
        'id'          => 'def-color-secondary',
        'title'       => 'Secondary',
        'default'     => '#36c577',
      ),
      'tertiary' => array(
        'id'          => 'def-color-tertiary',
        'title'       => 'Tertiary',
        'default'     => '#df342e',
      ),
      'accent1' => array(
        'id'          => 'def-color-accent1',
        'title'       => 'Accent #1',
        'desc'        => 'This is the default button and link color.',
        'default'     => '#8840a7',
      ),
      'accent2' => array(
        'id'          => 'def-color-accent2',
        'title'       => 'Accent #2',
        'default'     => '#edba22',
      ),

      // Predefined colors with no definition field
      'dark' => array(
        'id'          => 'def-color-dark',
        'title'       => 'Dark',
        'default'     => '@color-dark',
        'predefined'  => true
      ),
      'light' => array(
        'id'          => 'def-color-light',
        'title'       => 'Light',
        'default'     => '@color-light',
        'predefined'  => true
      ),
      'black' => array(
        'id'          => 'def-color-black',
        'title'       => 'Black',
        'default'     => '@color-black',
        'predefined'  => true
      ),
      'white' => array(
        'id'          => 'def-color-white',
        'title'       => 'White',
        'default'     => '@color-white',
        'predefined'  => true
      ),
    );


    //=========================================
    // Generate Color definition fields (where you set the colors)
    // Leave out predefined colors, such as black and white
    //-----------------------------------------

    foreach ($color_definition_registration as $key => $property) {
      if($property['predefined']){
        continue;
      }
      $fields[] = array(
        'id'            => $property['id'],
        'title'         => $property['title'],
        'subtitle'      => $property['subtitle'],
        'desc'          => $property['desc'],
        'type'          => 'color',
        'transparent'   => false,
        'default'       => $property['default'],
      );
    }

    // Create a Meta Box and reset the meta box array
    //-----------------------------------------

    // add fields to boxsections
    $boxSections[]['fields'] = $fields;

    $metaboxes[] = array(
        'id' => 'def-colors',
        'title' => 'Colors Definitions',
        'priority' => 'high',
        'post_types' => array('sms_color'),
        'sections' => $boxSections
    );

    //Reset for next meta box
    $fields = array();
    $boxSections = array();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Color Assignments
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Get names of color definitions from Redux metabox field setup array
    $color_def_names = array();
    foreach ($color_definition_registration as $key => $value) {
      $color_def_names[$value['id']] = $value['title'];
    }

    $sms_options['colors']['color-name-list'] = $color_def_names;

    $color_class_select2 = array(
        "sms-swatch sms-color-primary" => "Primary",
        "sms-swatch sms-color-secondary" => "Secondary",
        "sms-swatch sms-color-tertiary" => "Tertiary",
        "sms-swatch sms-color-accent1" => "Accent1",
        "sms-swatch sms-color-accent2" => "Accent2",
    );

    // Get the saved data of color definitons
    // Don't show assignment fields if there aren't any colors saved.
    if($current_post_meta){

      $types = array(
        'dark'  => 'Dark',
        'light' => 'Light', 
      );

      $assignment_field_list = array(
        'body'     => "Body",
        'heading'  => "Headings",
        'link'     => "Links (A tags)",
      );

      //=========================================
      // Element Assignments
      //-----------------------------------------

      foreach ($types as $shade_key => $shade_title) {

        foreach ($assignment_field_list as $field_key => $field_title) {

          $shade_contrast = $shade_key == 'dark' ? 'light' : 'dark';
          $tip = "The contrasting color is <code>$shade_contrast</code>";

          $fields[] = array(
            'id'            => "ass-$shade_key-$field_key-color",
            'title'         => "Select color",
            'type'          => 'select',
            'options'       => $color_def_names,
            'subtitle'          => $tip,
            'placeholder'   => 'Select a color:',
          );


 // ================================================================================================
 // Fancy color preview selection
 // ================================================================================================
          

          // $fields[] = array(
          //     'id'       => "ass-$shade_key-$field_key-color-fancy",
          //     'type'     => 'select',
          //     'select2'  => array( 'containerCssClass' => 'sms-color-select' ),
          //     'title'    => 'Color Selection',
          //     'subtitle' => 'Select Color',
          //     'class'    => ' font-icons sms-color-preview',
          //     'options'  => $color_class_select2
          // );

 // ================================================================================================
 // ================================================================================================
          
          $fields[] = array(
            'id'            => "ass-$shade_key-$field_key-color-brightness",
            'title'         => "◐ Brightness",
            'type'          => 'select',
            'default'       => '0',
            'options'       => $brightness_array,
            'placeholder'   => 'Select a brightness level:',
            'required'      => array(
              array( "ass-$shade_key-$field_key-color", 'not', 'def-color-white' ),
              array( "ass-$shade_key-$field_key-color", 'not', 'def-color-black' ),
              // array( "ass-$shade_key-$field_key-color", '=', true ),
            ),
          ); 

          // // Tints & Shades of Primary Color
          // $fields[] = array(
          //   'id'            => "ass-$shade_key-$field_key-color-brightness-primary",
          //   'title'         => "◐ Brightness of Primary Color",
          //   'type'          => 'select',
          //   'default'       => '0',
          //   'options'       => $brightness_array,
          //   'placeholder'   => 'Select a brightness level:',
          //   'required'      => array(
          //     // array( "ass-$shade_key-$field_key-color", 'not', 'def-color-white' ),
          //     // array( "ass-$shade_key-$field_key-color", 'not', 'def-color-black' ),
          //     array( "ass-$shade_key-$field_key-color-fancy", '=', 'sms-swatch sms-color-primary' ),
          //   ),
          // );

          // add fields to box sections
          $boxSections[] = array(
            'icon' => $icon = $current_post_meta[ "ass-$shade_key-$field_key-color" ] ? 'el-icon-star' : 'el-icon-star-empty',
            'fields' => $fields,
            'id' => "ass-$shade_key-$field_key-color-box",
            'title' => $field_title,
          );
          $fields = array();

        }

        $metaboxes[] = array(
            'title' => "$shade_title Band Color Assignments",
            'id' => "ass-band-color-assignments-$shade_key",
            'priority' => 'default',
            'post_types' => array('sms_color'),
            'sections' => $boxSections
        );

        //Reset for next meta box
        $fields = array();
        $boxSections = array();

      }


    } // endif

    //=========================================
    /////////////////////////////////////////////////


    // update_option('sms_options', $sms_options);
    $sms_utils->sms_options = $sms_options;
    return $metaboxes;

  }
  add_action('redux/metaboxes/sms_redux/boxes', 'redux_add_color_scheme_metaboxes');

endif;
