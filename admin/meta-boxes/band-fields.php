<?php

if ( !function_exists( "redux_add_metaboxes" ) ):
	
	function redux_add_metaboxes($metaboxes) {

		$post_id = intval( $_GET['post'] ) ?: false;
		$post_type = $post_id ? get_post_type( $post_id ) : false;
		if( $post_type !== "sms_band" ){
			// exit if this isn't a band post
			return $metaboxes;
		}

		global $sms_redux;
		global $sms_utils;
		$current_post_id = intval( $_GET['post'] );

		
		$current_post_meta = $sms_utils->get_redux_post_meta( $current_post_id , false, true);

		// $current_post_meta = redux_post_meta( "sms_redux", $current_post_id  );

		$font_kit_meta = $sms_utils->global_selected_font_kit_meta;
		$font_kit_id = $sms_utils->global_selected_font_kit_id;

		$color_scheme_meta = $sms_utils->global_selected_color_scheme_meta;
		$fields = array();
		$boxSections = array();

		//=========================================
		// TYPOGRAPHY
		//-----------------------------------------

		$band_font_assignment_field_array = array(
	 // $key                                   // $value['title']
	 // \/                                         \/
			'body'                => array( 'title' => 'Body' ),
			'heading--primary'    => array( 'title' => 'Heading' ),
			'heading--secondary'  => array( 'title' => 'Subheading' ),
			'heading--tertiary'   => array( 'title' => 'Accent Heading' ),
			// 'quote'               => array( 'title' => 'Quote' ),
		);

		// // Create on / off switches
		// foreach ($band_font_assignment_field_array as $key => $value) {
		// 	$fields[] = array(
		// 		'id'            => $key . '-enabled',
		// 		'title'         => 'Customize '.$value['title'].' Font',
		// 		'type'          => 'switch',
		// 		'default'       => false,
		// 	);
		// }

		$i = 0;
		// Create typography fields
		foreach ($band_font_assignment_field_array as $key => $value) {

			$fields = array();

			$font_type_slug      = $current_post_meta[ $key .'-font' ];
			// echo "<pre>\$key .'-font': " . print_r($key .'-font', true) . "</pre>";
			$font_family         = $font_type_slug ? $font_kit_meta[ $font_type_slug.'-font-family' ] : false;
			$font_type_options   = $sms_utils->get_font_type_list_as_redux_select_array( $font_kit_id );
			$title               = $value['title'];


			$font_type_default   = $font_type_options ? key( $font_type_options ) : false;
			$font_type_global    = $font_kit_meta[ "$key-font" ];
			$font_family_global  = $font_kit_meta[ $font_type_global . '-font-family' ];

			$debug_array = array(
				'title'              => $title,
				'font_type_slug'     => $font_type_slug,
				'font_family_key'    => $font_type_slug.'-font-family',
				'font_family'        => $font_family,
				'font_type_options'  => $font_type_options,
				'font_type_default'  => $font_type_default,
				'font_type_global'   => $font_type_global,
				'font_family_global' => $font_family_global,
			);
			// echo "<pre>\$current_post_meta: " . print_r($current_post_meta, true) . "</pre>";
			// echo "<pre>\$font_kit_meta: " . print_r($font_kit_meta, true) . "</pre>";

			// echo "<pre>\$debug_array: " . print_r($debug_array, true) . "</pre>";

			$font_type_subtitle = '<p>';
			if( $font_family ){
				$font_type_subtitle .= '<small><strong>Selected Font Family: </strong>' . $font_family .'</small>';
			}
			$font_type_subtitle .= '<br><small><strong>Global Font Type: </strong>'. $font_type_global.'</small>';
			$font_type_subtitle .= '<br><small><strong>Global Font Family: </strong>'. $font_family_global.'</small>';
			$font_type_subtitle .= '</p>';

			// $font_size_options   = $sms_utils->sms_options['fonts']['size-name-reference-list'];
			$font_size_options   = $sms_utils->sms_options['fonts']['size-name-px-list'];
			// $font_size_default   = $value['default_size'] ? $value['default_size'] : false;

			$font_weight_options_named = $sms_utils->get_weights_for_font_type( $font_type_slug );
			// $font_weight_options_named = $sms_utils->sms_options['fonts']['weight-name-list'];
			// $font_weight_default = $font_weight_options_named[0] ? $font_weight_options_named[0] : false;

			// $sms_utils->write_log("font-family-slug");
			// $sms_utils->write_log( $font_type_slug.'-font-family' );
			

			// Create on / off switches
			$fields[] = array(
				'id'            => $key . '-enabled',
				'title'         => 'Customize '.$value['title'].' Font',
				'type'          => 'switch',
				'default'       => false,
			);

			// Color selection
			$fields[] = array(
				'id'            => $key.'-color',
				'title'         => 'Color',
				'type'          => 'select',
				'options'       => $sms_utils->sms_options['colors']['color-name-list'],
				'required'      => array( $key . '-enabled', 'equals', true),
			);
			$fields[] = array(
				'id'            => $key.'-color-brightness',
				'title'         => 'Brightness',
				'type'          => 'select',
				'options'       => $sms_utils->sms_options['colors']['brightness-list'],
				'required'      => array( $key . '-enabled', 'equals', true),
			);

			// Font selection
			$fields[] = array(
				'id'            => $key.'-font',
				'title'         => "Font Type",
				'type'          => 'select',
				'subtitle'      => 'Select Font Type',
				'subtitle'      => $font_type_subtitle,
				// 'default'       => $font_type_default,
				'options'       => $font_type_options,
				'required'      => array( $key . '-enabled', 'equals', true),
			);

			// No font size selection for body
			if($key != 'body'){
				// Size selection
				$fields[] = array(
					'id'            => $key.'-size',
					'title'         => "Size",
					'type'          => 'select',
					// 'default'       => $font_size_default,
					'options'       => $font_size_options,
					'required'      => array( 
						array( $key . '-enabled', 'equals', true ),
					),
				);
			}

			// Weight selection
			$fields[] = array(
				'id'            => $key.'-weight',
				'title'         => "Weight",
				'type'          => 'select',
				// 'default'       => $font_weight_default,
				'options'       => $font_weight_options_named,
				'required'      => array( 
					array( $key . '-enabled', 'equals', true ),
					// array( $key.'-font', 'equals', true ),
				),
			);

			// Line Height
			$fields[] = array(
				'id'           => $key.'-line-height',
				'title'        => "Line Height",
				'type'         => 'select',
				'options'      => $sms_utils->sms_options['fonts']['line-height-list'],
				// 'default'      => $sms_utils->sms_options['fonts']['line-height-list']['1.5'],
				'required'     => array( $key . '-enabled', 'equals', true),
			);

			// Letter Spacing
			$fields[] = array(
				'id'            => $key.'-letter-spacing',
				'title'         => 'Letter Spacing / Tracking',
				'subtitle'      => "1 = 0.01em",
				'type'          => 'spinner',
				"default"       => 0,
				"min"           => 0,
				"step"          => 1,
				"max"           => 100,
				'display_value' => 'text',
				'required'      => array( $key . '-enabled', 'equals', true),
			);

			// Text Alignment
			$fields[] = array(
				'id' => $key.'-text-align',
				'title' => 'Text Alignment',
				'type' => 'select',
				'options' => $sms_utils->sms_options['fonts']['text-alignment-list'],
				'required'  => array( $key . '-enabled', 'equals', true),
			);

			// Text Transform
			$fields[] = array(
				'id' => $key.'-text-transform',
				'title' => 'Text Transform',
				'type' => 'select',
				'options' => $sms_utils->sms_options['fonts']['text-transform-list'],
				'required'  => array( $key . '-enabled', 'equals', true),

			);

			//Create a Meta Box and reset the meta box array
			//-----------------------------------------

			if( $current_post_meta[ "$key-enabled" ] ){
				$icon = 'el-icon-star';
			} else{
				$icon = 'el-icon-star-empty';
			}
			// add fields to boxsections
			$boxSections[] = array(
				'icon' => $icon,
				'fields' => $fields,
				'id' => $key.'-typography',
				'title' => $value['title'],
			);

			//Reset for next meta box
			// $fields = array();
			// $boxSections = array();
			$i++;

		} // endforloop

		

		$metaboxes[] = array(
			'id' => 'metabox-'.$key.'-typography',
			'title' => ' Band Typography',
			'post_types' => array('sms_band'),
			'sections' => $boxSections,
		);

		$fields = array();
		$boxSections = array();
		// echo "<pre>\$metaboxes: " . print_r($metaboxes, true) . "</pre>";
		// return $metaboxes;

		//=========================================
		/////////////////////////////////////////////////



		// //=========================================
		// // Custom LESS
		// //-----------------------------------------
		$slot_slug = $sms_utils->get_band_slot_slug_from_id( $current_post_id );

		if( isset($slot_slug) ){
			$css_selector = ".sms-$slot_slug";
			$band_selector_subtitle = "The CSS output will be nested within the selector<br><strong style='font-size: 20px'>$css_selector{}</strong><br>";
			$band_selector_desc = "You can use the parent selector <code>&</code> to make use of classes above the scope of this selector."."<br><br>".
			"See the <a href='http://lesscss.org/features/#parent-selectors-feature'>LESS documentation</a> for more info."."<br><br>".
			"Example:"."<br>".
			"<pre style='background: #eee; padding: 10px; border: solid 1px rgba(0,0,0,.05)'>$css_selector{"."<br>".
			"    body <strong>&</strong>{"."<br>".
			"        color: red;"."<br>".
			"    }"."<br>".
			"}"."</pre>".
			"Prints out as:<pre style='background: #eee; padding: 10px; border: solid 1px rgba(0,0,0,.05)'>body $css_selector{ color: red; }</pre>";
		} else {
			$band_selector_subtitle = "The CSS output will be nested within the selector<br>";
			$band_selector_desc = "You can use the parent selector <code>&</code> to make use of classes above the scope of this selector."."<br>".
			"See the <a href='http://lesscss.org/features/#parent-selectors-feature'>LESS documentation</a> for more info."."<br>";
		}
		

		$fields[] = array(
			'id'        => 'less',
			'type'      => 'ace_editor',
			'title'     => 'Custom LESS',
			'mode'      => 'less',
			'theme'     => 'monokai',
			'subtitle'  => $band_selector_subtitle,
			'desc'      => $band_selector_desc,
		);


		if( $current_post_meta[ "less" ] ){
			$icon = 'el-icon-star';
		} else{
			$icon = 'el-icon-star-empty';
		}

		// add fields to boxsections
		$boxSections[] = array(
			'icon' => $icon,
			'fields' => $fields,
			'id' => 'band-less',
			'title' => 'Custom LESS / CSS',
		);

		$fields = array();

		// //=========================================
		// // Link Color
		// //-----------------------------------------
		
		$fields[] = array(
			'id'            => 'link-color-enabled',
			'title'         => 'Customize the Link Color (A tags) on this band?',
			'type'          => 'switch',
			'on'            => 'Enabled',
			'off'           => 'Disabled',
			'default'       => false,
		);

		$fields[] = array(
			'id'            => "link-color-type",
			'title'         => 'Color Type',
			'type'          => 'select',
			'default'       => 'global',
			'options'       => array(
				'custom'  => 'Custom Color',
				'global'  => 'Global Colors',
			),
			'required'      => array(
				array('link-color-enabled', 'equals', true),
			),
		);

		// Global Color
		// //////////////
		$fields[] = array(
			'id'            => 'link-color-global',
			'title'         => 'Color',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['color-name-list'],
			'required'      => array(
				array('link-color-enabled', 'equals', true),
				array('link-color-type', 'equals', 'global'),
			),
		);

		$fields[] = array(
			'id'            => 'link-color-global-brightness',
			'title'         => 'Brightness',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['brightness-list'],
			'required'      => array(
				array('link-color-enabled', 'equals', true),
				array('link-color-type', 'equals', 'global'),
			),
		);

		// hover
		$fields[] = array(
			'id'            => 'link-color-hover-global',
			'title'         => 'Hover Color',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['color-name-list'],
			'required'      => array(
				array('link-color-enabled', 'equals', true),
				array('link-color-type', 'equals', 'global'),
			),
		);

		$fields[] = array(
			'id'            => 'link-color-global-hover-brightness',
			'title'         => 'Hover Brightness',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['brightness-list'],
			'required'      => array(
				array('link-color-enabled', 'equals', true),
				array('link-color-type', 'equals', 'global'),
			),
		);

		// Custom Color
		// //////////////
		$fields[] = array(
			'id'            => 'link-color-custom',
			'title'         => 'Color',
			'type'          => 'color_rgba',
			'transparent'   => false,
			'required'      => array(
				array('link-color-enabled', 'equals', true),
				array('link-color-type', 'equals', 'custom'),
			),
		);


		// Custom Hover Color
		// //////////////
		$fields[] = array(
			'id'            => 'link-color-hover-custom',
			'title'         => 'Hover Color',
			'type'          => 'color_rgba',
			'description'      => '<p style="padding-bottom: 160px"></p>',
			'transparent'   => false,
			'required'      => array(
				array('link-color-enabled', 'equals', true),
				array('link-color-type', 'equals', 'custom'),
			),
		);



		if( $current_post_meta[ "link-color-enabled" ] ){
			$icon = 'el-icon-star';
		} else{
			$icon = 'el-icon-star-empty';
		}

		// add fields to boxsections
		$boxSections[] = array(
			'icon' => $icon,
			'fields' => $fields,
			'id' => 'link-color',
			'title' => 'Link Color',
		);

		$fields = array();

		// //=========================================
		// // Borders
		// //-----------------------------------------
		
		$fields[] = array(
			'id'            => 'borders-enabled',
			'title'         => 'Enable Borders?',
			'type'          => 'switch',
			'on'            => 'Enabled',
			'off'           => 'Disabled',
			'default'       => false,
		);


		// Top
		// ------------------------------------------
		$fields[] = array(
			'id'            => 'border-top-title',
			'title'         => 'TOP BORDER',
			'type'          => 'section',
			'indent'        => true,
			'required'      => array(
				array('borders-enabled', 'equals', true),
			),
		);
		$fields[] = array(
			'id'            => 'border-top-enabled',
			'title'         => 'Enable Top Border?',
			'type'          => 'switch',
			'on'            => 'Enabled',
			'off'           => 'Disabled',
			'required'      => array(
				array('borders-enabled', 'equals', true),
			),
		);


		$fields[] = array(
			'id'            => "border-top-color-type",
			'title'         => 'Color Type',
			'type'          => 'select',
			'default'       => 'global',
			'options'       => array(
				'custom'  => 'Custom Color',
				'global'  => 'Global Colors',
			),
			'required'      => array(
				array('borders-enabled', 'equals', true),
			),
		);

		// Global Color
		// //////////////
		$fields[] = array(
			'id'            => 'border-top-color-global',
			'title'         => 'Color',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['color-name-list'],
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-top-enabled', 'equals', true),
				array('border-top-color-type', 'equals', 'global'),
			),
		);

		$fields[] = array(
			'id'            => 'border-top-color-global-brightness',
			'title'         => 'Brightness',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['brightness-list'],
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-top-enabled', 'equals', true),
				array('border-top-color-type', 'equals', 'global'),
			),
		);

		// Custom Color
		// //////////////
		$fields[] = array(
			'id'            => 'border-top-color',
			'title'         => 'Color',
			'type'          => 'color_rgba',
			'transparent'   => false,
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-top-enabled', 'equals', true),
				array('border-top-color-type', 'equals', 'custom'),
			),
		);

		$fields[] = array(
			'title'         => 'Width',
			'id'            => 'border-top-width',
			'type'          => 'slider',
			"default"       => 5,
			"min"           => 1,
			"step"          => 1,
			"max"           => 100,
			'display_value' => 'text',
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-top-enabled', 'equals', true),
			),
		);

		// Bottom
		// ------------------------------------------
		$fields[] = array(
			'id'            => 'border-bottom-title',
			'title'         => 'BOTTOM BORDER',
			'type'          => 'section',
			'indent'        => true,
			'required'      => array(
				array('borders-enabled', 'equals', true),
			),
		);
		$fields[] = array(
			'id'            => 'border-bottom-enabled',
			'title'         => 'Enable Bottom Border?',
			'type'          => 'switch',
			'on'            => 'Enabled',
			'off'           => 'Disabled',
			'required'      => array(
				array('borders-enabled', 'equals', true),
			),
		);


		$fields[] = array(
			'id'            => "border-bottom-color-type",
			'title'         => 'Color Type',
			'type'          => 'select',
			'default'       => 'global',
			'options'       => array(
				'custom'  => 'Custom Color',
				'global'  => 'Global Colors',
			),
			'required'      => array(
				array('borders-enabled', 'equals', true),
			),
		);

		
		// Global Colors
		// //////////////
		$fields[] = array(
			'id'            => 'border-bottom-color-global',
			'title'         => 'Color',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['color-name-list'],
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-bottom-enabled', 'equals', true),
				array('border-bottom-color-type', 'equals', 'global'),
			),
		);

		$fields[] = array(
			'id'            => 'border-bottom-color-global-brightness',
			'title'         => 'Brightness',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['brightness-list'],
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-bottom-enabled', 'equals', true),
				array('border-bottom-color-type', 'equals', 'global'),
			),
		);
		
		// Custom Colors
		// //////////////
		$fields[] = array(
			'id'            => 'border-bottom-color',
			'title'         => 'Color',
			'type'          => 'color_rgba', 
			'transparent'   => false,
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-bottom-enabled', 'equals', true),
				array('border-bottom-color-type', 'equals', 'custom'),
			),
		);

		$fields[] = array(
			'title'         => 'Width',
			'id'            => 'border-bottom-width',
			'type'          => 'slider',
			"default"       => 5,
			"min"           => 1,
			"step"          => 1,
			"max"           => 100,
			'display_value' => 'text',
			'required'      => array(
				array('borders-enabled', 'equals', true),
				array('border-bottom-enabled', 'equals', true),
			),
		);



		if( $current_post_meta[ "borders-enabled" ] ){
			$icon = 'el-icon-star';
		} else{
			$icon = 'el-icon-star-empty';
		}
		// add fields to boxsections
		$boxSections[] = array(
			'icon' => $icon,
			'fields' => $fields,
			'id' => 'band-border',
			'title' => 'Borders',
		);

		$fields = array();
		
		
		//=========================================
		// Shadows
		//-----------------------------------------

		$fields[] = array(
			'id'            => 'shadow-enabled',
			'title'         => 'Enable Shadows?',
			'type'          => 'switch',
			'on'            => 'Enabled',
			'off'           => 'Disabled',
			'default'       => false,
		);

		$fields[] = array(
			'id'            => 'shadow-color',
			'title'         => 'Shadow Color',
			'type'          => 'color',
			'required'      => array('shadow-enabled', 'equals', true),
		);

		$fields[] = array(
			'title'         => 'Opacity',
			'id'            => 'shadow-opacity',
			'type'          => 'slider',
			"default"       => 40,
			"min"           => 10,
			"step"          => 10,
			"max"           => 100,
			'display_value' => 'text',
			'required'      => array('shadow-enabled', 'equals', true),
		);

		$fields[] = array(
			'id'            => 'shadow-blur',
			'type'          => 'slider',
			'title'         => 'Blur amount (PX)',
			'min'           => '5',
			'step'          => '5',
			'default'       => '40',
			'max'           => '100',
			'required'      => array('shadow-enabled', 'equals', true),
		);

		$fields[] = array(
			'id'          => 'shadow-inset',
			'type'        => 'button_set',
			'title'       => 'Inner or Outer Shadow?',
			'default'       => 'inset',
			'options'     => array(
				'inset'  => 'Inner', 
				'outset' => 'Outer', 
			 ), 
			'required'      => array('shadow-enabled', 'equals', true),
		);

		$fields[] = array(
			'id'          => 'shadow-location',
			'title'       => 'Shadow Location',
			'type'        => 'button_set',
			'default'     => 'both',
			'options'     => array(
				'both'   => 'Both', 
				'top'    => 'Top',
				'bottom' => 'Bottom', 
			 ), 
			'required'      => array('shadow-enabled', 'equals', true),
		);


		if( $current_post_meta[ "shadow-enabled" ] ){
			$icon = 'el-icon-star';
		} else{
			$icon = 'el-icon-star-empty';
		}
		// add fields to boxsections
		$boxSections[] = array(
			'icon' => $icon,
			'fields' => $fields,
			'id' => $key.'-shadow',
			'title' => 'Shadows',
		);

		// Reset for next meta box
		$metaboxes[] = array(
			'id' => 'metabox-band-shadow',
			'title' => 'Fancy Options',
			'post_types' => array('sms_band'),
			'sections' => $boxSections,
		);

		$fields = array();
		$boxSections = array();




		// //=========================================
		/////////////////////////////////////////////////


		// //=========================================
		// // Custom Markup
		// //-----------------------------------------
		// $fields[] = array(
		// 	'id'        => 'band-markup-before',
		// 	'type'      => 'ace_editor',
		// 	'title'     => 'Custom Markup (Before Band)',
		// 	'mode'      => 'html',
		// 	'theme'     => 'monokai',
		// 	'subtitle'  => 'Paste your HTML & Javascript here.',
		// 	'desc'      => 'This markup will appear before the band markup. Useful for adding elements between sections, or for adding inline Javascript that only renders if this band is used.',
		// 	'default'   => "",
		// );

		// $fields[] = array(
		// 	'id'        => 'band-markup-after',
		// 	'type'      => 'ace_editor',
		// 	'title'     => 'Custom Markup (After Band)',
		// 	'mode'      => 'html',
		// 	'theme'     => 'monokai',
		// 	'subtitle'  => 'Paste your HTML & Javascript here.',
		// 	'desc'      => 'This markup will appear before the band markup. Useful for adding elements between sections, or for adding inline Javascript that only renders if this band is used.',
		// 	'default'   => "",
		// );

		// //Create a Meta Box and reset the meta box array
		// //-----------------------------------------
		// $boxSections[]['fields'] = $fields;
		// $metaboxes[] = array(
		// 		'id' => 'band-markup',
		// 		'title' => 'Custom Markup & CSS (LESS)',
		// 		'post_types' => array('sms_band'),
		// 		'sections' => $boxSections
		// );
		// $fields = array();
		// $boxSections = array();
		// //=========================================
		// /////////////////////////////////////////////////


		//=========================================
		// Background
		//-----------------------------------------
		$fields[] = array(
			'id'            => 'background-type',
			'title'         => 'Background Type',
			'type'          => 'select',
			'default'       => 'solid_color',
			'options'       => array(
				'none' => 'No Background',
				'background-color'         => 'Solid Color (Custom Color)',
				'background-color-global'  => 'Solid Color (Global Colors)',
				'background-image'         => 'Color + Texture / Image (Custom)',
				'background-library'       => 'Pattern Library',
				'background-gradient'      => 'Gradient',
				// 'background-parallax'      => 'Parallax',
				// 'background-fixed'         => 'Fixed Position',
			)
		);

		//background color.
		$fields[] = array(
			'id'            => 'background-color',
			'title'         => 'Background Color (Custom Color)',
			'type'          => 'color',
			'default'       => '#FFFFFF',
			'required'      => array('background-type', 'equals', 'background-color'),
		);

		//background color (global colors).
		$fields[] = array(
			'id'            => 'background-color-global',
			'title'         => 'Background Color (Global Color)',
			'type'          => 'select',
			'default'       => '#FFFFFF',
			'options'       => $sms_utils->sms_options['colors']['color-name-list'],
			'required'      => array('background-type', 'equals', 'background-color-global'),
		);
		$fields[] = array(
			'id'            => 'background-color-brightness-global',
			'title'         => 'Brightness',
			'type'          => 'select',
			'options'       => $sms_utils->sms_options['colors']['brightness-list'],
			'required'      => array('background-type', 'equals', 'background-color-global'),
		);

		// echo "<pre>\$sms_utils->sms_options['colors']['color-name-list']: " . print_r($sms_utils->sms_options['colors']['color-name-list'], true) . "</pre>";
		// echo "<pre>\$sms_utils->sms_options['colors']: " . print_r($sms_utils->sms_options['colors'], true) . "</pre>";
		
		////////////////////////////////////////////////////////////////////////////////////
		//background color & image settings.
		$fields[] = array(
			'id'            => 'background-image',
			'title'         => 'Background Texture',
			'type'          => 'background',
			'preview_media' => true,
			'preview'       => true,
			'default'       => '#FFFFFF',
			'required'      => array('background-type', 'equals', 'background-image'),
		);

		$fields[] = array(
			'id'            => 'background-image--overlay-enabled',
			'title'         => 'Enable color overlay?',
			'type'          => 'switch',
			'default'       => false,
			'required'      => array('background-type', 'equals', 'background-image'),
		);

		$fields[] = array(
			'id'             => 'background-image--overlay-opacity',
			'title'          => 'Background overlay opacity',
			'type'           => 'slider',
			'desc'           => '0-100%',
			// "default"        => 50,
			"min"            => 0,
			"step"           => 1,
			"max"            => 100,
			'display_value'  => 'text',
			'required'       => array('background-image--overlay-enabled', 'equals', true),
		);

		// overlay color.
		$fields[] = array(
			'id'            => 'background-image--overlay-color',
			'title'         => 'Background Overlay Color',
			'transparent'   => false,
			'type'          => 'color',
			'default'       => '#FFFFFF',
			// 'required'      => array('background-type', 'equals', 'background-image'),
			'required'      => array('background-image--overlay-enabled', 'equals', true),
		);
		////////////////////////////////////////////////////////////////////////////////////

		// Gradient
		$fields[] = array(
			'id'       => 'background-gradient',
			'type'     => 'color_gradient',
			'title'    => 'Gradient Background',
			'validate' => 'color',
			'default'  => array(
				'from' => '#333',
				'to'   => '#999',
			),
			'required' => array('background-type', 'equals', 'background-gradient')
		);

		$fields[] = array(
			'id'            => 'background-library',
			'type'          => 'select_image',
			'title'         => 'Background Texture Library',
			'subtitle'      => 'Select a pattern from the subtle patterns library.',
			'required'      => array('background-type', 'equals', 'background-library'),
			'options'       => $sms_utils->sms_options['patterns']['subtle_patterns_list']
		);


		// // Parallax & Fixed background
		// $fields[] = array(
		// 	'id'    => 'feature_not_available',
		// 	'type'  => 'info',
		// 	'title' => 'Feature Not Yet Available',
		// 	'style' => 'warning',
		// 	'desc'  => 'Stay tuned for updates.',
		// 	'required' => array( 'background-type', 'equals', array('background-parallax', 'background-fixed') )
		// );


		//Create a Meta Box and reset the meta box array
		//-----------------------------------------

		// add fields to boxsections
		$boxSections[]['fields'] = $fields;

		$metaboxes[] = array(
				'id' => 'sms-background',
				'title' => 'Background',
				'post_types' => array('sms_band'),
				'sections' => $boxSections
		);

		//Reset for next meta box
		$fields = array();
		$boxSections = array();
				
		//=========================================
		/////////////////////////////////////////////////


		return $metaboxes;
	}
	add_action('redux/metaboxes/sms_redux/boxes', 'redux_add_metaboxes');

endif;

///////////////////////////////////////////////////////////////////////////
// Add a little meta box to indicate whether this band is actively selected
///////////////////////////////////////////////////////////////////////////
add_action('redux/metaboxes/sms_redux/boxes', function( $metaboxes ){
	$post_id = intval( $_GET['post'] );
	$post_type = get_post_type( $post_id );

	// If this isn't a band post type, then skip the rest.
	if($post_type != 'sms_band'){
		return false;
	}

	global $sms_utils;
	global $sms_redux;
	
	$i = 0;
	$numbers = false;
	$shade = false;
	$active_slots = '';
	foreach ($sms_utils->get_band_slot_data() as $value) {

		if( $value['id'] == $post_id ){
			$numbers = $value[ 'slot-number' ];
			$shade = ucwords($value['shade']) . " slot";

			if($i == 0){
				$active_slots = $numbers;
			} else {
				if($i == 2) $shade .= 's';
				$active_slots .= ", ".$numbers;
			}
		}
		
		$i++;
	}

	if($numbers){
		$assignment_message = "This band is currently assigned to: <strong>$shade $numbers</strong>";
	} else {
		$assignment_message = "This band is not currently assigned to a slot.";
	}

	$fields[] = array(
		'id'            => 'current_slot',
		'title'         => 'Slot Assignment',
		'type'          => 'raw',
		'content'       => '<p>'.$assignment_message.'</p>',
	);

	// Create a Meta Box and reset the meta box array
	//-----------------------------------------

	// Add fields to boxsections
	$boxSections[]['fields'] = $fields;

	$metaboxes[] = array(
		'id' => 'current-slot-metabox',
		'title' => 'Is this band assigned?',
		'position' => 'side',
		'post_types' => array('sms_band'),
		'sections' => $boxSections
	);
	return $metaboxes;
});
