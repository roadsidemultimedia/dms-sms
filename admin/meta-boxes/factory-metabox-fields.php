<?php
class smsFactoryMetaboxes{

	public function __construct($factory_type){
		add_action('redux/metaboxes/sms_redux/boxes', array($this, 'add_redux_metaboxes'), 10);
		$this->factory_type = $factory_type;
	}

	public function add_redux_metaboxes($metaboxes, $suppress = false) {

		$post_id = intval( $_GET['post'] );
		$post_type = get_post_type( $post_id );
		if( $post_type !== "sms_".$this->factory_type ){
			// exit if this isn't the correct post type
			return $metaboxes;
		}

		global $sms_redux;
		global $sms_utils;
		global $sms_factory_data;
		$current_post_id = intval( $_GET['post'] );
		$current_post_meta = $sms_utils->get_redux_post_meta( $current_post_id , 'sms_redux', true);
		$dms_fields = new smsDmsFieldFactory($current_post_meta);

		// Generator Fields
		$metaboxes[] = $dms_fields->get_metabox_field_generator($this->factory_type);
		
		// Repeater Generator Fields
		$metaboxes[] = $dms_fields->get_metabox_field_generator_repeater($this->factory_type);

		// Custom Markup
		$metaboxes[] = $dms_fields->get_metabox_custom_markup($this->factory_type);

		// Global Markup
		$metaboxes[] = $dms_fields->get_metabox_global_custom_markup($this->factory_type);

		return $metaboxes;
	}

}
global $sms_utils;
foreach ($sms_utils->sms_options['factory_types'] as $value) {
	$sms_slider_metaboxes = new smsFactoryMetaboxes($value);
	
}