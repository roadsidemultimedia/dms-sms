<?php

// Temporary field data for later use

// ============================================================================
// Letter Spacing
// ============================================================================

$fields[] = array(
  'id' => 'letter-spacing',
  'title' => 'Letter Spacing',
  'subtitle' => "1 = 0.01em";
  'type' => 'spinner',
  "default" => 0;
  "min" => 0;
  "step" => 1;
  "max" => 100;
  'display_value' => 'text',
);


// ============================================================================
// Line Height
// ============================================================================

$fields[] = array(
  'id' => 'line-height',
  'title' => 'Line Height',
  'type' => 'slider',
  "default" => 0;
  "min" => 0;
  "step" => 1;
  "max" => 100;
  'display_value' => 'text',
);

// ============================================================================
// Text Alignment
// ============================================================================

$fields[] = array(
  'id' => 'text-align',
  'title' => 'Text Alignment',
  'type' => 'select',
  'options' => array(
    '1' => 'Inherit',
    '2' => 'Left',
    '3' => 'Right',
    '4' => 'Center',
    '5' => 'Justify',
    '6' => 'Initial',
);

// ============================================================================
// Text Transform
// ============================================================================

$fields[] = array(
  'id' => 'text-transform',
  'title' => 'Text Transform',
  'type' => 'select',
  'options' => array(
    '1' => 'None',
    '2' => 'Capitalize',
    '3' => 'Uppercase',
    '4' => 'Lowercase',
    '5' => 'Initial',
    '6' => 'Inherit',
  ),
);
