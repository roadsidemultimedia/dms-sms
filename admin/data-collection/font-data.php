<?php

//////////////////////////////////////////////////////////////////
// Get list of all font kit posts available for global selection
// Store the list in our global options array
//////////////////////////////////////////////////////////////////

$sms_utils->sms_options['fonts']['kit-name-list'] = $sms_utils->list_posts_as_redux_select_array('sms_font_kit');



//////////////////////////////////////////////////////////////////
// Modular font scale calculation
//////////////////////////////////////////////////////////////////

$font_ratio = $sms_redux['font-scale-ratio'] ?: 1.618;
$font_base = $sms_redux['font-scale-base'] ? intval( $sms_redux['font-scale-base'] ) : 16;
$font_scale_array = $sms_utils->get_font_scale_array($font_base, $font_ratio);
$font_scale_array_float = $sms_utils->get_font_scale_array($font_base, $font_ratio, false);

// A list of sizes. Numbers are rounded to nearest number with one decimal place
$sms_utils->sms_options['fonts']['size-integer-list'] = $font_scale_array;

// A list of sizes. Numbers are not rounded
$sms_utils->sms_options['fonts']['size-float-list'] = $font_scale_array_float;



//////////////////////////////////////////////////////////////////
// Assemble lists of PX and EM sizes
// With and without names
//////////////////////////////////////////////////////////////////

$font_scale = intval( $sms_redux['font-scale-base']);
$base_font_size_relative = $sms_utils->sms_options['fonts']['size-integer-list']['medium'];
$base_font_size_int = ($base_font_size_relative * $font_scale);
$base_font_size = $base_font_size_int.'px';

foreach ( $sms_utils->sms_options['fonts']['size-name-reference-list'] as $key => $value) {
  
  $font_size_em = round($sms_utils->sms_options['fonts']['size-float-list'][$key], 3);
  $sms_utils->sms_options['fonts']['size-name-em-list'][$key] = "[${font_size_em}em] $value";
  $sms_utils->sms_options['fonts']['size-em-list'][$key] = $font_size_em .'em';

  $font_size_px = round($sms_utils->sms_options['fonts']['size-integer-list'][$key] * $font_scale);
  $sms_utils->sms_options['fonts']['size-name-px-list'][$key] = "[${font_size_px}px] $value";
  $sms_utils->sms_options['fonts']['size-name-px-class-list']['fs-'.$key] = "[${font_size_px}px] $value";
  $sms_utils->sms_options['fonts']['size-px-list'][$key] = $font_size_px .'px';

  $sms_utils->sms_options['fonts']['size-name-rem-list'][$key] = "[". ($font_size_px * 0.1) ."rem] $value";
  $sms_utils->sms_options['fonts']['size-rem-list'][$key] = ($font_size_px * 0.1) .'rem';

}

//////////////////////////////////////////////////////////////////
// Load in Font Kit
//////////////////////////////////////////////////////////////////
$kit_type = $font_kit_meta['font-kit-type'];
$kit_markup = '';

if( $kit_type == "typekit" ){
  // TYPEKIT
  $kit_markup .= "\n".'<script type="text/javascript" src="//use.typekit.net/'. $font_kit_meta['typekit-id'] .'.js"></script>'."\n";
  $kit_markup .= '<script type="text/javascript">try{Typekit.load();}catch(e){}</script>'."\n";
} elseif( $kit_type == "google" ){
  // GOOGLE
  $kit_markup = $font_kit_meta['google-font-code'];
} elseif( $kit_type == "custom" ){
  // CUSTOM
  $kit_markup = $font_kit_meta['font-kit-markup'];
}

//===============================================================
$sms_utils->sms_options['fonts']['font-kit-markup'] = $kit_markup;
