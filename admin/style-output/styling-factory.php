<?php
/**
 * Process LESS for all factory types
 */
add_action('admin_init', function(){
  $less_cache_bust = get_option('sms_less_cache_bust');
  if( $less_cache_bust || isset($_GET['compile']) ){
    global $sms_utils;
    $f_styles = new factoryStyles();
    $string = $f_styles->flatten_css_array();
    $sms_utils->sms_css[ 'factory_styles' ] = $string;
  }
});

// class smsSectionFactory{

//   public $slug;
//   public $name;
//   public $less;
//   public $css;
//   public $redux_fields;
//   public $dms_fields;

//   private $slots;

//   function __construct(){

//   }
//   // function __get(){

//   // }
//   function compile(){
//     // compile less to css
//   }
//   function collect_less(){
//     // gather less from posts into an array with the post id as key
//   }
//   function comment_css(){
//     // add comments to each chunk off css to indicate where it came from and whatnot
//   }
//   function register_cpt($slug){

//   }
// }

// class smsSectionFactoryPost extends smsSectionFactory{

// }

class factoryStyles{

  function __construct(){
    global $sms_utils;
    $this->sms_utils = $sms_utils;
    // $this->compiled_css = $this->compile_posts( $this->get_less_array() );
    // $this->style_array = $this->get_less_array();
    
  }

  function flatten_css_array(){
    global $sms_utils;
    $p_css = '';
    foreach ($this->get_less_array() as $factory_type => $data) {
      $css_factory_chunk = '';
      foreach ($data as $id => $css) {
        $css_factory_chunk .= $css;
      }
      if($css_factory_chunk){
        
        $p_css .= $sms_utils->dev_mode ? "\n\n/* ========== ".strtoupper($factory_type)."S =============================== */\n\n" : '';
        $p_css .= "$css_factory_chunk";
        $p_css .= $sms_utils->dev_mode ? "\n\n/* ---------- end ".($factory_type)."s ------------------------------- */\n\n" : '';
      }
    }
    return $p_css;
  }

  function get_less_array(){
    global $sms_utils;
    global $sms_redux;

    $factory_types = array(
      'button'             => SMS_BUTTON_SLOTS,
      'card'               => SMS_CARD_SLOTS,
      'divider'            => SMS_DIVIDER_SLOTS,
      'slider'             => SMS_SLIDER_SLOTS,
      'accordion'          => SMS_ACCORDION_SLOTS,
      'quote'              => SMS_QUOTE_SLOTS,
      'list'               => SMS_LIST_SLOTS,
      'grid'               => SMS_GRID_SLOTS,
      'header'             => SMS_HEADER_SLOTS,
      'header_template'    => SMS_HEADER_TEMPLATE_SLOTS,
    );

    $v = array();
    $style_array = array();
    foreach ($factory_types as $factory_slug => $slot_total) {

      $style_array[ $factory_slug ] = array();
      for ($i=1; $i <= $slot_total; $i++) { 
        
        $slot_slug = "$factory_slug-slot$i";
        $id = $sms_redux[$slot_slug];

        // skip compiling css if the id has already been processed
        if( $id && array_key_exists($id, $v) == false ){

          $meta             = $sms_utils->get_redux_post_meta( $id );
          $raw_less         = $meta['less'];
          $title            = get_the_title($id);
          $post_edit_url    = get_edit_post_link( $id );
          $compiled_less = $sms_utils->compile_less( $raw_less, $slot_slug. ": <a href='".$post_edit_url."'>".$title."</a>" );

          $css= '';
          if($compiled_less){
            $css .= $sms_utils->dev_mode ? "\n/* --- ".$slot_slug." post id: ".$id." --- */\n\n" : '';
            $css .= $compiled_less;
          }

          $v[ $id ] = array(
            'id'            => $id,
            'slot_num'      => $i,
            'slot_slug'     => $slot_slug,
            'title'         => $title,
            'post_edit_url' => $post_edit_url,
            'css' => $css,
          );

          $style_array[ $factory_slug ][ $id ] = $css;

        }

      }

    }
    return $style_array;
  } 
}