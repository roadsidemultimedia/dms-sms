<?php

add_action('admin_init', function(){

	$less_cache_bust = get_option('sms_less_cache_bust');
	if( $less_cache_bust || isset($_GET['compile']) ){

		$sms_options = get_option('sms_options');

		global $sms_redux;
		global $sms_utils;
		// $active_color_scheme_id = $sms_redux['color-scheme'];
		// $color_scheme_meta = $sms_utils->get_redux_post_meta( $active_color_scheme_id, 'sms_redux', true );
		$color_scheme_meta = $sms_utils->global_selected_color_scheme_meta;

		$font_assignment_selectors = array(
		  'ass-light-body-color' => '.sms-light',
		  'ass-dark-body-color' => '.sms-dark',

		  'ass-light-heading-color'  => '.sms-light .sms-heading',
		  'ass-dark-heading-color'   => '.sms-dark .sms-heading',

		  'ass-light-link-color'     => '.sms-light a',
		  'ass-dark-link-color'      => '.sms-dark a',
		);

		global $css_selectors;
		$css_selectors = new CssSelectors();

		$css_debug_array = array();

		foreach ($font_assignment_selectors as $assignment => $selector) :
			
			// parse through color scheme post's meta info. 
			// filter for color assignments, exclude color brightnesses
			if (strpos($assignment,'ass-') !== false && strpos($assignment,'brightness') == false ){
				$definition = $color_scheme_meta[ $assignment ];
				$color = $color_scheme_meta[$definition];
				$color = $definition ? '@'.str_replace('def-', '', $definition) : false;
				
				$brightness = $color_scheme_meta[ $assignment.'-brightness' ];

				// Tack on the brightness variant if one has been chosen.
				if( $brightness && $color ) $color .= "-$brightness";

				$css_debug_array[] = array (
					'assignment'  => $assignment,
					'definition'  => $definition,
					'selector'    => $selector,
					'color'       => $color,
					'brightness'  => $brightness,
				);

				$css_selectors->get($selector);
				$css_selectors->get($selector)->add( 'color', $color );

				if( $assignment == 'ass-light-link-color' || $assignment == 'ass-dark-link-color' ){

					$light = "mix($color, #fff, 60%)";
					$dark = "mix($color, #000, 60%)";
					$hover = "contrast($color, $light, $dark, 50%)";

					if($color){
						$css_selectors->get("$selector:visited")->add( 'color', $color );
						$css_selectors->get("$selector:hover")->add( 'color', $hover );
					}

				}

			}

		endforeach;

		// $sms_utils->admin_notices['updated'][] = "<pre>\$css_debug_array: " . print_r($css_debug_array, true) . "</pre>";

		$raw_css = $css_selectors->get_css();
		$compiled_less = $sms_utils->compile_less( $raw_css, "styling-color.php" );

		// If the compilation was successful wrap it in comments that indicate 
		// where the CSS came from and save to database
		if( $compiled_less ){
			$css = $sms_utils->dev_mode ? "\n\n/* ========== Color Scheme ========================= */\n\n" : '';
			$css .= $compiled_less;
			$css .= $sms_utils->dev_mode ? "/* ================================================== */\n\n" : '';

			$sms_utils->sms_css['color-styles'] = $css;
		} else {
			// Clear the database entry if there's no CSS to compile.
			$sms_utils->sms_css['color-styles'] = '';
		}

		// Clear legacy LESS DB entry
		$sms_utils->sms_less['color-styles'] = '';

	}

});
