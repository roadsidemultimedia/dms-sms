<?php

add_action('admin_init', function(){

	$less_cache_bust = get_option('sms_less_cache_bust');
	if( $less_cache_bust || isset($_GET['compile']) ){

		$sms_options = get_option('sms_options');
		global $sms_redux;
		global $sms_utils;

		$font_assignment_selectors = array(
		  'body' => '.sms-band',
		  'heading--primary' => '.sms-band .sms-heading--primary',
		  'heading--secondary' => '.sms-band .sms-heading--secondary',
		  'heading--tertiary' => '.sms-band .sms-heading--tertiary',
		  'quote' => '.sms-band blockquote.sms-quote',
		);
		
		// Add the css data into the CSS processing class
		$css_selectors = new CssSelectors();

		$font_properties = $sms_utils->get_font_assignments();
		$css_selectors->get('body');
		$css_selectors->get('body')->add( 'font-family', $font_properties['body']['font-family'] );

		foreach ($sms_utils->get_font_assignments() as $key => $font_properties) :

			// $key = body, heading--primary
			// $font_properties = array (
			//     [font-family] => "Open Sans 9000"
			//     [font-size] => 3.2rem
			//     [font-weight] => 600
			// )
			// 

			$selector = $font_assignment_selectors[ $key ];
			$font_family = $font_properties['font-family'];
			$font_weight = $font_properties['font-weight'];

			$css_selectors->get($selector);
			$css_selectors->get($selector)->add( 'font-family', $font_family );
			$css_selectors->get($selector)->add( 'font-weight', $font_weight );
			
			if($font_properties['font-size'] && $key !== 'body'){
				$font_size = $font_properties['font-size'];
				$css_selectors->get($selector)->add( 'font-size', $font_size );
			}

		endforeach;

		$raw_css = $css_selectors->get_css();
		$compiled_less = $sms_utils->compile_less( $raw_css, "styling-global-fonts.php" );

		// If the compilation was successful wrap it in comments that indicate 
		// where the CSS came from and save to database
		if( $compiled_less ){
			$css = $sms_utils->dev_mode ? "\n\n/* ========== Global Fonts ========================= */\n\n" : '';
			$css .= $compiled_less;
			$css .= $sms_utils->dev_mode ? "/* ================================================== */\n\n" : '';
			$sms_utils->sms_css['global-font-styles'] = $css;
		} else {
			// Clear the database entry if there's no CSS to compile.
			$sms_utils->sms_css['global-font-styles'] = '';
		}

		// Clear legacy LESS DB entry
		$sms_utils->sms_less['global-font-styles'] = '';

	}

}, 10);
