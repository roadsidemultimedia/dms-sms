<?php

// Set the LESS compiler to bust the cache on the save of a post
add_action( 'save_post', 'sms_save_post_hook', 20, 1 );

// Set less compiler to bust cache when Redux field is saved
add_filter('redux/options/sms_redux/compiler', 'sms_save_post_hook', 10, 3);

function sms_save_post_hook( $post_id ) {
  global $sms_utils;
  $post_type = get_post_type( $post_id );

  // Skip recompile if it's a page or a post.
  if( $post_type == 'page' || $post_type == 'post' ){
    // If it's an array, it's probably a Redux page. Go ahead and bust the cache.
    if( is_array( $post_id ) ){
      update_option('sms_less_cache_bust', true);
      return;
    } else {
      return;
    }
  }
  // update_option('sms_front_end_cache_bust', true);
  update_option('sms_less_cache_bust', true);
}

// Write SMS CSS to file
add_action('admin_init', function(){
  
  global $sms_utils;

  if( get_option('sms_less_cache_bust') || isset($_GET['compile']) ){

    // Write the css string to a css file
    $style_factory_compiler = new styleChunkFactory();
    $style_factory_css = $style_factory_compiler->compile_css();
    $css_string = $sms_utils->css_array_to_string($sms_utils->sms_css);
    $css_string .= $style_factory_css;
    $sms_utils->save_css_string_to_file("sms-css.css", $css_string);
    $sms_utils->admin_notices['updated'][] = 'SMS LESS Successfully Recompiled';

    update_option('sms_less_cache_bust', false);
  }

  // Flush CSS DB entries if query post set to '?flush-sms-css=true'
  if( isset($_GET['flush-sms-css']) ){
    $sms_utils->sms_css = array();
    $css_string = '/* Flushed   */ /';
    $sms_utils->save_css_string_to_file("sms-css.css", $css_string);
    $sms_utils->admin_notices['updated'][] = 'SMS CSS database entries flushed';
  }

}, 11);

add_action('wp_enqueue_scripts', function(){

  global $sms_utils;
  wp_enqueue_style( 'sms-css', $sms_utils->get_upload_folder('uri')."sms-css.css", 'DMS-theme-css' );

}, 21);

class styleChunkFactory{

  function __construct(){
    global $sms_utils;
    $this->sms_utils = $sms_utils;
    $this->redux_slots = $sms_utils->filter_redux_array('style-factory-');
    // $this->compiled_css = $this->compile_posts( $this->get_enabled_posts() );
    // return $this->compile_posts( $this->get_enabled_posts() );
  }

  function get_enabled_posts(){
    $enabled_posts = array();
    foreach ($this->redux_slots as $slug => $enabled) {
      $id = str_replace('style-factory-', '', $slug);
      if($enabled)
        $enabled_posts[$id] = get_the_title($id);
    }
    return $enabled_posts;
  }

  function compile_css(){

    global $sms_utils;

    $posts = $this->get_enabled_posts();
    $css = '';
    $css .= $sms_utils->dev_mode ? "\n/* ========== Style Chunks ========================== */\n\n" : '';
    foreach ($posts as $id => $title) {
    
      $post_meta = $this->sms_utils->get_redux_post_meta($id);
      $raw_less = $post_meta['less'];
      $post_edit_url = get_edit_post_link( $id );
      $compiled_less = $this->sms_utils->compile_less( $raw_less, "<a href='".$post_edit_url."'>".$title."</a>" );

      // If the compilation was successful wrap it in comments that indicate 
      // where the CSS came from and save to database. 
      // It will be written to a CSS file from styling-functions.php
      if($compiled_less){
        $css .= $sms_utils->dev_mode ? "\n/* --- ".$title." / post id: ".$id." --- */\n" : '';
        $css .= $compiled_less;
        // $css .= "/* ---------------------------------- */\n";
      }

    }
    $css .= $sms_utils->dev_mode ? "\n/* -------------------------------------------------- */\n\n" : '';
    return $css;

  }

}
