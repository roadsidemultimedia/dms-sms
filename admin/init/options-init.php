<?php
if (!class_exists('sms_Redux_Framework_config')) {

	class sms_Redux_Framework_config {

		public $args        = array();
		public $sections    = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if (!class_exists('ReduxFramework')) {
				return;
			}

			$this->initSettings();

		}

		public function initSettings() {

			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Create the sections and fields
			$this->setSections();

			if (!isset($this->args['opt_name'])) { // No errors please
				return;
			}

			// If Redux is running as a plugin, this will remove the demo notice and links
			add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

			// // Function to test the compiler hook and demo CSS output.
			// // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
			// add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);
			
			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
		}

		// function compiler_action($options, $css, $changed_values) {
		// 	echo '<h1>The compiler hook has run!</h1>';
		// 	echo "<pre>";
		// 	print_r($changed_values); // Values that have changed since the last save
		// 	echo "</pre>";
		// 	//print_r($options); //Option values
		// 	//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

			
		// 	// Demo of how to use the dynamic CSS and write your own static CSS file
		// 	$filename = DMS_SMS_PATH . '/style' . '.css';
			
		// 	global $wp_filesystem;
		// 	if( empty( $wp_filesystem ) ) {
		// 		require_once( ABSPATH .'/wp-admin/includes/file.php' );
		// 		WP_Filesystem();
		// 	}

		// 	if( $wp_filesystem ) {
		// 		$wp_filesystem->put_contents(
		// 			$filename,
		// 			$css,
		// 			FS_CHMOD_FILE // predefined mode settings for WP files
		// 		);
		// 	}

		// }



		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if (class_exists('ReduxFrameworkPlugin')) {
				remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

				// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
				remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
			}
		}

		public function setSections() {

			// Grab global Redux option field
			global $sms_redux;
			if (!isset($GLOBALS['sms_redux'])) {
				$GLOBALS['sms_redux'] = get_option('sms_redux', array());
			}
			
			// Load meta boxes
			foreach ( glob( dirname( dirname( __FILE__ ) ) . "/meta-boxes/*" ) as $filename) {
				include_once( $filename );
			}

		}

		public function setArguments() {

			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
				'opt_name' => 'sms_redux',
				'dev_mode' => 'false',
				// 'page_slug' => 'sms_redux',
				'page_slug' => 'dms-sms.php',
				'page_title' => 'Style Management System',
				'update_notice' => '1',
				'admin_bar' => '0',
				'menu_type' => 'menu',
				'menu_title' => 'Style Management',
				'allow_sub_menu' => '0',
				'page_priority' => '1',
				'default_show' => '1',
				'default_mark' => '*',
				'google_api_key' => 'AIzaSyBr-nPw6GoHVdh2OBqvzRrxy3NDkDOkdog',
				'hints' => 
				array(
					'icon' => 'el-icon-question-sign',
					'icon_position' => 'right',
					'icon_size' => 'normal',
					'tip_style' => 
					array(
					'color' => 'light',
					),
					'tip_position' => 
					array(
					'my' => 'top left',
					'at' => 'bottom right',
					),
					'tip_effect' => 
					array(
					'show' => 
					array(
						'duration' => '500',
						'event' => 'mouseover',
					),
					'hide' => 
					array(
						'duration' => '500',
						'event' => 'mouseleave unfocus',
					),
					),
				),
				'output_tag' => '1',
				'compiler' => '1',
				'global_variable' => 'sms_redux',
				'page_icon' => 'icon-themes',
				'page_permissions' => 'manage_options',
				'save_defaults' => '1',
				'show_import_export' => '1',
				'transient_time' => '3600',
				);

		}

	}
	
	// new sms_Redux_Framework_config();
}
