<?php

// Load the TGM init if it exists
if (file_exists(dirname(__FILE__).'/tgm/tgm-init.php')) {
 require_once( dirname(__FILE__).'/tgm/tgm-init.php' );
}
add_action('init', function(){
  
  // Load Redux extensions - MUST be loaded before your options are set
  $redux_extentions_init_path = dirname(__FILE__).'/redux-extensions/extensions-init.php';
  if (file_exists( $redux_extentions_init_path )) {
    require_once( $redux_extentions_init_path );
  }

  // Load the theme/plugin options
  $options_path = dirname(__FILE__).'/options-init.php';
  if (file_exists( $options_path )) {
    require_once( $options_path );
  }

  global $reduxConfig;
  $reduxConfig = new sms_Redux_Framework_config();

}, 9);