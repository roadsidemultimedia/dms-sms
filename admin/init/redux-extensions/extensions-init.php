<?php

$redux_opt_name = "sms_redux";

// Replace {$redux_opt_name} with your opt_name.
// Also be sure to change this function name!

if(!function_exists('redux_register_sms_extension_loader')) :
if( is_admin() ) :
	function redux_register_sms_extension_loader($ReduxFramework) {
		$path = dirname( __FILE__ ) . '/extensions/';
		$folders = scandir( $path, 1 );        
		foreach($folders as $folder) {
			if ($folder === '.' or $folder === '..' or !is_dir($path . $folder) ) {
				continue;   
			} 
			$extension_class = 'ReduxFramework_Extension_' . $folder;
			if( !class_exists( $extension_class ) ) {
				// In case you wanted override your override, hah.
				$class_file = $path . $folder . '/extension_' . $folder . '.php';
				$class_file = apply_filters( 'redux/extension/'.$ReduxFramework->args['opt_name'].'/'.$folder, $class_file );
				// echo "<pre>\$class_file: " . print_r($class_file, true) . "</pre>";
				if ( file_exists( $class_file ) ) {
					require_once( $class_file );
					$extension = new $extension_class( $ReduxFramework );
				}
			}
		}
	}
	add_action("redux/extensions/{$redux_opt_name}/before", 'redux_register_sms_extension_loader', 0);
endif;
endif;
