<?php
// Register "sms_band" Custom Post Type
add_action( 'init', 
	function(){
		$args = array(
			'labels' => array(
				'name' => __( 'Banding Styles' ),
				'singular_name' => __( 'Banding Style' )
			),
			'hierarchical' => false,
			'supports' => array( 'title' ),
			'public' => false,
			'show_ui' => true,
			'show_in_menu'  => 'dms-sms.php',
			'menu_position' => 10,
			'show_in_nav_menus' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'has_archive' => false,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => false,
			'capability_type' => 'page',
			'menu_icon' => 'dashicons-menu',
		);

		register_post_type( 'sms_band', $args );
	}, 99
);



// Register "Shade" Taxonomy
add_action( 'init', 
	function() {
		$labels = array(
			'name'                       => 'Shades',
			'singular_name'              => 'Shade',
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => false,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => false,
			'show_tagcloud'              => false,
			'show_ui'                    => true,

			// The following arguments require the Single Value Taxonomy UI plugin
			// http://wordpress.org/plugins/single-value-taxonomy-ui/
			'required'                   => true,
			'single_value'               => true
		);
		register_taxonomy( 'sms_shade', array( 'sms_band' ), $args );
	}
,0);

// Add 'Dark' and 'Light' default terms
add_action('init', function(){

  $taxonomy = "sms_shade";

  $term = "Dark Background [Light Text]"; 
  $slug = "dark"; 
  if ( !term_exists( $term, $taxonomy ) ){
    wp_insert_term( $term, $taxonomy, array( 'slug' => $slug ) );
  }
  $term = "Light Background [Dark Text]"; 
  $slug = "light"; 
  if ( !term_exists( $term, $taxonomy ) ){
    wp_insert_term( $term, $taxonomy, array( 'slug' => $slug ) );
  }

});


// http://justintadlock.com/archives/2011/06/27/custom-columns-for-custom-post-types
add_filter( 'manage_edit-sms_band_columns', 

	function( $columns ) {

		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Bands' ),
			'shade' => __( 'Shade' ),
			'date' => __( 'Date' )
		);

		return $columns;
	}
);

// add_action('init',
// 	function(){
// 		$terms = wp_get_object_terms( 661, 'sms_shade' );
// 		echo "<pre>hello" . print_r($terms, true) . "</pre>";
// 	}
// );


// // http://justintadlock.com/archives/2011/06/27/custom-columns-for-custom-post-types
function sms_shade_column( $column, $post_id ) {
	global $post_id;
	switch( $column ) {

		/* If displaying the 'shade' column. */
		case 'shade' :

			$terms = get_the_terms( $post_id, 'sms_shade' );

			/* If terms were found. */
			if ( !empty( $terms ) ) {
				foreach ( $terms as $term ) {
				 _e($term->name);
				}
			}

			/* If no terms were found, output a default message. */
			else {
				_e( 'No Shade' );
			}

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
};
add_action( 'manage_sms_band_posts_custom_column', 'sms_shade_column',10,2 );


// // Load in custom CSS for post list view
// // http://fasttrak1.wpengine.com/wp-admin/edit.php?post_type=sms_band
// add_action( 'load-edit.php',
// 	function(){
// 		$screen = get_current_screen();
// 		if ( 'sms_band' != $screen->post_type ) {
// 			return;
// 		}
// 		add_action( 'admin_print_styles', 
// 			function() {
// 				wp_enqueue_style( 'sms_band_css' );
// 			}
// 		);
// 	}
// );
