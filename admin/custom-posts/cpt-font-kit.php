<?php
// Register "sms_font_kit" Custom Post Type
add_action( 'init', 
	function(){
		$args = array(
			'labels' => array(
				'name' => __( 'Font Kits' ),
				'singular_name' => __( 'Font Kit' )
			),
			'hierarchical' => false,
			'supports' => array( 'title' ),
			'public' => false,
			'show_ui' => true,
			'show_in_menu'  => 'dms-sms.php',
			// 'show_in_menu' => true,
			'menu_position' => 10,
			'show_in_nav_menus' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'has_archive' => false,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => false,
			'capability_type' => 'page',
			'menu_icon' => 'dashicons-art',
		);

		register_post_type( 'sms_font_kit', $args );
	}, 99
);
