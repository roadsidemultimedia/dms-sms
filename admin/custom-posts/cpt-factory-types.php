<?php
add_action( 'init', 
  function(){
    $factory_types = array(
      'header' => "Header Style",
      'header_template' => "Header Template",
      'button' => "Button Template",
      'card' => "Card Template",
      'slider' => "Slider Template",
      'divider' => "Divider Template",
      'quote' => "Quote Template",
      'list' => "List Template",
      'accordion' => "Accordion Template",
      'grid' => "Grid Template",
      
      'style' => "Style Chunk",
    );
    foreach ($factory_types as $key => $title) {
      $args = array(
        'labels'              => array(
          'name'          => __( $title.'s' ),
          'singular_name' => __( $title )
        ),
        'hierarchical'        => false,
        'supports'            => array( 'title' ),
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => 'dms-sms.php',
        'menu_position'       => 10,
        'show_in_nav_menus'   => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'has_archive'         => false,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => false,
        'capability_type'     => 'page',
        'menu_icon'           => 'dashicons-art',
      );
      register_post_type( "sms_$key", $args );
    }
  }, 99
);