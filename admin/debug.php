<?php
add_action('admin_footer', function(){
  echo "<div style='clear:both; background: #191919; color: #6b695a; padding: 40px; padding-left: 180px;'>";
  echo "<style>.codehead{color: #b2b098}</style>";
  global $post;
  global $sms_redux;
  global $sms_utils;
  $sms_options = $sms_utils->sms_options;
  
  if( isset($_GET['flush-post-meta']) ){
    $post_meta = $sms_utils->get_redux_post_meta( $post->ID );
    foreach ($post_meta as $key => $value) {
      delete_post_meta($post->ID, $key);
    }
    $sms_utils->admin_notices['updated'][] = 'Post Meta Data Cleared!';
  }

  if($post){
    $post_meta = $sms_utils->get_redux_post_meta( $post->ID );
    unset($post_meta['markup']);
    unset($post_meta['less']);
    echo "<h1 class='codehead'>Post Meta Data</h2>";
    $post_meta_expanded = print_r($post_meta, true);
    echo "<pre>" . print_r( htmlspecialchars($post_meta_expanded), true ) . "</pre>";  
  }

  echo "<h1 class='codehead'>\$sms_redux</h2>";
  $sms_redux_expanded = print_r($sms_redux, true);
  echo "<pre>" . htmlspecialchars($sms_redux_expanded) . "</pre>"; 

  echo "<h1 class='codehead'>\$sms_options (without css data showing)</h2>";
  //
  $sms_options_no_css = $sms_options;
  $sms_options_no_css['sms-less'] = 'HIDDEN FOR LIGHTER DEBUG';
  $sms_options_no_css_expanded = print_r($sms_options_no_css, true);
  echo "<pre>" . htmlspecialchars($sms_options_no_css_expanded) . "</pre>";  

  ////////////////////////////////////////////////////////////////////////////////
  echo "</div>";
});
