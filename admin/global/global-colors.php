<?php
add_filter('redux/options/sms_redux/sections', function($sections){

  global $sms_utils;
	$sms_options = get_option('sms_options');

  $posts = $sms_utils->get_post_list_by_term_as_redux_field_array('sms_color');
  $color_schemes_numbered = array_keys( $posts );
  
  if($color_schemes_numbered){

  	$sections[] = array(
      'title' => 'Color Scheme',
      'desc' => '<p class="description">Choose your font kit and assign sizes and colors.</p>',
      'icon' => 'el-icon-adjust',
      'fields' => array(

        array(
          'id'        => 'color-scheme',
          'title'     => 'Color Scheme',
          'type'      => 'select',
          'desc'      => 'Choose the active color scheme for this site.',
          'options'   => $sms_utils->get_post_list_by_term_as_redux_field_array('sms_color'),
          // 'default'   => $color_schemes_numbered[0],
          'select2'   => array( 'allowClear'=>false ),
          'compiler'  => true,
        ),
      )
    );
  }

	return $sections;

});
