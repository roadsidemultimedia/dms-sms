<?php

add_filter('redux/options/sms_redux/sections', function($sections){

	global $sms_redux;
	global $sms_utils;

	///////////////////////////////////////////////////////////////////////////
	// Style Chunk Slots
	///////////////////////////////////////////////////////////////////////////
	
	$post_list = $sms_utils->list_posts_as_redux_select_array( 'sms_style' );
	$fields = array();
	foreach ($post_list as $id => $title) {
		$post_edit_url = "/wp-admin/post.php?post=".$id."&action=edit";
		$title = get_the_title($id);
		$fields[] = array(
			'title'     => "<a href='$post_edit_url'>$title</a>",
			'id'        => "style-factory-$id",
			'type'      => 'switch',
			'compiler'  => true,
		);
	}

	// Register Section
	$sections[] = array(
		'title' => "Style Chunks",
		'desc' => '<p class="description">Choose the style chunks that are available on the site.</p>',
		'icon' => 'el-icon-css',
		'fields' => $fields,
	);

	return $sections;
	
});
