<?php
add_filter('redux/options/sms_redux/sections', function($sections){

	$sms_options = get_option('sms_options');
	global $sms_redux;
	global $sms_utils;

	$dark_choices = $sms_utils->get_post_list_by_term_as_redux_field_array('sms_band', 'sms_shade', 'dark');
	$light_choices = $sms_utils->get_post_list_by_term_as_redux_field_array('sms_band', 'sms_shade', 'light');

	$args = array(
		'title'    => 'Band',
		'id'       => 'band',
		'type'     => 'select',
		'slots'    => SMS_BAND_SLOTS,
		'variants' => array(
			array(
				'id'      => 'dark',
				'title'   => 'Dark',
				'options' => $dark_choices,
				'desc'    => array('link_to_post_edit' => true),
			),
			array(
				'id'      => 'light',
				'title'   => 'Light',
				'options' => $light_choices,
				'desc'    => array('link_to_post_edit' => true),
			),
		),
		
	);

	function get_field_slot_variations($args){
		global $sms_redux;

		$result = array();
		foreach ($args['variants'] as $variant) {
			$i=1;
			// $choices_numbered = array_keys( $variant['options'] );
			$result[$variant['id']]['info'] = array(
				'id'    => $variant['id'],
				'title' => $variant['title'],
			);
			for ($i=1; $i <= $args['slots']; $i++) {
				if($variant['desc']['link_to_post_edit'] == true){
					$slug = $args['id']."-slot$i-".$variant['id'];
					$post_id = $sms_redux[ $slug ];
					if($post_id){
						$post_edit_url = "/wp-admin/post.php?post=".$post_id."&action=edit";
						$description = "<a target='_blank' href='".$post_edit_url."'>Edit <strong>" . get_the_title($post_id) . "</strong> (".$variant['title'].")</a>";
					} else {
						$description = '';
					}
				} else {
					$description = '';
				}

				$result[ $variant['id'] ]['fields'][$i] = array(
					'id'      => $args['id']."-slot$i-".$variant['id'],
					'title'   => $args['title']." #".$i." (".$variant['title'].")",
					'type'    => $args['type'],
					// 'select2' => array( 'allowClear'=>false ),
					'options' => $variant['options'],
					'desc'    => $description,
					'compiler'=> true,
					// 'default' => $default,
				);
			}
		}
		return $result;
	}

	$field_variation_array = get_field_slot_variations($args);

	$fields[] = array(
		'id' => 'dark-band-section-start',
		'title' => '♟ Dark Bands',
		'type' => 'section',
		'indent' => true,
	);

	foreach ($field_variation_array['dark']['fields'] as $field) {
		$fields[] = $field;
	}

	$fields[] = array(
		'id' => 'dark-section-section-end',
		'type' => 'section',
		'indent' => false,
	);

	$fields[] = array(
		'id' => 'light-band-section-start',
		'title' => '♙ Light Bands',
		'type' => 'section',
		'indent' => true,
	);

	foreach ($field_variation_array['light']['fields'] as $field) {
		$fields[] = $field;
	}

	$fields[] = array(
		'id' => 'light-section-section-end',
		'type' => 'section',
		'indent' => false,
	);

	$sections[] = array(
		'title' => 'Bands',
		'desc' => '<p class="description">Assign Bands for use throughout the site.</p>',
		'icon' => 'el-icon-align-justify',
		'fields' => $fields,
	);

	return $sections;
});
