<?php

add_filter('redux/options/sms_redux/sections', function($sections){

	global $sms_redux;
	global $sms_utils;
	$sms_options = $sms_utils->sms_options;

	if( $sms_options['fonts']['kit-name-list'] ){
		$font_kit_list_numbered = array_keys( $sms_options['fonts']['kit-name-list'] );
	} else {
		$font_kit_list_numbered = array();
	}

	$fields[] = array(
		'id'        => 'font-kit',
		'type'      => 'select',
		'title'     => 'Font Kit',
		'subtitle'  => 'Choose the active font kit for this site.',
		'options'   => $sms_options['fonts']['kit-name-list'],
		// 'default'   => $font_kit_list_numbered[0],
		'select2'   => array( 'allowClear'=>false ),
		'compiler'  => true,
	);

	$fields[] = array(
		'id'             => 'font-scale-base',
		'title'          => 'Body / Font Scale Base',
		'type'           => 'slider',
		'desc'           => 'This represents the body font size. All other fonts will be based on this size.',
		// "default"        => 16,
		"min"            => 14,
		"step"           => 1,
		"max"            => 20,
		'display_value'  => 'text',
		'compiler'       => true,
	);

	$fields[] = array(
		'id'             => 'font-scale-ratio',
		'title'          => 'Font Scale Ratio Type',
		'type'           => 'select',
		'desc'           => 'Choose the mathematical ratio used to calculate all font sizes.',
		'select2'        => array( 'allowClear'=>false ),
		'options'        => $sms_options['fonts']['size-ratios'],
		'compiler'       => true,
	);


	// This data needs to be assembled here due to load order issues with shared data.
	// If a shared data array with size data in it is used, 
	// it will require two reloads of the page to see the correct data
	// 
	$font_scale = intval( $sms_redux['font-scale-base']);
	$base_font_size_int = ($sms_utils->sms_options['fonts']['size-integer-list']['medium'] * $font_scale);
	$base_font_size = $base_font_size_int.'px';

	$font_scale_preview_output = '';
	$font_scale_preview_output .= "<div class='sms-font-preview'>";
	foreach ( $sms_utils->sms_options['fonts']['size-name-reference-list'] as $key => $value) {
		$font_size = round($sms_utils->sms_options['fonts']['size-integer-list'][$key] * $font_scale);
		$font_size_px = round($sms_utils->sms_options['fonts']['size-float-list'][$key] * $font_scale).'px';
		$font_size_em = round($sms_utils->sms_options['fonts']['size-float-list'][$key], 2).'em';
		$font_size_rem = ($font_size*0.1).'rem';

		$font_scale_preview_output .= "<p style='font-size: $font_size_px'>";
		$font_scale_preview_output .= "<span><strong>$value</strong><br>$font_size_px<br>$font_size_rem<br>$font_size_em<br>.font-{$key}</span>";
		// $font_scale_preview_output .= "<strong>The quick brown fox jumped over the lazy dogs</strong>";
		$font_scale_preview_output .= "<strong>Heading</strong>";

		$font_scale_preview_output .= "</p>";
	}
	$font_scale_preview_output .= "</div>";

	$fields[] = array(
		'id'             => 'font-scale-visual',
		'title'          => 'Visual Font Scale Preview',
		'type'           => 'raw',
		'content'        => $font_scale_preview_output."<br><br>",
	);


	// Register Section
	$sections[] = array(
		'title' => 'Typography',
		'desc' => '<p class="description">Choose your font kit and assign the global size scale.</p>
		<div style="color: #8E3C38;" class="updated below-h2"><p>In order to see changes to the font scale, you will need to refresh twice.
			<br>For the font sizes to take full effect, make sure you flush the DMS cache.</p></div>',
		'icon' => 'el-icon-font',
		'fields' => $fields,
	);

	


	return $sections;
});
