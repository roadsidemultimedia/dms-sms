<?php
add_filter('redux/options/sms_redux/sections', function($sections){

  global $sms_redux;
  global $sms_utils;
  $sms_options = get_option('sms_options');

  $stats = array(
    'Font Kit ID' => $sms_utils->global_selected_font_kit_id,
    'Font Kit Name' => $sms_utils->global_selected_font_kit_name,
    'Color Scheme ID' => $sms_utils->global_selected_color_scheme_id,
    'Color Scheme Name' => $sms_utils->global_selected_color_scheme_id,
  );

  // echo "<pre>\$stats: " . print_r($stats, true) . "</pre>";

  $field_generator_array = array(

    array(
      'id'        => 'color-styles-css',
      'title'     => '[CSS] Color Style',
      'data'      => $sms_utils->sms_css['color-styles'],
      'subtitle'  => '[color-styles]',
    ),
    array(
      'id'        => 'band-styles-css',
      'title'     => '[CSS] Band Style',
      'subtitle'  => '[band-styles]',
      'data'      => $sms_utils->sms_css['band-styles'],
    ),
    array(
      'id'        => 'global-font-styles-css',
      'title'     => '[CSS] Font Style',
      'subtitle'  => '[global-font-styles]',
      'data'      => $sms_utils->sms_css['global-font-styles'],
    ),
    array(
      'id'        => 'font-kit-markup',
      'title'     => 'Font Kit Markup Output',
      'subtitle'  => 'sms_options[\'fonts\'][\'font-kit-markup\']',
      'data'      => htmlentities($sms_utils->sms_options['fonts']['font-kit-markup']),
    ),
    array(
      'id'        => 'font-sizes-rem',
      'title'     => 'Font sizes REM array',
      'desc'  => '',
      'data'      => 'sms_options[\'fonts\'][\'size-rem-list\']<br>'.print_r($sms_utils->sms_options['fonts']['size-rem-list'], true),
    ),
    array(
      'id'        => 'font-sizes-px',
      'title'     => 'Font sizes px array',
      'subtitle'  => 'sms_options[\'fonts\'][\'size-px-list\']',
      'data'      => print_r($sms_utils->sms_options['fonts']['size-px-list'], true),
    ),
    array(
      'id'        => 'font-sizes-em',
      'title'     => 'Font sizes em array',
      'subtitle'  => 'sms_options[\'fonts\'][\'size-em-list\']',
      'data'      => print_r($sms_utils->sms_options['fonts']['size-em-list'], true),
    ),
    
  );

  $field_array[] = array(
    'id'        => 'sms-library-mode',
    'title'     => 'Enable Library Mode?',
    'type'      => 'switch',
    'desc'      => 'This adds WAY more slots.',
  );

  $field_array[] = array(
    'id'        => 'sms-dev-mode',
    'title'     => 'Enable Developer Mode?',
    'type'      => 'switch',
    'desc'      => 'This enables handy comments in the sms-css.css file.',
  );

  $key = md5( site_url() );
  $dms_cache_flush_link = sprintf( '%s?pl_purge=%s', trailingslashit( site_url() ), $key );

  $urlprotocol = 'http'; if ($_SERVER["HTTPS"] == "on") {$urlprotocol .= "s";} $urlprotocol .= "://";
  $urldomain = $_SERVER["SERVER_NAME"];
  $urluri = $_SERVER['REQUEST_URI'];
  $urlvars = basename($urluri);
  $urlpath = str_replace($urlvars,"",$urluri);
  $urlfull = $urlprotocol . $urldomain . $urlpath;

  $field_array[] = array(
    'id'        => 'flush-less-cache',
    'title'     => 'Flush the DMS LESS Cache?',
    'type'      => 'raw',
    'content'   => sprintf( '<a target="_blank" class="button button-primary" href="%s">Flush DMS LESS Cache</a>', $dms_cache_flush_link ),
  );
  $field_array[] = array(
    'id'        => 'flush-sms-css-cache',
    'title'     => 'Flush the SMS CSS Cache?',
    'type'      => 'raw',
    'content'   => sprintf( '<a target="_blank" class="button button-primary" href="'.$urlfull.'?flush-sms-css=true">Flush SMS LESS Cache</a>' ),
  );
  $field_array[] = array(
    'id'        => 'recomplile-sms-less-cache',
    'title'     => 'Recompile the SMS LESS Cache?',
    'type'      => 'raw',
    'content'   => sprintf( '<a target="_blank" class="button button-primary" href="'.$urlfull.'?compile=true">Recompile SMS LESS</a>' ),
  );

  foreach ($field_generator_array as $key => $value) {
    
    // $field_array[] = array(
    //   'id'        => 'debug-'.$value['id'],
    //   'title'     => 'Enable '.$value['title'] .' Debug?',
    //   'subtitle'      => $value['subtitle'],
    //   'type'      => 'switch',
    // );
  
    $field_array[] = array(
      'id'        => 'debug-'.$value['id'].'-output',
      'type'      => 'raw',
      'title'     => $value['title'].' Debug Output',
      'markdown'  => true,
      'content'   => "<pre class='code-block'>".$value['data']."</pre>",
      // 'required'  => array('debug-'.$value['id'], 'equals', true ),
    );

    // @TODO
    // Get ace editor field to update after storing the initial default setting
    // $field_array[] = array(
    //   'id'        => 'debug-'.$value['id'].'-output',
    //   'title'     => $value['title'].' Debug Output',
    //   'type'      => 'ace_editor',
    //   'mode'      => 'less',
    //   'theme'     => 'monokai',
    //   'options'   => array(
    //     'readOnly' => true,
    //     'highlightActiveLine' => false,
    //     ),
    //   'subtitle'  => 'Load your Typekit, Webink, and Google fonts in here.',
    //   'default'   => $value['data'],
    //   'required'  => array('debug-'.$value['id'], 'equals', true ),
    // );
  }

  // $field_array[] = array(
  //   'id'        => 'debug-font-kit-output',
  //   'type'      => 'raw',
  //   'title'     => 'Font Kit markup Debug Output',
  //   'markdown'  => true,
  //   'content'   => "<pre class='code-block'>".htmlentities($sms_utils->sms_options['fonts']['font-kit-markup'])."</pre>",
  //   // 'required'  => array('debug-'.$value['id'], 'equals', true ),
  // );
  
  $sections[] = array(
    'title' => 'Debug / Misc.',
    'desc' => '<p class="description">Choose debug options</p>',
    'icon' => 'el-icon-warning-sign',
    'fields' => $field_array,
  );

  return $sections;

});
