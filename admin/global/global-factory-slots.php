<?php

add_filter('redux/options/sms_redux/sections', function($sections){

	global $sms_redux;
	global $sms_utils;

	///////////////////////////////////////////////////////////////////////////
	// Card Slots
	///////////////////////////////////////////////////////////////////////////
	
	function generate_global_slot_fields( $factory_type = null, $factory_name = null, $factory_icon = null ){
		global $sms_redux;
		global $sms_utils;

	  $factory_slots = constant("SMS_".strtoupper($factory_type)."_SLOTS");

		$post_list = $sms_utils->list_posts_as_redux_select_array( "sms_$factory_type" );
		$fields = array();
		for ($i=1; $i <= $factory_slots; $i++) {
			$id = $sms_redux[ "$factory_type-slot$i" ];
			$post_edit_url = "/wp-admin/post.php?post=".$id."&action=edit";
			$fields[] = array(
				'id'        => "$factory_type-slot$i",
				'type'      => 'select',
				'title'     => $id ? "<a href='$post_edit_url'>Slot #$i</a>" : "Slot #$i",
				'options'   => $post_list,
				'compiler'  => true,
				// 'default'   => $sms_utils->get_first_item_in_associative_array( $post_list ),
			);
		}

		// Register Section
		$sections = array(
			'title' => "$factory_name",
			'desc' => '<p class="description">Choose the templates that are available on the site.</p>',
			'icon' => $factory_icon,
			'fields' => $fields,
		);

		return $sections;
	}

	// Header Slots
	$sections[] = generate_global_slot_fields('header', 'Header Style', 'el-icon-website');

// Header Slots
	$sections[] = generate_global_slot_fields('header_template', 'Header Template', 'el-icon-website');

	// Card Slots
	$sections[] = generate_global_slot_fields('card', 'Cards', 'el-icon-photo');

	// Quote Slots
	$sections[] = generate_global_slot_fields('quote', 'Quotes', 'el-icon-quotes-alt');

	// Accordion Slots
	$sections[] = generate_global_slot_fields('accordion', 'Accordions', 'el-icon-stackoverflow');
	
	// Grid Slots
	$sections[] = generate_global_slot_fields('grid', 'Grids', 'el-icon-th');

	// Accordion Slots
	$sections[] = generate_global_slot_fields('list', 'Lists', 'el-icon-th-list');

  // Divider Slots
	$sections[] = generate_global_slot_fields('divider', 'Dividers', 'el-icon-minus');

  // Slider Slots
	$sections[] = generate_global_slot_fields('slider', 'Sliders', 'el-icon-play-circle');

	// Button Slots
	$sections[] = generate_global_slot_fields('button', 'Buttons', 'el-icon-iphone-home');


	return $sections;
});
