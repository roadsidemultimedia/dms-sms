### Changelog

#### 1.6.6 [01/18/2017]

* Switched back to using the standard Ace Editor library that ships with Redux, rather than the customized version included with SMS due to weird conflicts and broken paths.
* Disabled dev mode

#### 1.6.5 [07/07/2015]

* Database query optimizations

#### 1.6.4 [02/15/2015]

* Fix for Magnific Popup JS Enqueueing. JS would only load in if a Magnific Popup generator section was on the page. Now it loads in regardless.
* Map section button for "get directions" ACTUALLY opens in a new tab. This may not have actually been pushed into the 1.6.2 update correctly.
* Added a fix for checking font data on fresh WP installs that could cause a fatal error.
* Redux Extensions only load if the backend is being viewed now.

#### 1.6.3 [01/19/2015]

* Fix for Youtube popups when using ```.popup-youtube``` and ```.mfp-youtube``` classes. When the screen width was below 700px, videos wouldn't open. 

#### 1.6.2 [01/02/2015]

* Updated verbiage of example on less style field for bands.
* Map section button for "get directions" now opens in a new tab.
* Fixed issue that caused Gravity Form placeholder to display on mobile screens.

#### 1.6.1 [12/16/2014]

* Gravity form popup placeholder section added. No longer a seperate plugin. Correctly hides itself and occupies no space when not logged in.
* Fix for mobile devices when a fixed position background is used on a band.
* Fix for strange issue that would sometimes cause factory fields to output as if they were a color selector, and would append "-a" to the end of the output as a result.
* Fix for the selector display on Band LESS fields.
* Headers slots reduced from 2 to 1.
* Slick slider library added to SMS plugin instead of linking to CDN, so it can be minified
* Hoverintent added to SMS plugin as well. 

#### 1.6.0 [12/04/2014]

* Option added to textbox to switch between static and relative font units. This allows textboxes to use REM units that scale with the design. 
* Option added to bands to remove side padding on smaller screen sizes.
* Magnific Popup added to SMS with a bunch of handy classes.
* Alignment options added to button section.
* Default button alignment no longer adds "align-center" by default to the button container, allowing it to inherit the text alignment of the band.
* Library mode added. Allows 99 slots for each factory type. Enabled in the Global settings under "debug / misc."
* Added a modified version of the rapidtabs section for the blog.
* Fixed slickslider font & ajax loader gif path so it loads in correctly. ** Requires a LESS cache flush **
* Textbox section now defaults to no padding on top
* Textbox section now has options for removing padding from the top, bottom or both.
* Body tag is now assigned the global font family. If a canvas area does not have a theme selected, the correct font family is displayed.
* Filter added to prevent Pagelines from prefixing the titles of images uploaded on the front end
* Gradient backgrounds for band styles are now prefixed for all browsers that support them. Before, they only worked in webkit browsers.
* Added support for numerous LESS mixins. Pretty much everything here: http://css-tricks.com/snippets/css/useful-css3-less-mixins/ except for shadows and border radius, and opacity.

* Added a new SMS Map section
    * Allows color customizations from snazzymaps.com
    * Detects office addresses that are entered as Client Info tags (office1-address1, office1-address2, office2-address1, office2-address2, etc...) and provides buttons that will geocode the coordinates.
    * Supports client info tags & shortcodes for fields.
    * Seperate fields for address, phone and map link, which auto generate the markup for the description.
    * Revamped popup description style.
* Additional data sources added to select lists in factory back end.
    * Font size classes
    * Font Weight Classes
    * Font Type Classes
    * Line Height Classes
    * Letter Spacing Classes
    * Font Transform Classes
    * SMS Heading Classes
    * Heading Tags (H1-H6)
    * Color Picker (Custom)
    * Icon Picker (Font Awesome)
    * Page selection

* **Breaking Changes**
    * Mean menu and superfish replaced with slim menu

#### 1.5.0 [11/07/2014]

* **New Feature** Header Template Factory! This allows the creation of headers as sections, similar to other factories. Loads in Superfish 1.7.4, Meanmenu 1.0.6, and Hover Intent.
* Factory markup field now process WordPress shortcodes.
* Additional image size added "true-aspect-thumb-200", which produces an images size that is 200px wide and maintains the aspect ratio of the image.

#### 1.4.6 [11/06/2014] 

* Fixed issue that would occur if a shadow was enabled on a band without setting a color.
* Fixed issue that would make band border display even after being disabled.
* Fixed duplicate factory CSS field that was accidently added to version 1.4.2

#### 1.4.5 [10/16/2014] 

* Added method to Utility class ```get_band_slot_id_by_slug(``)``` which allows the ID of a band post to be retrieved based on it's slug.
* Altered load order to allow external manipulation of Redux global settings.
* Added plugin thumbnail. Photo Credit: https://www.flickr.com/photos/one43/241644518/in/photostream/

#### 1.4.4 [10/14/2014] 

* Global styles now automatically compiled, instead of requiring a click and a refresh.

#### 1.4.3 [10/12/2014] 

* Fixes custom css output for band styles. There were some situations that would cause the selector to output incorrectly, and apply the css to a different band.

#### 1.4.2 [10/01/2014] 

* Ace editor fields that display custom markup and css now use two space soft tabs as the default.

#### 1.4.1 [10/01/2014]

* Fixes LESS compilation issue. A code regression knocked out compilation for all the new factories.

#### 1.4.0 [09/29/2014] Style Chunk Release

* **New Feature** Style Chunks! Write chunks of LESS and CSS as posts and they auto compile into the site. Modular chunks of CSS for anything you want.
* **New Feature** Custom CSS per band style! You can now write Custom CSS for each band style.
* Featured image on blog posts can be set to display above or below the title tag and post meta for more layout control.
* Textbox section now supports {{quicktags}} of Client Info Tags plugin.
* Dev mode added. When dev mode is enabled the output of the sms-css.css file contains useful comments that indicate what post generated the css. When dev mode is not enabled, the css output is stripped of comments and minified.
* Button added to front end to edit the active section template backend settings. Also added additional feedback when a template is not selected.
* Revamped LESS compiler to address a minor issue present in 1.3.0. that could cause duplicate styles to output.

#### 1.3.0 [09/25/2014] Super Factory Release

* **New Feature** Slider Factory! Full width & content width. 
* **New Feature** Quote Factory! 
* **New Feature** List Factory! 
* **New Feature** Accordion Factory! 
* **New Feature** Grid Factory! 
* DMS Accordian field support added to all factories including buttons, cards, and dividers.
* Slick Slider JS and CSS added (compiling into section css)
* LESS compiles only when saving a post or redux setting.
* Significantly faster backend performance due to new ondemand LESS compilation system. Page load times roughly 4 times faster on average.
* All factory section fields now work with client info tags.
* Divider now able to display full width or content width.

#### 1.2.2 [09/10/2014]

* Button factory now uses dynamic fields rather than a few statically defined fields for maximum flexability.
* Improved LESS compilation for bands. Each band is compiled seperately for better error reporting. Eventually this will also be used for caching.
* Card limit changed from 4 to 10.
* Fixed potential issue that could cause duplicate button styles to output.
* Removed Quote styles from Band style. Will use a Quote factory in the future.
* Added {{uid}} template token that uses section's clone ID. Useful for things like magnific popups that are linked to a hidden piece of content. Also useful for accordians.
* Removed empty less files from button and card sections that were causing issues
* Abstracted DMS field generator logic so it is shared between factories.
* Fixed a couple of metabox errors.
* Removed old LESS compilation function.

#### 1.2.1 [09/04/2014]

* **New Feature** Header Styles. You can now create modular header styles with custom CSS in the backend.

#### 1.2.0 [09/03/2014] The Factory Release.

* **New Feature** Super fast auto compiling of LESS & CSS! Outside of section & theme development, there should be no need to ever flush the LESS cache again.
* **New Feature** Cards! You can now create modular card styles with custom HTML and CSS. You can also create arbitrary fields that will display on the front end, and use template tokens to output the data in the section output.
* **New Feature** Buttons! You can now create modular button styles with custom HTML and CSS. 
* **New Feature** Dividers! You can now create modular divider styles with custom HTML and CSS.
* **New Feature** Quick Links! The admin bar now contains links to edit active band styles, the active color scheme, the active font kit, active button styles, and active divider styles.
* Added a little utility function for debugging. If query var of ```?flush-post-meta=true``` is appended to the url, the current posts meta data will be deleted.
* Fixed band shadow option for outer shadows.
* Fixed band border field requirement issue that made custom color option show up even if global color option was selected.
* Fixed issue of shadow not filling star indicator when shadows are active.
* Global band slots allow you to deselect a band style now.
* Added method for compiling LESS outside the Pagelines system. Useful for debugging and error checking.
* LESS error feedback for button post type produces a link to edit the offending post.
* Removed 8 post limit when selecting band styles for global band slots 
* Only published posts are shown when listing posts using ```list_posts_as_redux_select_array()``` and ```get_post_list_by_term_as_redux_field_array()```
* Added ```display_admin_notices()``` method for displaying messages at the top of the admin screen

#### 1.1.3 [08/19/2014]

* Added list of font size classes for other plugins to reference: ```$sms_utils->sms_options['fonts']['size-name-px-class-list']```
* Added list of font type classes for other plugins to reference: ```get_available_font_type_classes()```

#### 1.1.2 [08/18/2014]

* New Feature: Band Shadows. Add shadows to bands that scale with fonts.
* New Feature: Band Borders. Add borders to bands that scale with fonts and allow either a completely custom color (with opacity options) or a global color.
* Fixed issue that causes LESS compililation issues when a color brightness is selected without selecting a color
* Line height for text can be set in band style without specifically specifying a font size override now.
* Font PX size is once again displayed in the selection list for bands and font kits.
* Removed a few pesky default values.


#### 1.1.1 [08/14/2014]

* Fixed rare issue that could cause a body selector to end up with a font-size of the previous element in the data collection loop.

#### 1.1.0 [08/13/2014]

* This update requires DMS Skeleton theme 0.6.0 or newer, otherwise typography is going to be extremely messed up.
* Reduced number of available Ratio types for modular font size scale
* Max body font scale base size is now 20px instead of 28px. This should cover our bases, while keeping headings from being overly enormous.
* Font scale converted to actual relative units, rather than pixels.
* Line heights are unitless now, so they scale better.
* Added option to clear LESS cache from debug page of SMS
* Removed Subtle Pattern library from SMS plugin. Now points to assets hosted on Amazon S3
 
#### 1.0.6 [08/04/2014]

* Fixed band line-height setting
* Fixed band letter spacing setting

#### 1.0.5 [08/04/2014]

* Split up CSS and LESS into two seperate channels. This should make updating the styles considerably faster. Right now, only color changes require a LESS recompile.
* Fixed color of DMS frontend editor icons
* Fixed font sizing issue


#### 1.0.4 [07/31/2014]

* Color field improvements
* Added backend stylesheet for styling admin interface
* Added frontend stylesheet that only displays for logged in users capable of managing options. This stylesheet will be used for front end styles, such as the indicators used on headings.
* Added option to specify path to write_log method
* Disabled link to SMS in admin top bar for now. The subpage links were broken.
* Added a debug page to the global settings that displays LESS output at the moment. Will add more later.

#### 1.0.3 [07/28/2014]

* Filter added that looks for ```[firstchild_redirect]``` in a page's text and  if found, redirects to first child page. (Note: Standard page templates do not seem to work with DMS.)
* Fixed issue that resulted in empty css selectors ending up in CSS output. They are filtered out now.
* Massive overhaul of admin interface for banding styles, font kits, and color schemes.
    * Vastly improved band style interface.
    * Now broken into subsections
    * Added indicators to signify what settings are enabled and what are disabled at a quick glance.  
* [Redux metabox extension](http://reduxframework.com/extension/metaboxes/) updated to 1.2.8
* Converted method added to deal with differences in redux metabox storage format change from version 1.2.3
* Added some handy info under band font type that tells you what font type is set globally, so you know what you're overriding.

#### 1.0.2 [7/26/2014]

* Fixed issue preventing font kit assets from loading in.
* Fixed issue preventing font kit styles from displaying on the site.
* Improved user interface for font kit fields
* Fixed issue that hid the WP Engine admin menu button

#### 1.0.1 [7/24/2014]

* Fixed admin menu link to Global Style Management settings.
* Fixed empty background image property.

#### 1.0.0 [07/23/2014]

 * Starting to keep a changelog.
 * Fixed issue that caused the Style Management menu to overwrite the WP Dashboard button in the sidebar.
 * Added ```write_log()``` method to utility class. Allows you to write info to a log file that's stored in wp-content folder as ```sms-debug.log``` if SMS_DEBUG is set to true.
 * Added method get_upload_folder() to utility class.
