<?php
/*
	Section: SMS Blog Tabs
	Author: PageLines
	Author URI: http://www.pagelines.com
	Description: Displays your most popular, and latest posts as well as Archives and Categories in a tabbed format.
	Class Name: smsBlogTabs
	Filter: widgetized
	Edition: pro
*/

class smsBlogTabs extends PageLinesSection {

	function section_persistent(){

	}
	
	function section_styles(){
		pl_enqueue_script( 'jquery-ui-tabs' );
	}
	

	function section_head(){

		?>
		<script>
		!function ($) {
			$(document).on('sectionStart', function( e ) {
				$('.sms-blog-tabs').tabs({
					show: true
				})
			})
		}(window.jQuery);
		</script>
		<?php

	}

		function section_opts(){

			$opts = array(
				array(
					'type'		=> 'multi',
					'key'		=> 'sms_blog_tab', 
					'title'		=> 'SMS Blog Tab Display',
					'col'		=> 2,
					'opts'		=> array(
						array(
							'key'			=> 'sms_blog_tab_thumb',
							'type' 			=> 'check',
							'label' 	=> __( 'Display thumbnail next to blog posts?', 'pagelines' ),
							// 'ref'		=> __( 'This option uses CSS padding shorthand. For example, use "15px 30px" for 15px padding top/bottom, and 30 left/right.', 'pagelines' ),
						),
						array(
							'label' 	=> 'Choose category that contains popular posts',
							'key'           => 'sms_blog_tab_top_cat',
							'type'          => 'select_taxonomy',
							'post_type'     => 'post' // the post type to grab taxonomies from
						),
						array(
							'label' 	=> 'How many posts to display?',
	            'key'           => 'sms_blog_tab_post_num',
	            'type'          => 'select', 
	            'opts'=> array(
	                '4'          => array( 'name' => '4 posts' ),
	                '5'          => array( 'name' => '5 posts' ),
	                '6'          => array( 'name' => '6 posts' ),
	                '7'          => array( 'name' => '7 posts' ),
	                '8'          => array( 'name' => '8 posts' ),
	                '9'          => array( 'name' => '9 posts' ),
	                '10'         => array( 'name' => '10 posts' ),
	            ),
            ),
					)
				),
			);

			return $opts;
		}


	 function section_template() {
		
		global $plpg; 
		$pageID = $plpg->id;


		if( $this->opt('sms_blog_tab_post_num') ){
			$num_posts = $this->opt('sms_blog_tab_post_num');
		} else {
			$num_posts = 4;
		}

		?>
	<div class="widget">
		<div class="widget-pad">
	<div class="sms-blog-tabs">
		<ul class="tabbed-list sms-blog-nav fix">
			<li><a href="#sms-blog-popular"><?php _e( 'Popular', 'pagelines' ); ?></a></li>
			<li><a href="#sms-blog-recent"><?php _e( 'Recent', 'pagelines' ); ?></a></li>
			<li><a href="#sms-blog-archives"><?php _e( 'Archives', 'pagelines' ); ?></a></li>
			<li><a href="#sms-blog-categories"><?php _e( 'Categories', 'pagelines' ); ?></a></li>
		</ul>

		<div id="sms-blog-popular">

				<ul class="media-list">
					<?php

					foreach( get_posts( array('ignore_sticky_posts' => 1, 'orderby' => 'post_date', 'order' => 'desc', 'numberposts' => $num_posts, 'exclude' => $pageID) ) as $p ){
						// echo "<pre>".print_r($p)."</pre>";
						// echo "<pre>".print_r( get_permalink($p->ID) )."</pre>";
						$link = get_permalink($p->ID);
						if( $this->opt('sms_blog_tab_thumb') ){
							$img = (has_post_thumbnail( $p->ID )) ? sprintf('<div class="img"><a class="the-media" href="%s" style="background-image: url(%s)"></a></div>', get_permalink( $p->ID ), pl_the_thumbnail_url( $p->ID, 'thumbnail')) : '';
						} else {
							$img = "";
						}
						printf(
							'<li class="media fix">%s<div class="bd"><a class="title" href="%s">%s</a></div></li>', 
							$img,
							get_permalink( $p->ID ), 
							$p->post_title, 
							pl_short_excerpt($p->ID)
						);

					} ?>


				</ul>
	
		</div>
		<div id="sms-blog-recent">
			
				<ul class="media-list">
					<?php

					foreach( get_posts( array('ignore_sticky_posts' => 1, 'orderby' => 'post_date', 'order' => 'desc', 'numberposts' => $num_posts, 'exclude' => $pageID) ) as $p ){
						if( $this->opt('sms_blog_tab_thumb') ){
							$img = (has_post_thumbnail( $p->ID )) ? sprintf('<div class="img"><a class="the-media" href="%s" style="background-image: url(%s)"></a></div>', get_permalink( $p->ID ), pl_the_thumbnail_url( $p->ID, 'thumbnail')) : '';
						} else {
							$img = "";
						}
						printf(
							//'<li class="media fix">%s<div class="bd"><a class="title" href="%s">%s</a><span class="excerpt">%s</span></div></li>', 
							'<li class="media fix">%s<div class="bd"><a class="title" href="%s">%s</a></div></li>', 
							$img,
							get_permalink( $p->ID ), 
							$p->post_title, 
							pl_short_excerpt($p->ID)
						);

					} ?>


				</ul>
		</div>

		<div id="sms-blog-archives">
			
			<?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
			<ul class="quote-list">

			</ul>
			
		</div>
		<div id="sms-blog-categories">
				
				<div class="categories-list">
					<?php

						$args = array(
							'show_option_all'    => '',
							'orderby'            => 'name',
							'order'              => 'ASC',
							'style'              => 'list',
							'show_count'         => 1,
							'hide_empty'         => 1,
							'use_desc_for_title' => 0,
							'child_of'           => 0,
							'feed'               => '',
							'feed_type'          => '',
							'feed_image'         => '',
							'exclude'            => '',
							'exclude_tree'       => '',
							'include'            => '',
							'hierarchical'       => 1,
							'title_li'           => '',
							'show_option_none'   => 'No categories',
							'number'             => null,
							'echo'               => 1,
							'depth'              => 0,
							'current_category'   => 0,
							'pad_counts'         => 0,
							'taxonomy'           => 'category',
							'walker'             => null
				    );
				    wp_list_categories( $args );
					
					 ?>


				</div>
		
		</div>
				
		</div>
	</div>
</div>
		<?php
		
		


	}



	
}


