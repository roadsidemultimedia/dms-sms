<?php
/*
	Plugin Name: SMS Divider Section
	Author: Milo Jennings
	Author URI: http://roadsidemultimedia.com
	Description: A divider section
	Class Name: sms_divider_section
	Version: 1.0
	Filter: component, dual-width
	Loading: active
	Section: Divider
*/

// Our class is namespaced with initials and the section name.
class sms_divider_section extends PageLinesSection {

	public function __construct(){
		parent::__construct();
		$this->name = "Divider";
	}

	// RUNS ALL TIME - This loads all the time, even if the section isn't on the page. Actions can go here, as well as post type setup functions
	function section_persistent(){
		
	}

	// BEFORE SECTION - This adds a class to the section wrap. You can also put HTML here and it will run outside of the section, and before it.
	function before_section_template( $location = '', $clone_id = null ) {

		// If the current section is displayed as a full width section
		if( $this->meta['draw'] == 'area' ){
			$class_id = $this->opt( $this->id.'_style' );
			$this->wrapper_classes['option_classes'] = "divider-full-width $class_id";
			// echo "<p>BEFORE: full width section</p>";
		}

	}
	function after_section_template( $location = '', $clone_id = null ) {
		if( $this->meta['draw'] == 'area' ){
			// echo "<p>AFTER: full width section</p>";
		}
	}

	function section_global_markup(){
		global $global_slider_markup_printed;
		$slot_id = $this->opt( $this->id.'_style' );
		if($global_slider_markup_printed[$slot_id] == true)
			return false;

		global $sms_redux;
		global $sms_utils;

		$template_id = $sms_redux[ $this->opt( $this->id.'_style' ) ];
		$template_meta = $sms_utils->get_redux_post_meta( $template_id );
		
		if(!isset($template_id))
			return false;

		echo "\n".$template_meta['global-markup']."\n";

		// Set variable to true now that the global data has been output by this style
		$global_slider_markup_printed[$slot_id] = true;
	}

	// SECTION MARKUP - This is the function that outputs all the HTML onto the page. Put all your viewable content here.
	function section_template() {

		global $sms_redux;
		global $sms_utils;
		global $pldraft;

		$edit = false;
		if( is_object( $pldraft ) && 'draft' == $pldraft->mode )
			$edit = true;

		// Add global markup to first instance of this section on the page.
		// This ensures that it's only loaded in if a section is on the page
		$this->section_global_markup();

		$dms_output = new smsDmsFieldFactory();
		$template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'divider' );
		echo $dms_output->render_frontend( $template_id, $this );

	}

	function section_opts(){

		global $sms_utils;
		global $sms_redux;

    $dms_output = new smsDmsFieldFactory();
    $template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'divider' );
    $options = $dms_output->generate_frontend_fields($template_id, $this, 'divider');

		return $options;

	}

} // Don't put code past this point.