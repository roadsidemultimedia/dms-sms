<?php
/*
	Plugin Name: SMS Header Template Section
	Author: Milo Jennings
	Author URI: http://roadsidemultimedia.com
	Description: A header section
	Class Name: sms_header_template_section
	Version: 1.0
	Filter: component, layout, dual-width
	Loading: active
	Section: true
*/

// Our class is namespaced with initials and the section name.
class sms_header_template_section extends PageLinesSection {

	public function __construct(){
		parent::__construct();
		$this->name = "Header Template";
	}

	// RUNS ALL TIME - This loads all the time, even if the section isn't on the page. Actions can go here, as well as post type setup functions
	function section_persistent(){
		
	}

	// LOAD SCRIPTS
	function section_scripts(){
		// wp_enqueue_script( 'rs-basic-js', $this->base_url.'/js/rs.basic.js', array( 'jquery' ), pl_get_cache_key(), true );
		// wp_enqueue_script( 'superfish-js', $this->base_url.'/js/superfish.js', array( 'jquery' ), pl_get_cache_key(), true );
		// wp_enqueue_script( 'meanmenu-js', $this->base_url.'/js/jquery.meanmenu.js', array( 'jquery' ), pl_get_cache_key(), true );
		// 
		// wp_enqueue_style( 'superfish-css', 'http://cdnjs.cloudflare.com/ajax/libs/superfish/1.7.4/superfish.min.css', false, '1.7.4');
    // wp_enqueue_style( 'meanmenu-css', 'https://cdn.rawgit.com/meanthemes/meanMenu/0ca2ffa899751504871065570fa30798f1bdb445/meanmenu.min.css');

    wp_enqueue_style( 'slimmenu-css', DMS_SMS_URL.'/assets/css/slimmenu.min.css');

		// wp_enqueue_script( 'rs-hoverIntent', '//cdnjs.cloudflare.com/ajax/libs/jquery.hoverintent/2013.03.11/hoverintent.min.js', array( 'jquery' ), pl_get_cache_key(), true );
		wp_enqueue_script( 'rs-hoverIntent', DMS_SMS_URL.'/assets/js/hoverIntent.min.js', array( 'jquery' ), pl_get_cache_key(), true );
		// 
		// wp_enqueue_script( 'superfish-js', '//cdnjs.cloudflare.com/ajax/libs/superfish/1.7.4/superfish.js', array( 'jquery' ), '1.7.4', true );
		// wp_enqueue_script( 'meanmenu-js', '//cdn.jsdelivr.net/jquery.meanmenu/2.0.6/jquery.meanmenu.min.js', array( 'jquery' ), '2.0.6', true );
		
		wp_enqueue_script( 'slimmenu-js', DMS_SMS_URL.'/assets/js/jquery.slimmenu.min.js', array( 'jquery' ), false, false );

	}

	// RUNS IN <HEAD>
	function section_head() {

		// Always use jQuery and never $ to avoid issues with other store products

			/*
		?><script>
			jQuery(document).ready(function($){

			});
		</script><?php
		*/

	}

		// BEFORE SECTION - This adds a class to the section wrap. You can also put HTML here and it will run outside of the section, and before it.
	function before_section_template( $location = '', $clone_id = null ) {

		// $this->wrapper_classes['background'] = 'special-class';
		// 
		// $alignment = $this->opt( $this->id.'_alignment' ) ? $this->opt( $this->id.'_alignment' ) : 'center';
		// $this->wrapper_classes['option_classes'] = "align-$alignment";

		// // WORK IN PROGRESS
		// // styles for full-width version of header_template section. Mostly for placing between bands
		// // If the current section is displayed as a full width section
		// if( $this->meta['draw'] == 'area' ){
		// 	// echo "<p>BEFORE: full width section</p>";
		// 	echo "<div class='pos-rel btn-full-width' style='background: #990; padding: 10px 0; z-index: 1; width: 100%; position: absolute; left: 0; margin-top: -25px;'>";
		// }

	}
	function after_section_template( $location = '', $clone_id = null ) {
		// if( $this->meta['draw'] == 'area' ){
		// 	echo "</div>";
		// 	// echo "<p>AFTER: full width section</p>";
		// }
	}

	function section_global_markup(){
		global $global_header_template_markup_printed;
		$slot_id = $this->opt( $this->id.'_style' );
		if($global_header_template_markup_printed[$slot_id] == true)
			return false;

		global $sms_redux;
		global $sms_utils;

		$template_id = $sms_redux[ $this->opt( $this->id.'_style' ) ];
		$template_meta = $sms_utils->get_redux_post_meta( $template_id );
		
		if(!isset($template_id))
			return false;

		echo "\n".$template_meta['global-markup']."\n";

		// Set variable to true now that the global data has been output by this style
		$global_header_template_markup_printed[$slot_id] = true;
	}

	// SECTION MARKUP - This is the function that outputs all the HTML onto the page. Put all your viewable content here.
	function section_template() {

		global $sms_redux;
		global $sms_utils;
		global $pldraft;
		$this->pldraft = $pldraft;

		// Add global markup to first instance of this section on the page.
		// This ensures that it's only loaded in if a section is on the page
		$this->section_global_markup();

		$dms_output = new smsDmsFieldFactory();
		$template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'header_template' );

		echo $dms_output->render_frontend( $template_id, $this );

	}

	function section_opts(){

		global $sms_utils;
		global $sms_redux;

    $dms_output = new smsDmsFieldFactory();
    $template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'header_template' );
    $options = $dms_output->generate_frontend_fields($template_id, $this, 'header_template');

		return $options;
	}

} // Don't put code past this point.