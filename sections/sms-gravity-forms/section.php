<?php
/*
Plugin Name: Inline Gravity Forms
Plugin URI: http://www.roadsidemultimedia.com
Description: Allows you to add a hidden gravity form that can be called by any button using #tag and class
Author: Curtis Grant
PageLines: true
Version: 1.1.1
Section: Gravity Form Popup Placeholder
Class Name: GForms
Filter: component, custom
Loading: active
Text Domain: dms-sms
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class GForms extends PageLinesSection {

  function section_persistent(){

    // Magnific Popup CSS is compiled in from this section, in style.less
    wp_enqueue_script( 'magnific-popup', $this->base_url . '/js/jquery.magnific-popup.min.js', array( 'jquery' ), pl_get_cache_key(), true );

  }

  function section_styles(){
  }

  function section_opts(){

    $opts = array(
      array(
        'type'    => 'text',
        'key'   => 'gforms_id',
        'label'   => __( 'Form ID', 'pagelines' ),
        'default' => false,
      )
    );

    return $opts;

  }

  /**
  * Section template.
  */
     function section_head() {

  }

  function section_template() {

    $gformsid = ( $this->opt('gforms_id') ) ?: false;
    if($gformsid){
      $content = "GRAVITY FORMS POPUP PLACEHOLDER - Href = #gform{$gformsid} | Class=popup-inline";
      echo "<div class='sms-gforms drag-drop-editing'>$content</div>";
      echo "<div id='gform{$gformsid}' class='white-popup mfp-hide'>";
      echo gravity_form($gformsid, true, true, false, '', true);
      echo "</div>";
    } else {
      $content = "GRAVITY FORMS POPUP PLACEHOLDER - No Gravity Form ID set";
      echo "<div class='sms-gforms drag-drop-editing'>$content</div>";
    }
    

  }

}


