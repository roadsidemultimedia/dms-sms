<?php
/*
	Section: Canvas Area
	Author: PageLines
	Description: Creates a full width, full height area, with a nested content width region for placing sections and columns.
	Class Name: PLSectionArea
	Filter: full-width
	Loading: active
*/

/**
 * Forked from DMS 2.1.6.1 
 * To add CSS based window height solution along with other customizations.
 */


class PLSectionArea extends PageLinesSection {

	function section_head(){
		
		// Upgrade area background options from 1.1 > 1.2
		$upgrade_options = array(
			'pl_area_image'		=> $this->id.'_background',
			'pl_area_bg_repeat'	=> $this->id.'_repeat',
			'pl_area_bg'		=> $this->id.'_theme',
			'pl_area_bg_color_enable'	=> $this->id.'_color_enable',
			'pl_area_bg_color'	=> $this->id.'_color',
		); 

		$this->upgrade_section_options( $upgrade_options );
	
	}
	
	function section_opts(){

		global $sms_utils;

		$options = array();

		$options[] = array(

			'key'			=> 'pl_area_pad_selects',
			'type' 			=> 'multi',
			'label' 	=> __( 'Set Area Padding', 'pagelines' ),
			'opts'	=> array(
				array(
					'key'			=> 'pl_area_pad',
					'type' 			=> 'count_select_same',
					'count_start'	=> 0,
					'count_number'	=> 200,
					'count_mult'	=> 10,
					'suffix'		=> 'px',
					'label' 	=> __( 'Area Padding (px)', 'pagelines' ),
				),
				array(
					'key'			=> 'pl_area_pad_bottom',
					'type' 			=> 'count_select_same',
					'count_start'	=> 0,
					'count_number'	=> 200,
					'count_mult'	=> 10,
					'suffix'		=> 'px',
					'label' 	=> __( 'Area Padding Bottom (if different)', 'pagelines' ),
				),
				array(
					'key'			=> 'pl_area_height',
					'type' 			=> 'text',
					'label' 	=> __( 'Area Minimum Height (px)', 'pagelines' ),
				),
				array(
					'key'			=> 'pl_remove_side_padding',
					'type' 			=> 'check',
					'label' 	=> __( 'Remove side padding ', 'pagelines' ),
					'help'   => 'This removes the side padding that displays for tablet and mobile breakpoints.'
				),
				array(
					'key'			=> 'pl_remove_vertical_padding',
					'type' 			=> 'check',
					'default' => false,
					'render' => false,
					'label' 	=> __( 'Remove vertical section padding ', 'pagelines' ),
					'help'   => 'This allows you to control padding on the .pl-section-pad div.',
				),
			),
			

		);

		$options[] = array(

			'key'			=> 'pl_custom_id',
			'type' 			=> 'multi',
			'label' 	=> __( 'Custom Band ID', 'pagelines' ),
			'opts'	=> array(
				array(
					'key'			=> 'pl_custom_band_id',
					'type' 			=> 'text',
					'label' 	=> __( 'Custom Band ID', 'pagelines' ),
				),
			),
			

		);
		
		// Determine which band style is being used, and generate buttons to edit it.
		$pl_area_theme_classes = $this->opt($this->id.'_theme');
		$pl_area_theme_class_array = explode(" ", $pl_area_theme_classes);
		
		// Check if the function exists and the class extracted is actually an sms slot slug.
		if( method_exists( $sms_utils, 'get_band_slot_id_by_slug') && strpos($pl_area_theme_class_array[0], "sms") !== false ){
			$pl_area_slot_slug = str_replace("sms-","",$pl_area_theme_class_array[0]);
			$this->slot_id = $sms_utils->get_band_slot_id_by_slug($pl_area_slot_slug);
		} else {
			$this->slot_id = false;
		}

		$post_edit_url = get_edit_post_link( $this->slot_id );
		$post_create_url = "/wp-admin/post-new.php?post_type=sms_band";
		$post_assign_url = "/wp-admin/admin.php?page=dms-sms.php";

		$my_template = "<div>".
		($this->slot_id ? "<a class='btn btn-primary btn-mini' style='margin-top: 4px;' target='_blank' href='$post_edit_url'>Edit this Band Style</a>" : '').
		($this->slot_id ? "<a class='btn btn-secondary btn-mini' style='margin-left: 4px; margin-top: 4px;' target='_blank' href='$post_assign_url'>Activate Band(s) Globally</a>" : '').
		"</div>";

		$options[] = array(

			'key'			=> 'pl_area_styling',
			'type' 			=> 'multi',
			'col'			=> 3,
			'label' 	=> __( 'Area Styling', 'pagelines' ),
			'opts'	=> array(
				array(
					'key'			=> 'pl_area_parallax',
					'type' 			=> 'select',
					'opts'			=> array(
						''						=> array('name' => "No Scroll Effect"),
						'pl-parallax'			=> array('name' => "Parallaxed Background Image"),
						'pl-scroll-translate'	=> array('name' => "Translate Content on Scroll"),
						// 'sms-window-height'		=> array('name' => "Set to height of window"),
						// 'sms-window-height sms-vcenter-pl-area'		=> array('name' => "Set to height of window & vertically center content"),
					),
					'label' 	=> __( 'Scrolling effects.', 'pagelines' ),
				),
				array(
					'key'			=> 'pl_area_window_height',
					'type' 			=> 'check',
					'label' 	=> __( 'Window Height & vertically center content?', 'pagelines' ),
				),
				array(
					'key'			=> 'pl_area_full_width',
					'type' 			=> 'check',
					'label' 	=> __( 'Full Width?', 'pagelines' ),
				),
				array(
					'key'			=> 'pl_area_band_theme',
					'type' 			=> 'template',
					'label' 	=> __( 'Edit Band Style?', 'pagelines' ),
					'template' => $my_template,
					
				),

			),

		);
		
	
		
		return $options;
	}
	
	
	
	function before_section_template( $location = '' ) {

		$this->alt_standard_title = true;
		

		$scroll_effect = $this->opt('pl_area_parallax');
		
		if( $scroll_effect && $scroll_effect == 1 ){
			$scroll_effect = 'pl-parallax';
		}
		$this->wrapper_classes['scroll'] = $scroll_effect;
		


		$window_height = $this->opt('pl_area_window_height');
		if( $window_height && $window_height == true ){
			$this->wrapper_classes['window-height'] = 'sms-window-height sms-vcenter-pl-area';
		} else {
			$this->wrapper_classes['window-height'] = '';
		}


		$full_width = $this->opt('pl_area_full_width');
		if( $full_width && $full_width == true ){
			$this->wrapper_classes['full-width'] = 'full-width';
		} else {
			$this->wrapper_classes['full-width'] = '';
		}

		if( $this->opt('pl_remove_side_padding') ){
			$this->wrapper_classes['no-side-padding'] = 'no-side-padding';
		}

		$vpadding = $this->opt('pl_remove_vertical_padding');
		if( $vpadding && $vpadding == true ){
			$this->wrapper_classes['no-vertical-padding'] = 'section-np';
		} else{
			$this->wrapper_classes['no-vertical-padding'] = '';
		}


		if($this->opt('pl_custom_band_id')){
			echo "<div id='". $this->opt('pl_custom_band_id') ."'></div>";
		}
		
	}

	

	function section_template( ) {
		
		$section_output = (!$this->active_loading) ? render_nested_sections( $this->meta['content'], 1) : '';
		
		$style = '';
		$inner_style = '';

		// Use alt mode for this
		$title = ( $this->opt('pl_standard_title') ) ? sprintf( '<h2 class="pl-section-title pla-from-top subtle pl-animation">%s</h2>', $this->opt('pl_standard_title') ) : '';
		
		$inner_style .= ($this->opt('pl_area_height')) ? sprintf('min-height: %spx;', $this->opt('pl_area_height')) : '';
		$inner_classes = 'pl-inner area-region pl-sortable-area editor-row';
		
		$classes = '';
		
		// If there is no output, there should be no padding or else the empty area will have height.
		if ( $section_output || $title != '' ) {
			
			// global
			$default_padding = pl_setting('section_area_default_pad', array('default' => '20'));
			// opt	
			$padding		= rtrim( $this->opt('pl_area_pad',			array( 'default' => $default_padding ) ), 'px' ); 			
			$padding_bottom	= rtrim( $this->opt('pl_area_pad_bottom',	array( 'default' => $padding ) ), 'px' ); 
			
			$style .= sprintf('padding-top: %spx; padding-bottom: %spx;',
				$padding,
				$padding_bottom
			);
			
			$content_class = $padding ? 'nested-section-area' : '';
			$buffer = pl_draft_mode() ? sprintf('<div class="pl-sortable pl-sortable-buffer span12 offset0"></div>') : '';
			$section_output = $buffer . $section_output . $buffer;
		}
		else {
			$pad_css = ''; 
			$content_class = '';
		}
	?>
	<div class="pl-area-wrap <?php echo $classes;?>" style="<?php echo $style;?>">
		
		<div class="pl-content <?php echo $content_class;?>">
			<?php echo $title; ?>
			<div class="<?php echo apply_filters( 'pl-area-inner-classes', $inner_classes, $this->meta ); ?>" style="<?php echo apply_filters( 'pl-area-inner-style', $inner_style, $this->meta );?>">
				<?php  echo $section_output; ?>
			</div>
		</div>
	</div>
	<?php
	}


}
