<?php
/*
	Plugin Name: SMS Card Section
	Author: Milo Jennings
	Author URI: http://www.roadsidemultimedia.com
	Description: A card section
	Class Name: sms_card_section
	Version: 1.0
	Filter: component
	Loading: active
	Section: Card
*/

// Our class is namespaced with initials and the section name.
class sms_card_section extends PageLinesSection {

	public function __construct(){
		parent::__construct();
		$this->name = "Card";
		// echo "<style>.pl-loader{display: none !important;}</style>";
	}

	// RUNS ALL TIME - This loads all the time, even if the section isn't on the page. Actions can go here, as well as post type setup functions
	function section_persistent(){
		
	}

	// LOAD SCRIPTS
	function section_scripts(){
			//wp_enqueue_script('script-name', $this->base_url.'/script.js', array('jquery'), self::version, true );
	}

	// RUNS IN <HEAD>
	function section_head() {
		
	}

	function section_global_markup(){
		global $global_card_markup_printed;
		$slot_id = $this->opt( $this->id.'_style' );
		if($global_card_markup_printed[$slot_id] == true)
			return false;

		global $sms_redux;
		global $sms_utils;

		$template_id = $sms_redux[ $this->opt( $this->id.'_style' ) ];
		$template_meta = $sms_utils->get_redux_post_meta( $template_id );
		
		if(!isset($template_id))
			return false;

		echo "\n".$template_meta['global-markup']."\n";

		// Set variable to true now that the global data has been output by this style
		$global_card_markup_printed[$slot_id] = true;
	}

	// SECTION MARKUP - This is the function that outputs all the HTML onto the page. Put all your viewable content here.
	function section_template() {

		global $sms_redux;
		global $sms_utils;
		global $pldraft;

		// Add global markup to first instance of this section on the page.
		// This ensures that it's only loaded in if a section is on the page
		$this->section_global_markup();

		$edit = false;
		if( is_object( $pldraft ) && 'draft' == $pldraft->mode )
			$edit = true;

		$dms_output = new smsDmsFieldFactory();
		$template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'card' );
		echo $dms_output->render_frontend( $template_id, $this );

	}

	function section_opts(){

		global $sms_utils;
		global $sms_redux;

		$dms_output = new smsDmsFieldFactory();
		$slot_slug = $this->opt( $this->id.'_style' );
		$template_id = $dms_output->validate_template_id_by_slot_slug( $slot_slug, 'card' );
		$options = $dms_output->generate_frontend_fields($template_id, $this, 'card');

		return $options;

	}

} // Don't put code past this point.