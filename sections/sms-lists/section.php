<?php
/*
	Plugin Name: SMS List Section
	Author: Milo Jennings
	Author URI: http://roadsidemultimedia.com
	Description: List Factory Section
	Class Name: sms_list_section
	Version: 1.0
	Filter: component
	Loading: active
	Section: List
*/

// Our class is namespaced with initials and the section name.
class sms_list_section extends PageLinesSection {

	public function __construct(){
		parent::__construct();
		$this->name = "List";
	}

	function section_global_markup(){
		global $global_list_markup_printed;
		$slot_id = $this->opt( $this->id.'_style' );
		if($global_list_markup_printed[$slot_id] == true)
			return false;

		global $sms_redux;
		global $sms_utils;

		$template_id = $sms_redux[ $this->opt( $this->id.'_style' ) ];
		$template_meta = $sms_utils->get_redux_post_meta( $template_id );
		
		if(!isset($template_id))
			return false;

		echo "\n".$template_meta['global-markup']."\n";

		// Set variable to true now that the global data has been output by this style
		$global_list_markup_printed[$slot_id] = true;
	}

	// SECTION MARKUP - This is the function that outputs all the HTML onto the page. Put all your viewable content here.
	function section_template() {

		global $sms_redux;
		global $sms_utils;
		global $pldraft;
		$this->pldraft = $pldraft;

		// Add global markup to first instance of this section on the page.
		// This ensures that it's only loaded in if a section is on the page
		$this->section_global_markup();

		$dms_output = new smsDmsFieldFactory();
		$slot_slug = $this->opt( $this->id.'_style' );
		$template_id = $dms_output->validate_template_id_by_slot_slug( $slot_slug, 'list' );

		echo $dms_output->render_frontend( $template_id, $this );

	}

	function section_opts(){

		global $sms_utils;
		global $sms_redux;

		$orientation_list = array(
			'left'   => 'Left',
			'center' => 'Center',
			'right'  => 'Right',
		);
		$orientation_list = $sms_utils->convert_redux_choices_to_dms( $orientation_list );

		$dms_output = new smsDmsFieldFactory();
		$slot_slug = $this->opt( $this->id.'_style' );
		$template_id = $dms_output->validate_template_id_by_slot_slug( $slot_slug, 'list' );

		$options = $dms_output->generate_frontend_fields($template_id, $this, 'list');


		return $options;
	}

} // Don't put code past this point.