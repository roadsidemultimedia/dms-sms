<?php
/*
	Section: SMS Maps
	Author: PageLines
	Author URI: http://www.pagelines.com
	Description: Google maps with markers.
	Class Name: SMSMaps
	Filter: component, dual-width
*/


class SMSMaps extends PageLinesSection {

	public $lat = '48.0511989';
	public $lng = '-122.18078400000002';
	public $desc = '1225 3rd ST #300<br>Marysville, WA 98270<br>';
	public $help = 'To find map the coordinates use this easy tool: <a target="_blank" href="http://www.mapcoordinates.net/en">www.mapcoordinates.net</a>';

	function __construct(){
		parent::__construct();
		global $sms_redux;
		$this->name = "SMS Maps";
		$this->sms_redux = $sms_redux;
		if( function_exists('client_info_filter') ){
			$this->cit = true;
		}
	}

	function section_styles(){

		wp_enqueue_script( 'google-maps', 'https://maps.google.com/maps/api/js?sensor=false', NULL, NULL, true );
		wp_enqueue_script( 'sms-maps', $this->base_url.'/maps.js', array( 'jquery' ), pl_get_cache_key(), true );

	}

	function get_office_list(){

		global $sms_utils;
		global $wpdb;

		if($this->cit){
			$table_name = $wpdb->prefix . "client_info";
			$info_tags_obj = $wpdb->get_results("SELECT * FROM $table_name");
			$info_tags = $sms_utils->objectToArray($info_tags_obj);

			foreach ($info_tags as $key) {
			  $key_exists = strpos( $key['info_key'], 'address' ) ? true : false;
			  $info_exists = strlen($key['info_value']) > 0 &&  $key['info_value'] !== 'false' ? true : false;
			  if( $key_exists && $info_exists ){
			    $office_num = substr($key['info_key'], 6, 1);
			    $line_num = substr($key['info_key'], -1);
			    $office_addresses['Office #'.$office_num][$line_num] = $key['info_value'];
			  }
			}

			$markup = '<div class="coord-info" id="coord-info"></div>';
			$markup .= '<ul style="padding-left: 0">';
			foreach ($office_addresses as $key => $value) {
			  $address = $value['1'] .', '. $value['2'];
			  $markup .= "<li class='margin-bottom-small'><a href='#' class='btn btn-mini' onclick='mapTools.getCoords(\"$address\")'>Get Coordinates of $key</a></li>";
			}
			$markup .= '</ul>';
			
			return $markup;

		} else {

			return 'Client Info Tags plugin not active. Please activate.';

		}
		
	}

	function section_head() {

		global $sms_redux;

		$locations = $this->opt('locations_array');
		$maps = array();
		$defaults = array(
			'lat'	=> floatval( $this->lat ),
			'lng'	=> floatval( $this->lng ),
			// 'mapinfo'	=> $this->desc,
			'image'		=> $this->base_url.'/marker.png'
		);
		$defaults['mapinfo'] = $this->cit ? client_info_filter('<p>{{office1-address1}}<br>{{office1-address2}}</p><p>{{office1-phone}}</p><p><a href="{{map-url}}" target="_blank" class="btn btn-primary">Get Directions</a></p>') : $this->desc;

		if( ! is_array( $locations ) ) {
			$maps = array(
					1 => $defaults
			);
		} else {
			$maps = array();
			$i = 1;
			foreach( $locations as $k => $data ) {

				$maps[$i] = array(
					'lat'	=> ( isset( $data['latitude'] ) ) ? floatval( $data['latitude'] ): $this->lat,
					'lng'	=> ( isset( $data['longitude'] ) ) ? floatval( $data['longitude'] ) : $this->lng,
					// 'mapinfo'	=> ( isset( $data['text'] ) && '' != $data['text'] ) ? $data['text'] : $this->desc,
					'image'		=> ( isset( $data['image'] ) && '' != $data['image'] ) ? do_shortcode( $data['image'] ) : $this->base_url.'/marker.png'
				);

				$maps[$i]['mapinfo'] = '';

				if( $data['address_line_1'] && $data['address_line_2'] )
					$maps[$i]['mapinfo'] .= '<p>'.$data['address_line_1'].'<br>'.$data['address_line_2'].'</p>';

				if( $data['phone'] )
					$maps[$i]['mapinfo'] .= '<p>'.$data['phone'].'</p>';

				if( $data['map_link'] )
					$maps[$i]['mapinfo'] .= '<p><a href="'.$data['map_link'].'" target="_blank" class="btn btn-primary">Get Directions</a></p>';

				// If there's still no data for the description, use the default
				if($maps[$i]['mapinfo'] == ''){
					$maps[$i]['mapinfo'] = $defaults['mapinfo'];
				}
				
				// Process map info shortcodes and client info tags
				$maps[$i]['mapinfo'] = $this->cit ? client_info_filter( do_shortcode( $maps[$i]['mapinfo'] ) ) : do_shortcode( $maps[$i]['mapinfo'] );

				$i++;
			}
		}

		$main = array(
			'lat'			=> $this->opt( 'center_lat', array( 'default' => $this->lat ) ),
			'lng'			=> $this->opt( 'center_lng', array( 'default' => $this->lng ) ),
			'zoom_level'	=> floatval( $this->opt( 'map_zoom_level', array( 'default' => 14 ) ) ),
			'zoom_enable'	=> $this->opt( 'map_zoom_enable', array( 'default' => true ) ),
			'enable_animation' => $this->opt( 'enable_animation', array( 'default' => true ) ),
			'image'			=> $this->base_url.'/marker.png',
			'custom_style' => json_decode( pl_setting( 'custom_style', array( 'default' => '' ) ) ),
			// 'custom_style' => json_decode($sms_redux['custom-map-style']),
		);

		wp_localize_script( 'sms-maps', 'map_data_' . $this->meta['unique'], $maps );

		wp_localize_script( 'sms-maps', 'map_main_' . $this->meta['unique'], $main );

	}

	function section_opts(){


		$options = array();

		$options[] = array(
			'type'	=> 'multi',
			'key'	=> 'plmap_office_buttons',
			'title'	=> __( 'Office Coordinates', 'pagelines' ),
			'col'	=> 1,
			'help'	=> $this->get_office_list(),
		);

		$options[] = array(
				'type'	=> 'multi',
				'key'	=> 'plmap_config',
				'title'	=> __( 'Google Maps Configuration', 'pagelines' ),
				'col'	=> 1,
				'opts'	=> array(

					array(
						'key'	=> 'center_lat',
						'type'	=> 'text_small',
						'default'	=> $this->lat,
						'place'		=> $this->lat,
						'label'	=> __( 'Latitude', 'pagelines' ),
						'help'	=> $this->help
					),
					array(
						'key'	=> 'center_lng',
						'type'	=> 'text_small',
						'default'	=> $this->lng,
						'place'	=> $this->lng,
						'label'	=> __( 'Longitude', 'pagelines' ),
						'help'	=> $this->help
					),

					array(
						'type'	=> 'select',
						'key'	=> 'map_height',
						'default'	=> '350px',
						'label'	=> __( 'Select Map Height ( default 350px)', 'pagelines' ),
						'opts'	=> array(
							'200px'	=> array( 'name' => '200px'),
							'250px'	=> array( 'name' => '250px'),
							'300px'	=> array( 'name' => '300px'),
							'350px'	=> array( 'name' => '350px'),
							'400px'	=> array( 'name' => '400px'),
						)
					),
						array(
							'type'	=> 'count_select',
							'key'	=> 'map_zoom_level',
							'default'	=> '12',
							'label'	=> __( 'Select Map Zoom Level (default 14)', 'pagelines' ),
							'count_start'	=> 1,
							'count_number'	=> 18,
							'default'		=> '14',
						),
						array(
							'type'	=> 'check',
							'key'	=> 'map_zoom_enable',
							'label'	=> __( 'Enable Zoom Controls', 'pagelines' ),
							'default'		=> true,
							'compile'		=> true,
						),
					array(
						'type'	=> 'check',
						'key'	=> 'enable_animation',
						'label'	=> __( 'Enable Animations', 'pagelines' ),
						'help'	=> __( 'This makes the markers appear one at a time, with a small bouncing animation.', 'pagelines' ),
						'default'		=> true,
						'compile'		=> true,
					),
					array(
						'type'	=> 'textarea',
						'key'	=> 'custom_style',
						'label'	=> __( 'Custom Style?', 'pagelines' ),
						'help' => 'Find map styles at <a href="http://snazzymaps.com/" target="_blank" class="btn btn-mini btn-primary">snazzymaps.com</a>',
						'scope'	=> 'global',
					),
				)

			);

		$options[] = array(
			'key'		=> 'locations_array',
			'type'		=> 'accordion',
			'col'		=> 2,
			'opts_cnt'	=> 1,
			'title'		=> __('Pointer Locations', 'pagelines'),
			'post_type'	=> __('Location', 'pagelines'),
			'opts'	=> array(
				array(
					'key'		=> 'image',
					'label' 	=> __( 'Pointer Image', 'pagelines' ),
					'type'		=> 'image_upload',
					'help'		=> __( 'For best results use an image size of 64 x 64 pixels.', 'pagelines' )
				),
				array(
					'key'	=> 'latitude',
					'label'	=> __( 'Latitude', 'pagelines' ),
					'type'	=> 'text_small',
					'place'	=> $this->lat,
					'help'	=> $this->help,
				),
				array(
					'key'	=> 'longitude',
					'label'	=> __( 'Longitude', 'pagelines' ),
					'type'	=> 'text_small',
					'place'	=> $this->lng,
					'help'	=> $this->help,
				),
				// array(
				// 	'key'	=> 'text',
				// 	'label'	=> 'Location Description',
				// 	'type'	=> 'textarea',
				// 	'default'	=> $this->desc,
				// 	'place'		=> $this->desc
				// ),
				array(
					'key'	=> 'address_line_1',
					'label'	=> 'Address Line 1',
					'type'	=> 'text',
					'default'	=> '{{office1-address1}}',
					'place'		=> '{{office1-address1}}'
				),
				array(
					'key'	=> 'address_line_2',
					'label'	=> 'Address Line 2',
					'type'	=> 'text',
					'default'	=> '{{office1-address2}}',
					'place'		=> '{{office1-address2}}'
				),
				array(
					'key'	=> 'phone',
					'label'	=> 'Phone',
					'type'	=> 'text',
					'default'	=> '{{office1-phone}}',
					'place'		=> '{{office1-phone}}'
				),
				array(
					'key'	=> 'map_link',
					'label'	=> 'Map Link',
					'type'	=> 'text',
					'default'	=> '{{office1-map-url}}',
					'place'		=> '{{office1-map-url}}'
				),
			)
		);
		return $options;
	}

   function section_template( ) {
		$height = $this->opt( 'map_height', array( 'default' => '350px' ) );	
		printf( '<div class="pl-map-wrap"><div id="pl_map_%s" data-map-id="%s" class="sms-map pl-end-height" style="height: %s"></div></div>', $this->meta['unique'], $this->meta['unique'], $height );
	}
}