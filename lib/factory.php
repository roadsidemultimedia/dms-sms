<?php
class smsFactoryData {

	public $markup_field_description = "<p>Use triple curly braces if you need to render HTML (<code>{{{token}}}</code> rather than <code>{{token}}</code>). By default double curly braces will escape HTML characters.</p><p>See the <a href='https://github.com/bobthecow/mustache.php/wiki/Mustache-Tags' target='_blank'>Mustache template engine documentation</a> for more info.</p>";
	public $less_field_description = '<p>Example of using the siteURL variable:</p><p><code>background: url(~"@{siteURL}/wp-content/uploads/image.png");</code></p>';
	public $repeater_title;
	public $repeater_token;

	public function __construct(){

	}
}
global $sms_factory_data;
$sms_factory_data = new smsFactoryData();

// add_action('shutdown', 'sms_save_data_on_shutdown');
// function sms_save_data_on_shutdown(){
//   global $sms_factory_data;
//   $sms_factory_data->save();
// };

class smsDmsFieldFactory {

	public $repeater_fields;
	public $boxSectionsRepeater;
	public $tokens = array();
	public $repeater_tokens = array();

	public $dms_field_types = array(
		'1' => array(
			'name'  => 'Text',
			'type'  => 'text',
			'scope' => 'single',
			'icon'  => 'el-icon-font',
		),
		'2' => array(
			'name'  => 'Textarea',
			'type'  => 'textarea',
			'scope' => 'single',
			'icon'  => 'el-icon-align-justify',
		),
		'4' => array(
			'name'  => 'Image',
			'type'  => 'image_upload',
			'scope' => 'single',
			'icon'  => 'el-icon-picture',
		),
		'5' => array(
			'name'  => 'Select',
			'type'  => 'select',
			'scope' => 'multi',
			'icon'  => 'el-icon-stackoverflow',
		),
		'6' => array(
			'name'  => 'Checkbox', 
			'type'  => 'check',
			'scope' => 'single',
			'icon'  => 'el-icon-check',
		),
		'7' => array(
			'name'  => 'Accordian / Repeater',
			'type'  => 'repeater',
			'scope' => 'multi',
			'icon'  => 'el-icon-magic',
		),
		'8' => array(
			'name'  => 'Icon Chooser',
			'type'  => 'select_icon',
			'scope' => 'single',
			'icon'  => 'el-icon-puzzle',
		),
	);

	function __construct($template_meta = null){
		global $sms_redux;
		global $sms_utils;
		$this->sms_redux = $sms_redux;
		$this->template_meta = $template_meta;
		if( function_exists('client_info_filter') ){
			$this->cit = true;
		}
		$this->data_source_map = array(
			"font-size-classes" => array(
				'data_source' => $sms_utils->sms_options['fonts']['size-class-list'],
			),
			"font-type-classes" => array(
				'data_source' => $sms_utils->get_font_type_classes(),
			),
			"font-weight-classes" => array(
				'data_source' => $sms_utils->sms_options['fonts']['weight-class-list'],
			),
			"letter-spacing-classes" => array(
				'data_source' => $sms_utils->sms_options['fonts']['letter-spacing-list'],
			),
			"line-height-classes" => array(
				'data_source' => $sms_utils->sms_options['fonts']['line-height-classes'],
			),
			"text-transform-classes" => array(
				'data_source' => $sms_utils->sms_options['fonts']['text-transform-classes'],
			),
			"heading-type-classes" => array(
				'data_source' => $sms_utils->sms_options['fonts']['heading-type-classes'],
			),
			"heading-tags" => array(
				'data_source' => $sms_utils->sms_options['fonts']['heading-tags'],
			),
			"pages" => array(
				'data_source' => $sms_utils->list_pages_hierarchically(),
			),
			"color-classes" => array(
				'data_source' => $sms_utils->sms_options['colors']['primary-classes'],
				'data_source_secondary' => $select_options_alt = $sms_utils->sms_options['colors']['brightness-list'],
			),
			"color-hex-custom" => array(
				'data_source' => $type = 'color',
			),
		);
	}

	function get_token_array(){
		$tokens = $this->tokens;
		$r_tokens = $this->repeater_tokens;

		$output = array();
		foreach ($this->tokens as $key => $value) {
			if( $r_tokens[$key] ){
				$output[$key] = $r_tokens[$key];

				// Add static tokens for the index
				$output[$key][] = 'index';
				$output[$key][] = 'index0';
			} else {
				$output[$key] = $value;
			}
		}
		// add static token for unique identifier of section
		$output['uid'] = 'uid';

		return $output;
	}

	function get_token_list_string(){
		$token_array = $this->get_token_array();
		$string = "<p>Available template tokens</p>";
		$string .= "<p>";
		foreach ($token_array as $key => $token) {
			if( is_array($token) ){
				$string .= "<ul class='token-list'>";
				$string .= "<code class='token-block repeater-token'><strong>{$key}</strong></code> ◀ repeater field";
				foreach ($token as $repeater_token) {
					$string .= "<li class='no-bullet'><span class='rotate-cw-90'>↩</span><code class='token-block repeater-token'><abbr title='{$key}.0.{$repeater_token}'>{$repeater_token}</abbr></code></li>";
				}
				$string .= "</ul>";
			} else{
				$string .= "<code class='token-block'>{$token}</code> ";
			}
		}
		$string .= "</p>";
		return $string;
	}

	function get_metabox_field_generator($factory_type = null){

		global $sms_utils;
		$template_meta = $this->template_meta;

		$dms_field_scopes = array();
		foreach ($this->dms_field_types as $value) {
			$dms_field_scopes[ $value['type'] ] = $value['scope'];
		}
		$dms_field_types_redux = array_column($this->dms_field_types, 'name', 'type');

		if( $template_meta['dms-field-total'] > 0 ){

			for ($i=1; $i <= $template_meta['dms-field-total']; $i++) { 

				$fields[] = array(
					'title'          => 'Field Type',
					'subtitle'       => 'Note: You must save before you can set the default output for this field.',
					'id'             => "dms-field$i-type",
					'type'           => 'select',
					'options'        => $dms_field_types_redux,
				);

				$fields[] = array(
					'title'          => "Title",
					'desc'           => 'DMS field title displayed in DMS section editor.',
					'id'             => "dms-field$i-title",
					'type'           => 'text',
				);

				$fields[] = array(
					'title'          => "Field Template Token",
					'desc'           => 'Used to output this field\'s data in the template. Should be lowercase with no spaces.',
					'id'             => "dms-field$i-token",
					'type'           => 'text',
				);

				$fields[] = array(
					'title'          => "Help Text / Description",
					'id'             => "dms-field$i-description",
					'desc'           => 'Adds text below the DMS field title.',
					'type'           => 'text',
				);

				$field_type = $template_meta['dms-field'.$i.'-type'];
				$field_type_scope = $dms_field_scopes[ $field_type ];

				$field_token = $template_meta["dms-field$i-token"];
				$field_token_slug = $sms_utils->to_slug( $field_token );

				if($field_token)
					$this->tokens[$field_token_slug] = $field_token_slug;

				if( $field_type_scope == 'multi' ){

					if( $field_type == 'repeater' ){

						// Save repeater field title for later use as metabox title
						$this->repeater_title = $template_meta["dms-field$i-title"];
						$this->repeater_token = $field_token_slug;

						$fields[] = array(
							'id'             => "dms-field$i-repeater-options-total",
							'title'          => 'Number of available options for this repeater item',
							'type'           => 'slider',
							'desc'           => 'How many repeater fields?',
							"default"        => 1,
							"min"            => 1,
							"step"           => 1,
							"max"            => 20,
							'display_value'  => 'text'
						);

						$total_repeater_options = $template_meta["dms-field$i-repeater-options-total"];

						if( $total_repeater_options ){
							for ($j=1; $j <= $total_repeater_options; $j++) { 
								
								// Remove repeater option from list to prevent crazy recursion
								$dms_field_types_redux_wo_repeater = $dms_field_types_redux;
								unset($dms_field_types_redux_wo_repeater['repeater']);

								$repeater_token = $template_meta["dms-field$i-repeater$j-token"];
								$repeater_token_slug = $sms_utils->to_slug($template_meta["dms-field$i-repeater$j-token"]);
								
								if($repeater_token)
									$this->repeater_tokens[$field_token_slug][] = $repeater_token_slug;

								$repeater_fields[] = array(
									'title'          => "Field Type",
									'subtitle'       => 'Note: You must save before you can set the default output for this field.',
									'id'             => "dms-field$i-repeater$j-type",
									'type'           => 'select',
									'options'        => $dms_field_types_redux_wo_repeater,
								);

								$repeater_fields[] = array(
									'title'          => "Title",
									'desc'           => 'DMS field title displayed in DMS section editor.',
									'id'             => "dms-field$i-repeater$j-title",
									'type'           => 'text',
								);

								$repeater_fields[] = array(
									'title'          => "Field Template Token",
									'desc'           => 'Used to output this field\'s data in the template. Should be lowercase with no spaces.',
									'id'             => "dms-field$i-repeater$j-token",
									'type'           => 'text',
								);

								$repeater_fields[] = array(
									'title'          => "Help Text / Description",
									'id'             => "dms-field$i-repeater$j-description",
									'desc'           => 'Adds text below the DMS field title.',
									'type'           => 'text',
								);

								$repeater_field_type = $template_meta['dms-field'.$i.'-repeater'.$j.'-type'];
								$repeater_field_type_scope = $dms_field_scopes[ $repeater_field_type ];
								if( $repeater_field_type_scope == 'multi' ){
									$repeater_fields[] = array(
										'title'          => 'Data Source',
										'id'             => "dms-field$i-repeater$j-data-source",
										'subtitle'       => 'Refresh page to configure data.',
										'type'           => 'select',
										'options'        => array(
											// 'font-size-classes'       => 'Font Size Classes [.fs-medium, .fs-large]',
											// 'font-weight-classes'     => 'Font Weights Classes [.fw-100 - .fw-800]',
											// 'font-type-classes'       => 'Font Type Classes [.font-serif, .font-sans-serif, etc...]',
											// 'line-height-classes'     => 'Line Height Classes [.lh-0-1, .lh-0-2, etc...]',
											// 'letter-spacing-classes'  => 'Letter Spacing Classes [.ls-1, ls-2, etc...]',
											// 'font-transform'          => 'Font Transform Classes [.uppercase, .small-caps, etc...]',
											// 'colors-classes'          => 'Colors (classes)',
											// 'colors-variables'        => 'Colors (less variables)',
											// 'heading-selectors'       => 'Heading Tag Selectors [H1-H6]',
											// 'heading-class-selectors' => 'Heading Class Selectors [sms-heading-, Subheading, Accent heading]',
											'manual-entry' => 'Manual Entry',
										),
									);
								} else {
									// repeater field scope == single
									$repeater_fields[] = array(
										'type'           => 'text',
										'id'             => "dms-field$i-repeater$j-option-default-enabled",
										'title'          => 'Enabled value',
										'required'       => array("dms-field$i-repeater$j-type", 'equals', 'check'),
										'subtitle'       => "<p>This is the value that will display when the checkbox is enabled</p><p>Besides merely displaying a value, you can also selectively display content based on the state of this checkbox like so: </p><p><code>{{#".$repeater_token_slug."}}content{{/".$repeater_token_slug."}}</code><p><p><strong>Note:</strong> The default value must be blank, and the enabled value must contain <code>true</code> for this to work.</p>",
									);

								}

								$repeater_fields[] = array(
									'type'           => 'text',
									'id'             => "dms-field$i-repeater$j-option-default",
									'title'          => 'Default value',
									'subtitle'       => $repeater_token_slug ? "Template tag: <code>{{".$repeater_token_slug."}}</code>" : "",
								);

								if( 'manual-entry' == $template_meta["dms-field$i-repeater$j-data-source"] ) {
									
									$repeater_fields[] = array(
										'id'             => "dms-field$i-repeater$j-options-total",
										'title'          => 'Number of available options for this field',
										'type'           => 'slider',
										'desc'           => 'How many option fields?',
										"default"        => 1,
										"min"            => 1,
										"step"           => 1,
										"max"            => 20,
										'display_value'  => 'text'
									);

									$total_options = $template_meta["dms-field$i-repeater$j-options-total"];

									if( $total_options ){
										for ($k=1; $k <= $total_options; $k++) { 
											$repeater_fields[] = array(
												'title'          => "Option #$k",
												'id'             => "dms-field$i-repeater$j-option$k-section-title",
												'type'           => 'section',
												'indent'         => true,
											);
											$repeater_fields[] = array(
												'title'          => "Title",
												'id'             => "dms-field$i-repeater$j-option$k-title",
												'type'           => 'text',
											);
											$repeater_fields[] = array(
												'title'          => "Value",
												'id'             => "dms-field$i-repeater$j-option$k-value",
												'type'           => 'text',
											);
										}
									}

								} // end manual-entry field generation

								$repeater_field_type_slug = $template_meta["dms-field$i-repeater$j-type"];
								$field_icon_array = array_column($this->dms_field_types, 'icon', 'type');
								$repeater_icon = $field_icon_array[$repeater_field_type_slug];

								$title = $template_meta["dms-field$i-repeater$j-title"] ?: '* Repeater Field #'.$j;

								// add fields to metabox sections
								$this->boxSectionsRepeater[] = array(
									'title' => $title,
									'id' => "dms-field$i-repeater$j",
									'icon' => $repeater_icon,
									'fields' => $repeater_fields,
								);
								$repeater_fields = array();

							}
							// echo "<pre>\$this->boxSectionsRepeater: " . print_r($this->boxSectionsRepeater, true) . "</pre>";
						}

					} else {
						// Note to self. LESS vars could be used for inline styles if they were pumped through the compiler and output rgba and hex values
						$fields[] = array(
							'title'          => 'Data Source',
							'id'             => "dms-field$i-data-source",
							'subtitle'       => 'Refresh page to configure data.',
							'type'           => 'select',
							'options'        => array(
								'manual-entry'            => 'Manual Entry',
								'font-size-classes'       => 'Font Size Classes',
								'font-weight-classes'     => 'Font Weight Classes',
								'font-type-classes'       => 'Font Type Classes',
								'line-height-classes'     => 'Line Height Classes',
								'letter-spacing-classes'  => 'Letter Spacing Classes',
								'text-transform-classes'  => 'Font Transform Classes',
								'heading-type-classes'    => 'SMS Heading Classes',
								'heading-tags'            => 'Heading Tags (H1-H6)',
								'pages'                   => 'Page List',
								
								'color-hex-custom'        => 'Color (Custom Hex)',
								'color-classes'           => 'Color (Theme Classes)',
								// 'color-hex'               => 'Color (Theme Hex)',

								// 'icon-picker'             => 'Icon Classes (Font Awesome)',
								// 'color-variables'         => 'Colors (less variables)',
							),
						);

						$fields[] = array(
						 'title'          => 'Color Selector',
						 'id'             => "dms-field$i-color-selector",
						 'type'           => 'select',
						 'options'        => array(
						   'text'   => 'Text',
						   'bg'     => 'background',
						   'border' => 'border',
						   'a'      => 'link',
						   'icon'   => 'icon',
						 ),
						 'required' => array("dms-field$i-data-source", 'equals', 'color-classes'),
						);

						// $fields[] = array(
						//  'title'          => 'Color Important?',
						//  'id'             => "dms-field$i-color-important",
						//  'type'           => 'checkbox',
						//  'required' => array("dms-field$i-data-source", 'equals', 'color-classes'),
						// );

						// $fields[] = array(
						// 	'type'           => 'select',
						// 	'id'             => "dms-field$i-option-default",
						// 	'title'          => 'Default value',
						// 	// 'required'       => array("dms-field$i-custom-output", 'equals', false),
						// 	'options' => $sms_utils->sms_options['fonts']['size-class-list'],
						// );
					
					}

					// Manual Entry
					$fields[] = array(
						'id'             => "dms-field$i-options-total",
						'title'          => 'Number of available options for this field',
						'type'           => 'slider',
						'desc'           => 'How many option fields?',
						"default"        => 1,
						"min"            => 1,
						"step"           => 1,
						"max"            => 40,
						'display_value'  => 'text',
						'required' => array("dms-field$i-data-source", 'equals', 'manual-entry'),
					);

					// Generate fields for select list
					if( 'manual-entry' == $template_meta["dms-field$i-data-source"] ) {
						
						$total_options = $template_meta["dms-field$i-options-total"];

						if( $total_options ){
							for ($j=1; $j <= $total_options; $j++) { 
								$fields[] = array(
									'title'          => "Option #$j",
									'id'             => "dms-field$i-option$j-section-title",
									'type'           => 'section',
									'indent'         => true,
								);
								$fields[] = array(
									'title'          => "Title",
									'id'             => "dms-field$i-option$j-title",
									'type'           => 'text',
								);
								$fields[] = array(
									'title'          => "Value",
									'id'             => "dms-field$i-option$j-value",
									'type'           => 'text',
								);
							}
						}

					} // end manual-entry field generation

				} elseif( $field_type_scope == 'single' ) {

					// ///////////////////////////////////////////////////////////////////////////
					// Field scope: single
					// ///////////////////////////////////////////////////////////////////////////

					

					$fields[] = array(
						'type'           => 'text',
						'id'             => "dms-field$i-option-default-enabled",
						'title'          => 'Enabled value',
						'required'       => array("dms-field$i-type", 'equals', 'check'),
						'subtitle'       => "<p>This is the value that will display when the checkbox is enabled</p><p>Besides merely displaying a value, you can also selectively display content based on the state of this checkbox like so: </p><p><code>{{#".$field_token."}}content{{/".$field_token."}}</code><p><p><strong>Note:</strong> The default value must be blank, and the enabled value must contain <code>true</code> for this to work.</p>",
					);

				}

				if( $field_type !== 'repeater' ) {

					$fields[] = array(
						'type'           => 'text',
						'id'             => "dms-field$i-option-default",
						'title'          => 'Default value',
						'subtitle'       => $field_token ? "Template tag: <code>{{".$field_token."}}</code>" : "",
						// 'required'       => array("dms-field$i-custom-output", 'equals', false),
					);

				}
				
				$field_type_slug = $template_meta["dms-field$i-type"];
				$field_icon_array = array_column($this->dms_field_types, 'icon', 'type');
				$icon = $field_icon_array[$field_type_slug];

				$section_title = $template_meta["dms-field$i-title"] ?: '* Field #'.$i;

				// add fields to metabox sections
				$boxSections[] = array(
					'title' => $section_title,
					'id' => "dms-field$i",
					'icon' => $icon,
					'fields' => $fields,
				);
				$fields = array();

			} //end forloop

		
		} // end if( $template_meta['dms-field-total'] > 0 )


		// Generator Field
		// -------------------
		$fields = array();
		
		$fields[] = array(
			'id'             => 'dms-field-total',
			'title'          => 'Number of available content fields',
			'type'           => 'slider',
			'subtitle'       => 'These options will be selectable in the DMS section. The title is displayed in the dropdown list. The value is what is output int the template',
			'desc'           => 'How many content fields?',
			"default"        => 0,
			"min"            => 0,
			"step"           => 1,
			"max"            => 20,
			'display_value'  => 'text'
		);

		// add fields to boxsections
		$boxSections[] = array(
			'icon'   => $icon = 'el-icon-plus-sign',
			'fields' => $fields,
			'id'     => 'dms-field-field-total',
			'title'  => 'DMS Field Generator',
		);

		$metaboxes = array(
			'id' => "01-generator-fields",
			'title' => "DMS Section Field Settings",
			'priority' => "high",
			'post_types' => array("sms_$factory_type"),
			'sections' => $boxSections,
		);

		return $metaboxes;
	}

	function get_metabox_field_generator_repeater($factory_type = null){
		$repeater_fields = $this->boxSectionsRepeater;
		if( isset($repeater_fields) ){
			$metaboxes = array(
				'id' => "02-generator-repeater-fields",
				'title' => "∞ Repeater Fields for ".$this->repeater_title." {{".$this->repeater_token."}}",
				'priority' => "high",
				'post_types' => array("sms_$factory_type"),
				'sections' => $repeater_fields,
			);
			return $metaboxes;
		} else {
			return false;
		}
	}

	function get_metabox_custom_markup($factory_type = null){
		
		global $sms_factory_data;
		$template_tokens = $this->get_token_list_string();
		$fields = array();

		// Markup
		$fields[] = array(
			'id'        => 'markup',
			'type'      => 'ace_editor',
			'title'     => 'Custom Markup',
			'mode'      => 'html',
			'theme'     => 'monokai',
			'desc'      => "$template_tokens",
			'subtitle'  => $sms_factory_data->markup_field_description,
		);

		// CSS
		$fields[] = array(
			'id'        => 'less',
			'type'      => 'ace_editor',
			'title'     => 'Custom LESS',
			'mode'      => 'less',
			'theme'     => 'monokai',
			'desc'      => $sms_factory_data->less_field_description,
		);

		

		// Create a Meta Box and reset the meta box section array
		//------------------------------------------------------------------
		$metaboxes = array(
			'id' => '03-markup',
			'title' => 'HTML &amp; CSS',
			'priority' => "low",
			'post_types' => array("sms_$factory_type"),
			'sections' => array(array('fields' => $fields))
		);

		return $metaboxes;
	}

	function get_metabox_global_custom_markup($factory_type = null){
		$fields = array();

		//Markup
		$fields[] = array(
			'id'        => 'global-markup',
			'type'      => 'ace_editor',
			'title'     => 'Global Markup',
			'mode'      => 'html',
			'theme'     => 'monokai',
			'subtitle'  => "<p>This field will render once per template. It's perfect for adding javascript that applies to every instance of this template.</p>",
			// 'desc'      => "<p><a href='http://kenwheeler.github.io/slick/'>Slick Slider Documentation</a></p>",
		);

		// Create a Meta Box and reset the meta box section array
		//------------------------------------------------------------------
		$metaboxes = array(
			'id' => '04-global-markup',
			'title' => 'Global HTML &amp; Javascript',
			'priority' => "low",
			'post_types' => array("sms_$factory_type"),
			'sections' => array(array('fields' => $fields))
		);

		return $metaboxes;
	}

	// Loop through global section style slots and produce a selectable list with ID as the key
	function get_style_list($prefix, $count, $id){
		global $sms_utils;
		global $sms_redux;
		$style_list = array();
		for ($i=1; $i <= $count; $i++) {
			$id = $sms_redux[ "$prefix-slot$i" ];
			if($id){
				$title = get_the_title($id);
				$style_list["$prefix-slot$i"] = "Style $i - $title";
			}
		}
		return $sms_utils->convert_redux_choices_to_dms( $style_list );
	}

	function generate_frontend_fields( $template_id, $parent_obj, $factory_type ){
		global $sms_utils;

		$template_meta = $sms_utils->get_redux_post_meta( $template_id );
		$factory_slots = constant("SMS_".strtoupper($factory_type)."_SLOTS");
		$style_list = $this->get_style_list($factory_type, $factory_slots, $template_id);

		$fields = array();
		$field_total = $template_meta['dms-field-total'];
		for ($i=1; $i <= $field_total; $i++) {

			$key          = $parent_obj->id."_field$i";
			$title        = $template_meta["dms-field$i-title"];
			$token        = $template_meta["dms-field$i-token"];
			$token_slug   = $sms_utils->to_slug( $token );
			$type         = $template_meta["dms-field$i-type"];
			$help         = $template_meta["dms-field$i-description"] ?: '';

			if($type == 'select'){
				$data_source  = $template_meta["dms-field$i-data-source"] ?: '';
			} else {
				$data_source = '';
			}

			// $fields[] = array(
			// 	'title' => 'Diagnostics for field #' .$i,
			// 	'key'   => 'diagnostic'.$i,
			// 	'type'  => 'text',
				
			// 	'help'  => '<pre>'.
			// 	"\$key: " . print_r($key, true) . "<br>".
			// 	"\$token: " . print_r($token, true)."<br>".
			// 	"\$token_slug: " . print_r($token_slug, true)."<br>".
			// 	"\$type: " . print_r($type, true). "<br>".
			// 	"\$data_source: " . print_r($data_source, true) . "<br>".
			// 	"</pre>",
			// );

			if( $type == "select" ){

				if( $data_source == "manual-entry" ){

					// Collect data for select fields
					$select_total = $template_meta["dms-field$i-options-total"];
					if( $select_total > 0 ){
						$select_options = array();
						for ($j=1; $j <= $select_total; $j++) {
							$option_title = $template_meta["dms-field$i-option$j-title"];
							$option_value = $template_meta["dms-field$i-option$j-value"];
							$select_options[$option_value] = $option_title;
						}
					}

				} else if( $data_source == "color-hex-custom" ) {

					$type = 'color';

				} else {

					$select_options = $this->data_source_map[$data_source]['data_source'];
					$select_options_alt = $this->data_source_map[$data_source]['data_source_secondary'];
					// $select_options_alt = $sms_utils->sms_options['colors']['brightness-list'];

				}

				// Convert select array to DMS array
				$select_opts  = $select_options ? $sms_utils->convert_redux_choices_to_dms($select_options) : false;
				$select_opts_alt  = $select_options_alt ? $sms_utils->convert_redux_choices_to_dms($select_options_alt) : false;
			}

			if( 'repeater' === $type ){

				$repeater_field_total = $template_meta["dms-field$i-repeater-options-total"];
				for ($j=1; $j <= $repeater_field_total; $j++) {

					$repeater_token = $template_meta["dms-field$i-repeater$j-token"];
					$repeater_token_slug = $sms_utils->to_slug($template_meta["dms-field$i-repeater$j-token"]);

					$repeater_select_options = array();

					if( isset($repeater_token) && $repeater_token ){
						// Collect data for repeater select fields
						$repeater_select_total = $template_meta["dms-field$i-repeater$j-options-total"];
						if( $repeater_select_total > 0 ){
							for ($k=1; $k <= $repeater_select_total; $k++) { 
								$repeater_select_title = $template_meta["dms-field$i-repeater$j-option$k-title"];
								$repeater_select_value = $template_meta["dms-field$i-repeater$j-option$k-value"];
								$repeater_select_options[$repeater_select_value] = $repeater_select_title;
							}
						}

						// Collect data for field
						$repeater_title           = $template_meta["dms-field$i-repeater$j-title"];
						$repeater_key             = $repeater_token_slug;
						$repeater_type            = $template_meta["dms-field$i-repeater$j-type"];
						$repeater_help            = $template_meta["dms-field$i-repeater$j-description"] ?: '';
						$repeater_select_options  = $repeater_select_options ? $sms_utils->convert_redux_choices_to_dms($repeater_select_options) : false;

						if( isset($repeater_type) && $repeater_type ){
							
							$repeater_fields[$j] = array(
								'key'   => $repeater_key,
								'type'  => $repeater_type,
								'title' => $repeater_title,
								'help'  => $repeater_help,
								'imgsize'  => '2',
							);

							// echo "<pre>\$repeater_type: " . print_r($repeater_type, true) . "</pre>";
							// if( $repeater_type === 'image_upload' )
							// 	$repeater_fields[$j]['imgsize'] = '2';
								
							if( $repeater_type === 'select' )
								$repeater_fields[$j]['opts'] = $repeater_select_options;

						}

					}

				}

				$repeater_field_set[] = array(
					'title'     => $title,
					'key'       => $key,
					'type'      => 'accordion',
					'opts_cnt'  => 2, //default number of accordians
					'opts'      => $repeater_fields,
				);

			} else {
				// If not a repeater field

				$fields[] = array(
					'key'   => $key,
					'type'  => $type,
					'title' => $title,
					'help'  => $help,
					'opts'  => $select_opts,
				);

				

				if( $data_source == 'color-classes' ){

					$fields[] = array(
						'key'   => 'temp',
						'type'  => 'text',
						'title' => 'title',
						'help'  => "<pre>\$data_source: " . print_r($data_source, true) . "</pre>",
						// 'opts'  => $select_opts,
					);

					$fields[] = array(
						'key'   => $key."-brightness",
						'type'  => 'select',
						'title' => $title." Brightness",
						'opts'  => $select_opts_alt,
					);
					$fields[] = array(
						'key'   => $key."-important",
						'type'  => 'check',
						'title' => "$title <strong style='color: red'>!important</strong>?",
					);

				}
			}

		}
		
		// Section Options
		$options[] = array(
			'type'  => 'multi', // Here you can nest multiple options.
			'title' => 'Section Options',
			'opts'  => $fields,
			'col'   => 1,
		);

		$post_edit_url = get_edit_post_link( $template_id );
		$post_create_url = "/wp-admin/post-new.php?post_type=sms_$factory_type";
		$post_assign_url = "/wp-admin/admin.php?page=dms-sms.php";
		$numbered_style_list = array_keys($style_list);
		$options[] = array(
			'type'  => 'multi', // Here you can nest multiple options.
			'title' => 'Template',
			'col'   => 2,
			'opts'  => array(
				array(
					'key'   => $parent_obj->id.'_style',
					'type'  => 'select',
					'title' => 'Style',
					'opts'  => $style_list,
					'default'		=> $numbered_style_list[0],
					'compile'		=> true,
					'help'  => ''.
					($style_list ? '' : "<a class='btn btn-primary btn-mini' target='_blank' href='$post_create_url'>Create an Accordion Template</a>").
					($style_list ? 'Refresh after changing templates to see the corresponding fields for this template.' : '').
					($template_id ? "<a class='btn btn-primary btn-mini' style='margin-top: 4px;' target='_blank' href='$post_edit_url'>Edit this template</a>" : '').
					($template_id ? "<a class='btn btn-secondary btn-mini' style='margin-left: 4px; margin-top: 4px;' target='_blank' href='$post_assign_url'>Activate Template(s) Globally</a>" : ''),
				),
			),
		);
		 // echo "<pre>\$style_list: " . print_r($style_list, true) . "</pre>";
		 // $sms_utils->admin_notices['updated'][]

		if( isset($repeater_fields) ){
			$options[] = array(
				'type'  => 'multi', // Here you can nest multiple options.
				'title' => $repeater_field_set[0]['title'],
				// '.$template_meta["dms-field$i-repeater$j-title"]
				'col'   => 2,
				'opts'  => $repeater_field_set,
			);
		}

		// echo "<pre>Title: " . print_r(, true) . "</pre>";
		// echo "<pre>\$repeater_field_set: " . print_r($repeater_field_set, true) . "</pre>";


		return $options;
	}

	// If no template selected, select the first available for that post type
	function validate_template_id_by_slot_slug( $slot_slug, $slug ){
		global $sms_redux;
		$template_id = $sms_redux[ $slot_slug ];
		if(!isset( $template_id )){
			// set to first slot if no template is selected
			$template_id = $sms_redux[ "$slug-slot1" ];
		}
		return $template_id;
	}

	function render_frontend( $template_id, $parent_obj ){
		global $sms_utils;
		$edit = false;
		if( is_object( $parent_obj->pldraft ) && 'draft' == $parent_obj->pldraft->mode )
			$edit = true;

		$template_meta = $sms_utils->get_redux_post_meta( $template_id );

		// If there's no markup, pump out some default markup
		// $template_markup = $template_meta['markup'] ?: pl_blank_template($parent_obj->name.' Section');
		$template_markup = $template_meta['markup'] ?: setup_section_notify( $parent_obj , "Select a template to get this {$parent_obj->name} up and running.");

		$field_total = $template_meta['dms-field-total'];

		// Acquire token data from DMS section field info
		$field_tokens = array();
		for ($i=1; $i <= $field_total; $i++) {
			$token = $template_meta["dms-field$i-token"];
			$type = $template_meta["dms-field$i-type"];

			if($type == 'select'){
				$data_source = $template_meta["dms-field$i-data-source"] ?: false;
			} else {
				$data_source = false;
			}
			$enabled_data = $template_meta["dms-field$i-option-default-enabled"];

			// $sync_id = 'mediabox_height';
			// $sync_mode = 'css';
			// $sync_target = 'min-height';
			// $sync_post = 'px';
			// $sync_data = ($edit) ? 'data-sync="'.$sync_id.'" data-sync-mode="'.$sync_mode.'" data-sync-target="'.$sync_mode.'" data-sync-post="'.$sync_post.'"' : '';

			if($type == 'check' && isset($enabled_data) && $parent_obj->opt( $parent_obj->id."_field$i" ) ){
				// If checkbox field has data for enabled state & it's enabled in the section,
				$field_tokens[$token] = $this->cit ? client_info_filter( do_shortcode($template_meta["dms-field$i-option-default-enabled"]) ) : do_shortcode($template_meta["dms-field$i-option-default-enabled"]);
			} elseif($type == 'repeater') {

				$repeater_total = $template_meta["dms-field$i-repeater-options-total"];
				if(isset($repeater_total) && $repeater_total > 0){

					$dms_repeater_array = $parent_obj->opt( $parent_obj->id."_field{$i}" );
					$dms_repeater_array_total = count($dms_repeater_array);

					for ($j=0; $j < $dms_repeater_array_total; $j++) {
						
						$field_tokens[$token][$j]['index'] = $j+1;
						$field_tokens[$token][$j]['index0'] = $j;

						$keys = array_keys( $dms_repeater_array );
						$repeater_item = $dms_repeater_array[ $keys[$j] ];
						$repeater_field_total = $template_meta["dms-field$i-repeater-options-total"];

						for ($k=1; $k <= $repeater_field_total; $k++) { 
							$repeater_token = $template_meta["dms-field$i-repeater$k-token"];
							$repeater_type = $template_meta["dms-field$i-repeater$k-type"];
							$repeater_default = $template_meta["dms-field$i-repeater$k-option-default"];
							$repeater_value = $repeater_item[$repeater_token];
							$repeater_enabled_data = $template_meta["dms-field$i-repeater$k-option-default-enabled"];

							if( $repeater_type == 'check' && isset($repeater_enabled_data) && $repeater_value ){
								// If checkbox field has data for enabled state & it's enabled in the section
								$field_tokens[$token][$j][$repeater_token] = $this->cit ? client_info_filter( do_shortcode( $repeater_enabled_data ) ) : do_shortcode( $repeater_enabled_data );
							} elseif($repeater_value){
								// Use set value
								$field_tokens[$token][$j][$repeater_token] = $this->cit ? client_info_filter( do_shortcode( $repeater_value ) ) : do_shortcode( $repeater_value );
							} else {
								// Use default value
								$field_tokens[$token][$j][$repeater_token] = $this->cit ? client_info_filter( do_shortcode( $repeater_default ) ) : do_shortcode( $repeater_default );
							}
						}
					}

				}
			} else { 
				$val = $parent_obj->opt( $parent_obj->id."_field$i" );

				// Icon Field Type
				if($type == 'select_icon'){
					$val = "fa fa-{$val}";
				}

				// Color Class data source for select field
				if( $data_source == 'color-classes' && $val ){
					$brightness = $parent_obj->opt( $parent_obj->id."_field$i-brightness" ) ? '-'.$parent_obj->opt( $parent_obj->id."_field$i-brightness" ) : '';
					if($template_meta["dms-field$i-color-selector"] == 'text'){
						$template_meta["dms-field$i-color-selector"] = '';
					}
					$color_type = $template_meta["dms-field$i-color-selector"] ? '-'.$template_meta["dms-field$i-color-selector"] : '';
					$important = $parent_obj->opt( $parent_obj->id."_field$i-important" ) ? 'i' : '';
					$val = "{$val}{$brightness}{$color_type}{$important}";
				}
				// Custom Color data source for select field
				if( $data_source == 'color-hex-custom' && $val){
					$val = "#{$val}";
				}
				// Pump through client info tag filter if available
				$val = $this->cit ? client_info_filter( $val ) : $val;

				$field_tokens[$token] = $val ? do_shortcode( $val ) : do_shortcode( $template_meta["dms-field$i-option-default"] );
			}
		}
		// Add clone ID to the list of tokens available for use as a unique identifier of the section
		$field_tokens['uid'] = $parent_obj->meta[ 'clone' ];
		

		// echo "<pre style='text-align: left !important;'>\$field_tokens: " . print_r($field_tokens, true) . "</pre>";

		// Start the mustache engine
		$m = new Mustache_Engine;

		// Render the template with the set values
		return do_shortcode($m->render($template_markup, $field_tokens));
	}

}