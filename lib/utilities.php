<?php
class smsUtilities{

	public $sms_redux;
	public $sms_options;
	public $sms_less;
	public $sms_css;
	public $global_selected_font_kit_id;
	public $global_selected_font_kit_meta;
	public $admin_notices = array();
	public $dev_mode;

	function __construct(){
		global $sms_redux;
		
		if (!isset($GLOBALS['sms_redux'])) {
			$GLOBALS['sms_redux'] = get_option('sms_redux', array());
		}

		$this->sms_redux = $sms_redux;
		// $this->redux_config = $reduxConfig;
		$this->sms_options = get_option('sms_options');
		$this->sms_less = get_option('sms_less');
		$this->sms_css = get_option('sms_css');
		$this->global_selected_font_kit_id = $this->sms_redux['font-kit'];
		$this->global_selected_font_kit_name = get_the_title($this->sms_redux['font-kit']);
		$this->global_selected_font_kit_meta = $this->get_redux_post_meta( $this->global_selected_font_kit_id );
		$this->global_selected_color_scheme_id = $this->sms_redux['color-scheme'];
		$this->global_selected_color_scheme_name = get_the_title($this->sms_redux['color-scheme']);
		$this->global_selected_color_scheme_meta = $this->get_redux_post_meta( $this->global_selected_color_scheme_id );
		$this->dev_mode = $sms_redux['sms-dev-mode'];

		add_action( 'admin_notices', array( $this , 'display_admin_notices' ) );
	}

	public function compile_less( $less, $source = null ){

		// // get the cached data
		// $cache_data = get_transient("sms_post_${post_id}_less");

		require_once( PL_INCLUDES . '/less.plugin.php' );
		$lparser = new plessc();

		if($this->dev_mode == true){
			$lparser->setPreserveComments(true);
			$lparser->setFormatter("classic");
		} else {
			$lparser->setPreserveComments(false);
			$lparser->setFormatter("compressed");
		}

		$pagelines_parser = new PageLinesLESSEngine();
		
		// Get Mixins
		$mixins = pl_file_get_contents( DMS_SMS_PATH . "assets/less/mixins.less" );

		// Get DMS & SMS LESS variables
		$less_var_array = $pagelines_parser->core_less_vars();
		$less_vars = $pagelines_parser->lessify_vars( $less_var_array ); 
		$less = $mixins . $less_vars . $less;

		try {
			return $lparser->compile( $less );
		} catch ( Exception $e) {
			if($source){
				$this->admin_notices['error'][] = "<strong>Less compile error from $source: </strong><pre>".$e->getMessage()."</pre>";
			} else {
				$this->admin_notices['error'][] = "Less compile error: <pre>".$e->getMessage()."</pre>";
			}
		}
	}

	/**
	 * Converts a string to a slug
	 * 
	 * Input: This is My Title.
   * Returns: this-is-my-title
	 */
	
	function to_slug($string){
		$string = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		$string = rtrim($string,"-");
		return $string;
	}

	function get_current_full_url($include_query_vars = false){
		$urlprotocol = 'http'; if ($_SERVER["HTTPS"] == "on") {$urlprotocol .= "s";} $urlprotocol .= "://";
		$urldomain = $_SERVER["SERVER_NAME"];
		$urluri = $_SERVER['REQUEST_URI'];
		$urlvars = basename($urluri);
		$urlpath = str_replace($urlvars,"",$urluri);

		if($include_query_vars){
			$urlfull = $urlprotocol . $urldomain . $urlpath . $urlvars;
		} else {
			$urlfull = $urlprotocol . $urldomain . $urlpath;
		}
		return $urlfull;
	}

	/**
	 * Formats and echos any notice messages in the $this->admin_notices array();
	 *
	 * Usage inside this class: 
	 * $this->admin_notices['error'][] = 'Error message here';
	 * $this->admin_notices['updated'][] = 'Update message here';
	 * 
	 * Usage outside this class: 
	 * $sms_utils->admin_notices['error'][] = 'Error message here';
	 * $sms_utils->admin_notices['updated'][] = 'Update message here';
	 */
	public function display_admin_notices( $return = FALSE ){
		if( ! empty( $this->admin_notices ) ){
			// Remove an empty and then sort
			array_filter( $this->admin_notices );
			ksort( $this->admin_notices );
			$output = '';
			foreach( $this->admin_notices as $key => $value ){
				// Probably an array but best to check
				if( is_array( $value ) ){
					foreach( $value as $v ){
						$output .= '<div class="' . esc_attr( $key ) . '"><p>' . $v . '</p></div>';
					}
				} else {
					$output .= '<div class="' . esc_attr( $key ) . '"><p>' . $value . '</p></div>';
				}
			}
			if( $return ){
					return $output;
			} else {
					echo $output;
			}
		}
	}

	/**
	 * Converts the redux post meta into a common format. 
	 * The data storage format was changed in version 1.2.3
	 * @param  integer $id The post's ID
	 * @return array       returns the post meta formatted a key pair associative array.
	 */
	public function get_redux_post_meta($id){
		$meta = get_post_meta($id, '', true);

		// if using old redux storage format:
		if( isset( $meta['sms_redux'] ) ){
			$unserialized_value = get_post_meta($id, 'sms_redux', true);
			return $unserialized_value;

		// if using new redux storage format:
		} elseif($meta) {
			foreach ($meta as $key => $value) {
				$unserialized_value = get_post_meta($id, $key, true);
				// $unserialized_value = redux_post_meta( "sms_redux", $id, $key );
				// echo "<pre>\$unserialized_value: " . print_r($unserialized_value, true) . "</pre>";
				$new_meta[$key] = $unserialized_value;
			}
			return $new_meta;
		} else {
			return false;
		}
	}

	public function get_first_item_in_associative_array( $array ){
		return key($array); 
	}

	// THIS METHOD IS A WORK IN PROGRESS
	public function get_active_global_post_id($post_type, $redux_key_name){
		global $reduxConfig;
		$posts = $this->list_posts_as_redux_select_array( $post_type );
		$redux_key_val = $this->sms_redux[ $redux_key_name ];
		$first_post_id = $this->get_first_item_in_associative_array($posts);
		$result = '';
		// echo "<pre>===================================</pre>";
		// echo "<h2>1) \$post_type: " . print_r($post_type, true) . "</h2>";
		// echo "<pre>2) \$posts: " . print_r($posts, true) . "</pre>";
		// echo "<pre>2) first post: " . print_r( $first_post_id, true) . "</pre>";
		// echo "<pre>3) \$redux_key_name: " . print_r($redux_key_name, true) . "</pre>";
		// echo "<pre>3) \$redux_key_val: " . print_r($redux_key_val, true) . "</pre>";

		if( !$redux_key_val || $redux_key_val == 'error' ){
			$result = $first_post_id;
		} elseif( $redux_key_val && 'publish' == get_post_status ( $redux_key_val ) ){
			// if redux is set, check if the post actually exists and is published
			$result = $redux_key_val;
		} else {
			// everything has failed, there is no post to choose. It's over
			$result = false;
		}
		return $result;
	}

	/**
	 * Converts a redux formatted array (key, pair) to the array format that DMS uses.
	 * @param  Array $redux_array
	 * @return Array
	 */
	public function convert_redux_choices_to_dms($redux_array){
		if($redux_array){
			$dms_array = array();
			foreach ($redux_array as $key => $value) {
				$dms_array[$key] = array( 'name' => $value );
			}
			return $dms_array;
		}
	}

	public function filter_out_redundant_css_properties($array){
		if( is_array($array) ){ 
			foreach ($array as $key => $value) {
				if( $key == 'none' || $key == 'initial' || $key == 'inherit' )
					unset($array[$key]);
			}
		}
		return $array;
	}
	
	public function get_font_size_int_from_key($size_key){
		$size_int_list = $this->sms_options['fonts']['size-integer-list'];
		$size_int = $size_int_list[$size_key];
		if($size_int)
			return $size_int;
	}
	
	public function get_font_size_rem_from_key($size_key){
		if($size_key)
			return $this->sms_options['fonts']['size-rem-list'][$size_key];
	}
	
	public function get_font_size_em_from_key($size_key){
		$size_int_list = $this->sms_options['fonts']['size-float-list'];
		$size_int = round($size_int_list[$size_key], 3);
		if($size_int)
			return $size_int . "em";
	}

	public function get_font_size_px_from_key($size_key){
		$size_int_list = $this->sms_options['fonts']['size-integer-list'];
		$size_int = ($size_int_list[$size_key] * intval( $sms_redux['font-scale-base']));
		if($size_int)
			return $size_int . "px";
	}
	
	public function get_font_scale_array( $base_size, $ratio, $round = true ){

		global $sms_utils;

		$base_size = 1;
		$cur_size = $base_size;
		$return_array = array();
		$i = 0;
		foreach ( array_reverse($sms_utils->sms_options['fonts']['size-name-reference-list']) as $key => $value) {
			if($i == 0){
				// mini
				$int = ( ($base_size / $ratio) / $ratio );
				$return_array[$key] = $round ? round($int,2) : $int;
			} elseif($i == 1){
				// small
				$int = ($base_size / $ratio);
				$return_array[$key] = $round ? round($int,2) : $int;
			} else {
				// medium and up
				$return_array[$key] = $round ? round($base_size,2) : $base_size;
				//increase base size for next iteration
				$base_size = $base_size * $ratio;
			}
			
			$i++;
		}
		return $return_array;
	}

	public function get_font_assignments(){

		//===============================================================
		// Assemble an array with actively selected font assignment data
		// body, heading--primary, heading--secondary
		// size, weight, family
		// --------------------------------------------------------------

		$assignment_keys = array();
		if($this->sms_options['fonts']['assignment-field-data']){
			foreach ($this->sms_options['fonts']['assignment-field-data'] as $key => $value) {
				$assignment_keys[$key] = $value['title'];
			}
		}

		if($assignment_keys){
			$selected_font_assignment_data = array();
			foreach ($assignment_keys as $key => $val) {
				$size_key = $key."-size"; //heading--primary-size, body-size, etc...
				$weight_key = $key."-weight"; //heading--primary-weight, body-weight, etc...
				$font_key = $key."-font"; //heading--primary-font, body-font, etc...

				$font_type = $this->global_selected_font_kit_meta[$font_key];
				$font_family = $this->global_selected_font_kit_meta[$font_type . "-font-family"];
				$size_int_list = $this->sms_options['fonts']['size-integer-list'];
				$size_name = $this->global_selected_font_kit_meta[$size_key];
				$size_int = $size_int_list[$size_name];
				$size_rem = $this->sms_options['fonts']['size-rem-list'][$size_name];
				$weight = $this->global_selected_font_kit_meta[$weight_key];

				$selected_font_assignment_data[$key]['font-family'] = $font_family;
				$selected_font_assignment_data[$key]['font-weight'] = $weight;
				// if( $fontUnit == 'rem' ){
					$selected_font_assignment_data[$key]['font-size'] = $size_rem;
				// } else {
				//   $selected_font_assignment_data[$key]['font-size'] = $size_int;
				// }
			}
			// $sms_options['fonts']['assignment-data'] = $selected_font_assignment_data;
			
			return $selected_font_assignment_data;
		}
	}

	public function get_all_font_type_data(){
		
		$font_kit_fields = $this->sms_options['fonts']['definition-field-data'];
		// echo "<pre>\$font_kit_fields from class: " . print_r($font_kit_fields, true) . "</pre>";

		$result = array();
		foreach ($font_kit_fields as $value) {
			$result[$value['id']]['id'] = $value['id'];
			$result[$value['id']]['weights'] = $this->get_weights_for_font_type($value['id']);
			$result[$value['id']]['default-font-family'] = $this->get_font_family_from_font_type($value['id']);
		}
		return $result;
	}

	public function get_active_font_type_data(){
		$result = array();
		foreach ($this->get_font_type_list_as_redux_select_array() as $font_type_slug => $value) {
			$result[$font_type_slug]['id'] = $font_type_slug;
			$result[$font_type_slug]['weights'] = $this->get_weights_for_font_type($font_type_slug);
			$result[$font_type_slug]['font-family'] = $this->get_font_family_from_font_type($font_type_slug);
		}
		return $result;
	}

	public function get_weights_for_font_type($font_type_slug, $post_id = false){
		if($post_id == false){
			// If no post ID is specified as an argument, 
			// use the globally selected font kit ID

			$post_id = $this->global_selected_font_kit_id;
			$post_meta = $this->global_selected_font_kit_meta;
			if(!$post_id) {
				// If there is no global post selected, fail
				return false;
			}
		} else {
			$post_meta = $this->get_redux_post_meta( $post_id );
		}
		$weight_array = $post_meta[ $font_type_slug .'-weights'] ?: false;
		if(!$weight_array) {
			// If there are no weights assigned, fail silently
			return false;
		}

		$font_weight_options_named = array();
		foreach ($post_meta[ $font_type_slug .'-weights'] as $weight) {
			$result = array_search( intval($weight), array_flip($this->sms_options['fonts']['weight-name-list'] ),true );
			if($result)
				$font_weight_options_named[$weight] = $result;
		}
		return $font_weight_options_named;
	}

	public function get_font_family_from_font_type($font_type_slug, $font_kit_id = null){
		if(!$font_type_slug) return false;
		if($font_kit_id){
			$font_kit_id;
		} else {
			$this->global_selected_font_kit_id;
		}

		$active_font_types = $this->get_available_font_types( $font_kit_id );
		$result = $this->global_selected_font_kit_meta[ $font_type_slug .'-font-family' ] ?: $active_font_types[$font_type_slug]['family'];
		return $result;
	}

	// //===============================================================
	// // List posts from a post type as a redux select array
	// // --------------------------------------------------------------

	public function list_posts_as_redux_select_array( $post_type ){

		$wp_query = new WP_Query( array( 
			'post_type'   => array( $post_type ),
			'post_status' => 'publish',
			'fields'      => 'ids',
			'pagination'  => false,
			'posts_per_page' => '99',
		) );

		$return_array = array();
		while ( $wp_query->have_posts() ) : $wp_query->the_post();
			$id = get_the_id();
			$title = get_the_title();
			// add them to the array
			$return_array[$id] = $title;
		endwhile;

		return $return_array;

	}

	// //===============================================================
	// // Gather list of fonts available from active font kit
	// // --------------------------------------------------------------

	public function get_available_font_types($font_kit_post_id){

		// Get the stored info from the active font kit post
		if($font_kit_post_id === null)
			$font_kit_post_id = $this->global_selected_font_kit_id;

		$font_kit_meta = $this->get_redux_post_meta( $font_kit_post_id );

		if(!$font_kit_meta){
			return array();
		}

		// Use the array that builds the fields in order to grab the titles
		$font_kit_fields = $this->sms_options['fonts']['definition-field-data'];

		// Create an array that stores all active font type info 
		// slug, title, weights, family
		if($font_kit_fields){
			$active_font_data = array();
			foreach ($font_kit_fields as $value) {
				$font_slug = $value['id'];
				$font_enabled = $font_kit_meta[$value['id'].'-enabled'] ?: false;
				if($font_enabled){
					$active_font_data[$font_slug] = array(
						'slug' => $font_slug,
						'title' => $value['title'],
						'weights' => $font_kit_meta[$font_slug.'-weights'],
						'family' => $font_kit_meta[$font_slug.'-font-family'] ?: $value['default-family'],
					);
				}
			}
			return $active_font_data;
		}

	}

	public function get_font_type_classes(){

		$array = $this->get_font_type_list_as_redux_select_array();
		if($array){
			foreach ($array as $key => $value) {
				$output["font-$key"] = $value;
			}
			return $output;
		} else {
			return false;
		}
	}


	// //===============================================================
	// // Create list of available font types into an array for use in a
	// // Redux select field
	// // --------------------------------------------------------------

	

	public function get_font_type_list($font_kit_post_id = null){
		if($font_kit_post_id === null)
			$font_kit_post_id = $this->global_selected_font_kit_id;
		
		$type_data = $this->get_available_font_types($font_kit_post_id);
		$font_kit_fields = $this->sms_options['fonts']['definition-field-data'];
		$select_array = array();
		if($type_data){
			foreach ($type_data as $value) {
				$select_array[ $value['slug'] ] = $value['title'];
			}
			return $select_array;
		}
	}

	public function get_font_type_list_as_redux_select_array($font_kit_post_id = null){
		if($font_kit_post_id === null)
			$font_kit_post_id = $this->global_selected_font_kit_id;
		
		return $this->get_font_type_list($font_kit_post_id);
	}

	public function get_available_font_type_classes($font_kit_post_id = null){
		if($font_kit_post_id === null)
			$font_kit_post_id = $this->global_selected_font_kit_id;
		
		$type_list = $this->get_font_type_list( $font_kit_post_id );
		foreach ($type_list as $key => $value) {
			$class_list['font-'.$key] = $value;
		}
		return $class_list;
	}

	public function objectToArray ($object) {
	    if(!is_object($object) && !is_array($object))
	        return $object;

	    return array_map(array($this, 'objectToArray'), (array) $object);
	}

	// =================================================================
	// Filter Redux global array by a string, and return an array of 
	// key value pairs that contain the string passed to the function
	//
	// Able to accept a string or an array of strings
	// When passing an array, it must match ALL strings 
	// to return a key/pair
	// ------------------------------------------------------------------

	public function filter_redux_array($needle, $haystack = null, $offset = false, $exclude_string = null){
		
		if($haystack === null) $haystack = $this->sms_redux;
		// $debug = true;
		$debug = false;

		// Can filter an array or a string
		// When filtering an array, filter for keys that match the needle and return all matches

		$result = array();
		foreach ($haystack as $haystack_key => $value) {

			if($debug) echo "<pre>searching for key: $haystack_key</pre>";

			if( is_array($needle) ){
				$i = 1;
				foreach ($needle as $needle_item) {
					$pos = strpos($haystack_key, $needle_item, $offset);
					if($debug) echo "<pre>position: $pos</pre>";
					if(false===$pos) {
						if($debug) echo "<pre>[$i] Needle ($needle_item) was not found in ($haystack_key)</pre>";
						// Needle not found in this iteration. 
						// break loop to next item in array
						break;
					} else {
						if($debug) echo "<pre>[$i] Needle ($needle_item) was FOUND in ($haystack_key) in position: ($pos)</pre>";
						// Needle found in this iteration
						if( $i == count($needle) ){
							// If the count of needles matches the iterations, 
							// then it's assumed that all keys have been checked, 
							// and it's safe to add the value found to the result array
							if( strpos( $value, $exclude_string ) ){
								if($debug) echo "<pre>[$i] exclude string ($exclude_string) matched, skipping ($value)</pre>";
							} else {
								$result[$haystack_key] = $value;
								if($debug) echo "<pre>[$i] ADDING TO \$RESULT!</pre>";
							}
						}
					}
					$i++;
				}
			} else {
				$pos = strpos($haystack_key, $needle, $offset);

				if($offset !== false && $pos !== $offset){
					// If offset is set, and the needle isn't found in the haystack, skip to next loop
				} elseif ($pos !== false){
					if($debug) echo "<pre>Needle ($needle) was found in ($haystack_key) at position ($pos)</pre>";

					if( strpos( $haystack_key, $exclude_string ) ){
						if($debug) echo "<pre>Exclude string ($exclude_string) matched, skipping ($haystack_key)</pre>";
					} else {
						$result[$haystack_key] = $value;
					}
				}
			}
		}
		// echo "<pre>\$result: " . print_r($result, true) . "</pre>";
		return $result;
	}

	// //===============================================================
	
	public function list_pages_as_flat_array( $id = null ) {
	  $args = array(
	    'hierarchical' => 0,
	    // 'parent_of' => 0,
	    'parent' => ($id ? $id : 0),
	    'sort_coumn' => 'menu_order',
	    // 'child_of' => $id,
	  );
	  return get_pages($args);
	}
	public function list_pages_hierarchically() {
		$option = wp_cache_get('dms_sms_heirarchical_pages_options', 'sms-dms');
		if($option !== false) return $option;
	  $pages = $this->list_pages_as_flat_array();
	  $i = 0;
	  foreach ( $pages as $page ) {
	    $i++;

	    $link = str_replace(get_site_url(), "[pl_site_url]", get_page_link( $page->ID ) );
	    $title = $page->post_title;
	    $option[$link] = $title;

	    $children = $this->list_pages_as_flat_array($page->ID);
	    $j = 0;
	    if($children){
	      foreach ($children as $child_page) {
	        $j++;

	        $link = str_replace(get_site_url(), "[pl_site_url]", get_page_link( $child_page->ID ) );
	        $title = $child_page->post_title;
	        $option[ $link ] = " - ".$title;
	      }
	    }
	  }
	  wp_cache_set('dms_sms_heirarchical_pages_options', $option, 'sms-dms');
	  return $option;
	}
	
	// //===============================================================

	function hex2rgb( $colour ) {
		if ( $colour[0] == '#' ) {
			$colour = substr( $colour, 1 );
		}
		if ( strlen( $colour ) == 6 ) {
			list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
		} elseif ( strlen( $colour ) == 3 ) {
			list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
		} else {
			return false;
		}
		$r = hexdec( $r );
		$g = hexdec( $g );
		$b = hexdec( $b );
		return array( 'red' => $r, 'green' => $g, 'blue' => $b );
	}

	function array_slice_assoc($array,$keys) {
		return array_intersect_key($array,array_flip($keys));
	}

	//===============================================================
	public function get_band_slot_id_by_slug($slug){
		$bands = $this->get_band_slot_data();
		$slot_array = array_column($bands, 'id', 'slot-slug');
		return $slot_array[$slug];
	}

	public function get_band_slot_slug_from_id($id){
		$bands = $this->get_band_slot_data();
		$slot_array = array_column($bands, 'slot-slug', 'id');
		return $slot_array[$id];
	}

	public function get_band_slot_data_by_shade($shade = 'light'){
		$result = array();
		$slot_list = $this->filter_redux_array( array('band-slot', $shade) );
		$i = 1;
		foreach ( $slot_list as $post_id ) {
			if($post_id){
				$post_meta = $this->get_redux_post_meta( $post_id );
				// $result[$post_id] = array(
				$result[] = array(
					'id'          => $post_id,
					'slot-slug'   => "band$i-$shade",
					'title'       => get_the_title( $post_id ),
					'shade'       => $shade,
					'slot-number' => $i,
				);
			}

			$i++;
		}
		// echo "<pre>\$result: " . print_r($result, true) . "</pre>";
		return $result;
	}
	// //===============================================================

	public function get_band_slot_data(){
		$data = array_merge($this->get_band_slot_data_by_shade('light'), $this->get_band_slot_data_by_shade('dark'));
		return $data;
	}

	// //===============================================================

	public function get_post_list_by_term_as_redux_field_array($post_type_input, $taxonomy_input = false, $term_input = false){
		$cpt_query = new WP_Query( 
			array(
				'post_type'      => $post_type_input,
				'post_status'    => 'publish',
				'fields'         => 'ids',
				'pagination'     => false,
				'posts_per_page' => '99',
			) 
		);

		if( !$cpt_query->have_posts() )
			return array("error" => "Nothing Found");

		$post_list = array();
		while ( $cpt_query->have_posts() ) : $cpt_query->the_post();

			$id = get_the_id();
			$title = get_the_title();

			if($term_input){
				$terms = get_the_terms( $id, $taxonomy_input );
				foreach($terms as $term){
					
					if($term_input){
						if($term->slug == $term_input){
							// add to array
							if( $title ){
								$post_list[$id] = $title;
							}
						}
					} else {
						if( $title ){
							$post_list[$id] = $title;
						}
					}
				}
			} else{
				$post_list[$id] = $title;
			}

		endwhile;

		return $post_list;
	}

  /* 
   * Added to version 1.6.0
   */
  
	// A function that allows the use of an array as the needle
	// Usage: 
	// $string = 'Whis string contains word "cheese" and "tea".';
	// $array  = array('burger', 'melon', 'cheese', 'milk');
	// var_dump(strposa($string, $array)); // will return true, since "cheese" has been found
	function strposa($haystack, $needle, $offset=0, $return_match = null) {
		if(!is_array($needle)) $needle = array($needle);
		foreach($needle as $query) {
			if($return_match){
			if(strpos($haystack, $query, $offset) !== false) return $query; // stop on first true result
			} else {
				if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
			}
		}
		return false;
	}

	public function css_array_to_string($css_array){
		if( !is_array($css_array) ) return false;
		
		$css_string = '';
		foreach ($css_array as $key => $category) {
			if( isset($category) ){
				$css_string .= $category;
			}
		}
		return $css_string;
	}

	public function get_upload_folder($type = 'path'){
		$wp_uploads_dir = wp_upload_dir();
		$sms_uploads_dir = $wp_uploads_dir['basedir'];
		$sms_uploads_url = $wp_uploads_dir['baseurl'];
		if($type == 'path'){
			return trailingslashit( $sms_uploads_dir );
		}else{
			return trailingslashit( $sms_uploads_url );
		}
	}

	/* Write to the WordPress debug log */
	public function write_log( $log, $log_path = null )  {
		if(!$log_path)
			$log_path = $this->get_upload_folder()."sms-debug.log";

		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( "[".date("H:i:s")."] " . print_r( $log, true ), 3, $log_path );
		} else {
			error_log( "[".date("H:i:s")."] ".$log."\n", 3, $log_path );
		}
	}

	public function save_css_string_to_file($filename, $css_data) {

		/** Capture dynamic CSS output from php **/
		// $css_dir = trailingslashit(DMS_SMS_PATH) . 'css/'; // Shorten code, save 1 call
		// ob_start();
		// require($css_dir . 'styles.php');
		// $css = ob_get_clean();
		//////////////////////////////
		
		// Load up WordPress filesystem
		global $wp_filesystem;
		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}
		$save_file_path = $this->get_upload_folder('path').$filename;
		$save_file_url = $this->get_upload_folder('uri').$filename;

		/** Check if there's anything to write **/
		if(!$css_data){
			return;
		}
		 
		/** Write string to css file **/
		global $wp_filesystem;
		if ( ! $wp_filesystem->put_contents( $save_file_path, $css_data, FS_CHMOD_FILE) ) {
			add_action('admin_notices', function() {
				$sms_admin_url = '/wp-admin/admin.php?page=dms-sms.php';
				echo "<div class='updated'>";
				echo "<h3 style=\"color: #ba0620;\">Error saving css file ($save_file_path)</h3>";
				echo "<p>Please refresh your browser</p>";
				echo "</div>";
			});
		} else {
			return $save_file_url;
		}
	}

	public function save(){
		// echo "<pre style='padding-left: 180px;'>saving: " . print_r($this->sms_options, true) . "</pre>";
		// echo "<pre style='padding-left: 180px;'><strong>saving css:</strong><br> " . print_r($this->sms_css, true) . "</pre>";
		// echo "<pre style='padding-left: 180px;'><strong>saving less:</strong><br> " . print_r($this->sms_less, true) . "</pre>";
		update_option('sms_options', $this->sms_options);
		update_option('sms_less', $this->sms_less);
		update_option('sms_css', $this->sms_css);
	}

	// function __destruct(){
	//   $this->save();
	// }
	// 

}
// add_action('plugins_loaded', function(){
$sms_utils = new smsUtilities();
	
// },20);

class CssSelectors {
	var $selectors = Array();
 
	public function __construct() {
		// Initialize the hell out of this class
	}
 
	public function get($selector_name) {
		if( !$this->selectors[$selector_name] ) {
			$this->selectors[$selector_name] = new CssProperties();
		}
		// $result_arr = $this->selectors[$selector_name];
		// if( !empty($result_arr->properties) ){
		//   echo "<pre>" . var_dump($result_arr->properties) . "</pre>";
		// }
		return $this->selectors[$selector_name];
	}

	public function get_css() {
		$css = '';
		foreach ($this->selectors as $selector => $props) {
			// if there are no properties for this selector, skip it
			if( count($props->properties) == 0 )
				continue;
			$css .= $selector . "{\n";
			foreach ($props->properties as $prop => $val){
				$css .= "\t" . $prop . ": " . $val . ";\n";
			}
			$css .= "}\n";
		}
		return $css;
	}

	public function get_css_and_less_seperated() {
		$css = '';
		$less = '';
		$i = 1;
		foreach ($this->selectors as $selector => $props) {
			// if there are no properties for this selector, skip it
			if( count($props->properties) == 0 ){
				$i++;
				continue;
			}

			$less_properties = '';
			$css_properties = '';
			foreach ($props->properties as $prop => $val){
				if($val){
					if( strpos($val, '@') !== false){
						$less_properties .= "\t" . $prop . ": " . $val . ";\n";
					} else {
						$css_properties .= "\t" . $prop . ": " . $val . ";\n";
					}
				}
			}
			$less .= $less_properties ? $selector . "{\n" . $less_properties . "}\n" : '';
			$css .= $css_properties ? $selector . "{\n" . $css_properties . "}\n" : '';

			$i++;
		}

		$return_array = array('css'=>$css, 'less'=>$less);
		// echo "<pre>\$return_array: " . print_r($return_array, true) . "</pre>";
		return $return_array;
	}

}
 
class CssProperties {
	var $properties = Array();
	public function __construct() {
		// initialization code
	}
 
	public function add($prop_name, $value, $priority=10) {
		$result = $this->validate($prop_name, $value);
		if(!$result["success"]) return false;
 
		// If it is valid, proceed with adding the property to $this->properties
		$this->properties[$prop_name] = $value;
 
		return true;
	}
 
	public function validate($prop_name, $value) {
		// remove whitespace
		$value = preg_replace('/\s+/', '', $value);
		if( !$value || $value == "#" || $value == "px" || $value == " px" || $value == "em" ){
			return Array(
				"success" => false,
				"error" => "CSS property is empty."
			);
		}

		// if( is_array( $value ) ){
		//   $result = $this->format_css_property_from_array($value);
		// }

		// Validate this bad boy! return an array containing "success" and "error"
		// keys. The "error" key you only populate if theres a problem
		return Array("success"=>true, "error"=>"Message in case its invalid");
	}

}
$css_selectors = new CssSelectors();

add_action('shutdown', 'sms_save_data_on_shutdown');
function sms_save_data_on_shutdown(){
	global $sms_utils;
	$sms_utils->save();
};