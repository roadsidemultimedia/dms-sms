<?php
// Add menu links to admin bar
// ------------------------------------------------------
function sms_admin_bar_render(){
  global $wp_admin_bar;
  global $sms_utils;
  global $sms_redux;

  $wp_admin_bar->add_menu( 
    array(
      'parent' => false,
      'id'     => 'dms_sms',
      'title'  => 'SMS',
      'href'   => '/wp-admin/admin.php?page=dms-sms.php',
    )
  );

  // Bands
  // ----------------------
  
  $wp_admin_bar->add_menu(
    array(
      'id'     => 'dms_sms_bands',
      'parent' => 'dms_sms',
      'title'  => 'Bands',
      'href'   => '/wp-admin/edit.php?post_type=sms_band'
    )
  );

  foreach ($sms_utils->get_band_slot_data() as $v) {
    $id       = $v['id'];
    $slug     = $v['slug'];
    $title    = $v['title'];
    $shade    = $v['shade'] == 'light' ? '♙' : '♟';
    $slot     = $v['slot-number'];
    $edit_url = get_edit_post_link( $v['id'] );

    $wp_admin_bar->add_menu(
      array(
        'parent' => 'dms_sms_bands',
        'id'     => $slug,
        'title'  => "$shade Slot: $slot / Title: $title",
        'href'   => $edit_url,
      )
    );
  }


  // Buttons
  // ----------------------
  
  

  function get_sms_factory_admin_bar_menu($factory_type = null, $factory_name){
    global $wp_admin_bar;
    global $sms_utils;
    global $sms_redux;
    $factory_slots = constant("SMS_".strtoupper($factory_type)."_SLOTS");

    $wp_admin_bar->add_menu(
      array(
        'id'     => "dms_sms_$factory_type",
        'parent' => 'dms_sms',
        'title'  => $factory_name,
        'href'   => "/wp-admin/edit.php?post_type=sms_$factory_type"
      )
    );
    $slot_list = array();
    for ($i=1; $i <= $factory_slots; $i++) { 
      $slot_list["$factory_type-slot$i"] = "Slot $i";
    }
    foreach ($slot_list as $key => $v) {

      $id = $sms_redux[$key];
      if(!$id)
        continue;

      $title = get_the_title($id);

      $wp_admin_bar->add_menu(
        array(
          'parent' => "dms_sms_$factory_type",
          'id'     => $key,
          'title'  => "$v / $title",
          'href'   => get_edit_post_link( $id ),
        )
      );
    }
  }
  get_sms_factory_admin_bar_menu('header_template', 'Header Template');
  get_sms_factory_admin_bar_menu('button', 'Buttons');
  get_sms_factory_admin_bar_menu('slider', 'Sliders');
  get_sms_factory_admin_bar_menu('divider', 'Dividers');
  get_sms_factory_admin_bar_menu('card', 'Cards');
  get_sms_factory_admin_bar_menu('quote', 'Quotes');
  get_sms_factory_admin_bar_menu('accordion', 'Accordions');
  get_sms_factory_admin_bar_menu('list', 'Lists');
  get_sms_factory_admin_bar_menu('grid', 'Grid');
  
  // Header
  // ----------------------
  $id = $sms_redux["header-slot1"] ?: false;
  if($id){
    $wp_admin_bar->add_menu(
      array(
        'id'     => 'dms_sms_header',
        'parent' => 'dms_sms',
        'title'  => 'Header',
        'href'   => get_edit_post_link( $id ),
      )
    );
  } else {
    $wp_admin_bar->add_menu(
      array(
        'id'     => 'dms_sms_header',
        'parent' => 'dms_sms',
        'title'  => 'Header <span style="float: right; padding-right: 2px; font-size: 14px;">✚</span>',
        'href'   => '/wp-admin/edit.php?post_type=sms_header',
      )
    );
  }

  // Style Chunks
  // ----------------------
  
  $wp_admin_bar->add_menu(
    array(
      'id'     => 'dms_sms_style',
      'parent' => 'dms_sms',
      'title'  => 'Style Chunks',
      'href'   => "/wp-admin/edit.php?post_type=sms_style",
    )
  );

  $style_chunk = new styleChunkFactory();
  $style_chunk_posts = $style_chunk->get_enabled_posts();

  foreach ($style_chunk_posts as $id => $title) {

    $wp_admin_bar->add_menu(
      array(
        'parent' => "dms_sms_style",
        'id'     => "sms_style_$id",
        'title'  => "$title",
        'href'   => get_edit_post_link( $id ),
      )
    );
  }

  // Color Scheme
  // ----------------------

  if($sms_utils->global_selected_color_scheme_id){
    $wp_admin_bar->add_menu(
      array(
        'title'  => 'Color Scheme',
        'id'     => 'dms_sms_color_scheme',
        'parent' => 'dms_sms',
        'href'   => get_edit_post_link($sms_utils->global_selected_color_scheme_id),
      )
    );
  } else {
    $wp_admin_bar->add_menu(
      array(
        'title'  => 'Color Scheme <span style="float: right; padding-right: 2px; font-size: 14px;">✚</span>',
        'id'     => 'dms_sms_color_scheme',
        'parent' => 'dms_sms',
        'href'   => '/wp-admin/edit.php?post_type=sms_color',
      )
    );
  }


  // Font Kit
  // ----------------------

  if($sms_utils->global_selected_font_kit_id){
    $wp_admin_bar->add_menu(
      array(
        'title'  => 'Font Kit',
        'id'     => 'dms_sms_font_kit',
        'parent' => 'dms_sms',
        'href'   => get_edit_post_link($sms_utils->global_selected_font_kit_id),
      )
    );
  } else {
    $wp_admin_bar->add_menu(
      array(
        'title'  => 'Font Kit <span style="float: right; padding-right: 2px; font-size: 14px;">✚</span>',
        'id'     => 'dms_sms_font_kit',
        'parent' => 'dms_sms',
        'href'   => '/wp-admin/edit.php?post_type=sms_font_kit',
      )
    );
  }
}
add_action( 'wp_before_admin_bar_render', 'sms_admin_bar_render' );