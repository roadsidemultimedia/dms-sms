<?php
/*
Plugin Name: DMS SMS (Style Management System)
Plugin URI: https://bitbucket.org/roadsidemultimedia/dms-sms
Description: Manage styles used throughout your site.
Version: 1.6.6
Author: Roadside Multimedia, Milo Jennings
Contributors: Barak Llewellyn
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/dms-sms
Bitbucket Branch: master
Class Name: sms_core_section
Filter: component
*/

define('DMS_SMS_VERSION', '1.6.6');
define('DMS_SMS_PATH', plugin_dir_path(__FILE__) );
define('DMS_SMS_URL', plugins_url('', __FILE__ ) );
define('SMS_DEBUG', false );

$library = get_option('sms-library-mode');

define('SMS_BAND_SLOTS',            $library ? 99 : 6);
define('SMS_CARD_SLOTS',            $library ? 99 : 30 );
define('SMS_BUTTON_SLOTS',          $library ? 99 : 8  );
define('SMS_DIVIDER_SLOTS',         $library ? 99 : 10  );
define('SMS_HEADER_TEMPLATE_SLOTS', $library ? 99 : 1  );
define('SMS_SLIDER_SLOTS',          $library ? 99 : 20  );
define('SMS_QUOTE_SLOTS',           $library ? 99 : 10  );
define('SMS_ACCORDION_SLOTS',       $library ? 99 : 10  );
define('SMS_LIST_SLOTS',            $library ? 99 : 10  );
define('SMS_GRID_SLOTS',            $library ? 99 : 10  );

define('SMS_HEADER_SLOTS',  1  );

// Set library mode from redux option to a WP option, so it's available when defining the slots above
add_action('admin_init', function(){
  global $sms_redux;
  update_option('sms-library-mode', ($sms_redux["sms-library-mode"] ? true : false) );
});

add_image_size( 'true-aspect-thumb-200', 200, 0, false );

require dirname( __FILE__ ) . '/lib/vendor/polyfill/array_column.php';
require dirname( __FILE__ ) . '/lib/get-plugin-versions.php';

// Register mustache library
require dirname( __FILE__ ) . '/lib/vendor/Mustache/Autoloader.php';
Mustache_Autoloader::register();

include_once ( dirname( __FILE__ ) . "/lib/utilities.php" );
include_once ( dirname( __FILE__ ) . "/lib/factory.php" );
include_once ( dirname( __FILE__ ) . "/lib/admin-bar.php" );

add_action('wp_enqueue_scripts', function(){
  // wp_enqueue_script( 'magnific-js', DMS_SMS_URL.'/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), pl_get_cache_key(), true );
  wp_enqueue_script( 'sms-js', DMS_SMS_URL.'/assets/js/sms-common.js', array( 'jquery' ), pl_get_cache_key(), true );
  // wp_enqueue_style( 'magnific-css', DMS_SMS_URL.'/assets/css/magnific-popup.css');
});

if( wp_get_theme() == 'DMS Skeleton Theme' ){
  // Skeleton is active
} else {
  add_action('admin_notices', function() {
    $sms_admin_url = '/wp-admin/admin.php?page=dms-sms.php';
    echo "<div class='updated'>";
    echo "<h3 style=\"color: #ba0620;\">Fatal error!</h3>";
    echo "<p>The <a href='https://bitbucket.org/roadsidemultimedia/dms-skeleton'>DMS Skeleton Theme</a> needs to be active in order for the Style Management System to have any effect on the public facing pages.";
    echo "<a style='margin-left: 10px;' class='button' href='https://bitbucket.org/roadsidemultimedia/dms-skeleton/downloads'>Download theme</a>";
    echo "</p>";
    echo "</div>";
  });
  return;
}

add_action('admin_head', 'sms_admin_css');
function sms_admin_css() {
  echo '<link rel="stylesheet" href="'.DMS_SMS_URL.'/assets/css/sms-admin-style.css" type="text/css" media="all" />';
}
add_action('wp_head', 'sms_frontend_css');
function sms_frontend_css() {
  if( ! is_admin() && current_user_can( 'manage_options' ) ){
    echo '<link rel="stylesheet" href="'.DMS_SMS_URL.'/assets/css/sms-frontend-style.css" type="text/css" media="all" />';
  }
}

if(SMS_DEBUG)
  include_once ( dirname( __FILE__ ) . "/admin/debug.php" );


include_once ( dirname( __FILE__ ) . "/admin/init/admin-init.php" );

// Auto load php files
foreach ( glob( dirname( __FILE__ ) . "/admin/custom-posts/*" ) as $filename) {
	include_once( $filename );
}
foreach ( glob( dirname( __FILE__ ) . "/admin/style-output/*" ) as $filename) {
  include_once( $filename );
}

include_once( dirname( __FILE__ ) . "/admin/global/global-colors.php" );
include_once( dirname( __FILE__ ) . "/admin/global/global-fonts.php" );
include_once( dirname( __FILE__ ) . "/admin/global/global-bands.php" );
include_once( dirname( __FILE__ ) . "/admin/global/global-style-slots.php" );
include_once( dirname( __FILE__ ) . "/admin/global/global-factory-slots.php" );
include_once( dirname( __FILE__ ) . "/admin/global/global-debug.php" );

// Gather data and store in an option variable named 'sms_options' for later use
add_action('admin_init', function(){
  global $sms_redux;
  global $sms_utils;

  $color_scheme_id   = $sms_utils->global_selected_color_scheme_id;
  $color_scheme_meta = $sms_utils->global_selected_color_scheme_meta;
  $font_kit_meta     = $sms_utils->global_selected_font_kit_meta;
  $font_kit_id       = $sms_utils->global_selected_font_kit_id;

  foreach ( glob( dirname( __FILE__ ) . "/admin/data-collection/*" ) as $filename) {
    include_once( $filename );
  }
});


/* 
 * Added 11/19/2014 to SMS version 1.6.0 
 */

//from http://www.pagelinestheme.com/remove-customize-dms-editors-media-uploads-prefix/
//replace 'PageLines-' prefix for DMS Editor media uploads (from editor/editor.actions.php)
add_filter( 'pl_up_image_prefix', 'my_pl_upload_prefix');
function my_pl_upload_prefix(){
  return ''; //example result: 'filename.png'
  //return 'My Keyword - '; //example result: 'My Keyword - filename.jpg'
  //return get_bloginfo('name','raw') . ' - '; //example result: 'Site Name - filename.gif'
}

/* 
 * Disable DMS generated Mobile menu that's not used.
 * Added to SMS version 1.6.0
 */
add_action( 'pl_no_mobile_menu', '__return_true' );


/*WORK IN PROGRESS*/
/*
add_action('admin_init', function(){
  global $sms_utils;
  $active_font_kit_id = $sms_utils->active_font_kit_id;
  $active_color_scheme_id = $sms_utils->active_color_scheme_id;


  if(!$active_font_kit_id || !$active_color_scheme_id){
    add_action('admin_notices', function() {
      $sms_font_kit_edit_url = '/wp-admin/post-new.php?post_type=sms_font_kit';
      $sms_color_scheme_edit_url = '/wp-admin/post-new.php?post_type=sms_color';
      echo "<div class='updated'>";
      echo "<h3 style=\"color: #ba0620;\">Notice!</h3>";
      if($active_font_kit_id == false){
        echo "<p>No font kits exist yet. Create or import one.";
        echo "<a style='margin-left: 10px;' class='button' href='".$sms_font_kit_edit_url."'>Create a font kit</a>";
      }
      if(!$active_color_scheme_id == false){
        echo "<p>No color schemes exist yet. Create or import one.";
        echo "<a style='margin-left: 10px;' class='button' href='".$sms_color_scheme_edit_url."'>Create a color scheme</a>";
      }
      echo "</p>";
      echo "</div>";
    });
  } 
});
*/

// // TODO:
// // Dynamically generate color css for the backend, so select2 boxes can show a preview of the colors
// // This is just an example of the CSS needed.
// add_action('admin_head', function(){
//   echo "<style>";
//   echo "
//   i.sms-swatch{
//     display: inline-block; 
//     position: relative;
//     top: 3px;

//     width: 15px;
//     height: 15px;
//     border-radius: 50%;
//     background: #ccc;
//   }
//   i.sms-color-primary{
//     background: red;
//   }
//   i.sms-color-secondary{
//     background: blue;
//   }
//   ";
//   echo "</style>";
// });

add_action('wp_head', function(){
  $sms_options = get_option('sms_options');
  $kit_markup = $sms_options['fonts']['font-kit-markup'];
  if($kit_markup){
    echo "\n";
    echo $kit_markup;
  }
});

// Register top level admin page to serve as parent for custom post types
add_action( 'admin_menu', 'register_sms_menu_page', 9 );
function register_sms_menu_page(){
  add_submenu_page('dms-sms.php', 'Style Management', 'Global Settings', 'manage_options', 'admin.php?page=dms-sms.php');
}

/* Added by Milo 07/26/2014 as of version 1.0.3
 * Filter for [firstchild_redirect] and redirect to first child page
 */
add_action('template_redirect', 'firstChild_redirect_filter'); 
function firstChild_redirect_filter() { 
  global $wp_query; 
  if ( is_page() ) { 
    $post = $wp_query->get_queried_object();
    if ( false !== strpos($post->post_content, '[firstchild_redirect') ) { 
      if( function_exists("pause_exclude_pages") ) pause_exclude_pages();
      $pagekids = get_pages("child_of=".$post->ID."&sort_column=menu_order");
      $firstchild = $pagekids[0];
      if( function_exists("resume_exclude_pages") ) resume_exclude_pages();
      wp_redirect( get_permalink($firstchild->ID), 301 );
      exit;
    }
  } 
}

// ------------------------------------------------------

// Add title to edit pages to show which Custom Post Type you're editing
function sms_rename_add_cpt_title() { 
  global $current_screen;
  if ( $current_screen->id == 'sms_font_kit' ) { 
    $title = "Font Kit";
  } elseif( $current_screen->id == 'sms_color' ) {
    $title = "Color Scheme";
  } elseif( $current_screen->id == 'sms_band' ) {
    $title = "Banding Style";
  } elseif( $current_screen->id == 'sms_card' ) {
    $title = "Card Factory";
  } elseif( $current_screen->id == 'sms_button' ) {
    $title = "Button Factory";
  } elseif( $current_screen->id == 'sms_divider' ) {
    $title = "Divider Factory";
  } elseif( $current_screen->id == 'sms_slider' ) {
    $title = "Slideshow Factory";
  } elseif( $current_screen->id == 'sms_quote' ) {
    $title = "Quote Factory";
  } elseif( $current_screen->id == 'sms_list' ) {
    $title = "List Factory";
  } elseif( $current_screen->id == 'sms_grid' ) {
    $title = "Grid Factory";
  } elseif( $current_screen->id == 'sms_accordion' ) {
    $title = "Accordion Factory";
  } elseif( $current_screen->id == 'sms_style' ) {
    $title = "Style Chunk";
  } else {
    return false;
  }
  $output = "\n";
  $output .= "<script type='text/javascript'>\n";
  $output .= "jQuery('document').ready(function($) {\n";
  $output .= "$('.wrap h2').parent().prepend( '<h1>$title</h1>' );\n";
  $output .= "});\n";
  $output .= "</script>\n";
  echo $output;
}
add_action('admin_head', 'sms_rename_add_cpt_title');


// Clear out the DB query cache
function clear_dms_sms_wp_cache ($post_ID, $post_after=null, $post_before=null) {
  global $sms_utils;
  wp_cache_delete('dms_sms_heirarchical_pages_options');
}

add_action('post_updated', 'clear_dms_sms_wp_cache', 10, 3);
add_action('edit_post', 'clear_dms_sms_wp_cache', 10, 3);
add_action('wp_trash_post', 'clear_dms_sms_wp_cache', 10, 3);
add_action('untrash_post', 'clear_dms_sms_wp_cache', 10, 3);